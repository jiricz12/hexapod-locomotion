import copy
import os

import numpy as np
import pybullet as p
import pybullet_data
import time

import src.OBJparser
from src.robot import *
from src.heightmap import *
import src.environment.URDF_generator

NUM_LEGS = 6
NUM_JOINTS_IN_LEG = 3
NUM_JOINTS = NUM_LEGS * NUM_JOINTS_IN_LEG


class HexapodEnv:

    def __init__(self, terrain=None, parameters=Parameters(), animate=False, max_steps=100, noise_level=0.0, start_position=[0., 0., 0.]):
        self.animate = animate
        self.animation_speed = 0.0
        self.max_steps = max_steps

        # Initialize simulation
        if animate:
            self.client_ID = p.connect(p.GUI)
        else:
            self.client_ID = p.connect(p.DIRECT)
        assert self.client_ID != -1, "Physics client failed to connect"

        # Set simulation world params
        p.setGravity(0, 0, -9.8, physicsClientId=self.client_ID)
        p.setRealTimeSimulation(0, physicsClientId=self.client_ID)
        p.setAdditionalSearchPath(pybullet_data.getDataPath(), physicsClientId=self.client_ID)

        src.environment.URDF_generator.generate_robot_urdf(f"{os.path.join(os.path.dirname(os.path.realpath(__file__)))}/hexapod.urdf", parameters)

        if terrain.name == "Flat":
            self.plane = p.loadURDF(os.path.join(os.path.dirname(os.path.realpath(__file__)), "plane.urdf"), physicsClientId=self.client_ID) # Floor
        else:
            file_name = f"{os.path.join(os.path.dirname(os.path.realpath(__file__)))}/{terrain.name}"
            src.OBJparser.terrain_to_obj(terrain, file_name)
            self.plane = self.load_terrain(file_name+".obj")
        # Load actual robot into the world
        self.robot = p.loadURDF(os.path.join(os.path.dirname(os.path.realpath(__file__)), "hexapod.urdf"), physicsClientId=self.client_ID) # Hexapod

        # Various other parameters
        self.max_joint_force = 1.6
        self.target_vel = 0.4
        self.sim_steps_per_iter = 10 # The amount of simulation steps done every iteration.
        self.lateral_friction = 5.0

        for i in range(NUM_LEGS):
            p.changeDynamics(self.robot, 3 * i + 2, lateralFriction=self.lateral_friction, physicsClientId=self.client_ID)
        p.changeDynamics(self.robot, -1, lateralFriction=self.lateral_friction, physicsClientId=self.client_ID)

        self.start_position = start_position

        self.noise = None
        self.noise_level = noise_level

        self.step_ctr = 0
        self.act_dim = NUM_JOINTS

        self.vel_x_rew = 5
        self.pos_x_rew = 10.0 * 0
        self.time_rew = 1.0 * 0
        self.pos_y_rew = 50.0
        self.vel_z_rew = 0.5
        self.yaw_rew = 10.0
        self.pitch_rew = 30.0
        self.roll_rew = 30.0

        self.total_vel_x_rew = 0
        self.total_pos_x_rew = 0
        self.total_time_rew = 0
        self.total_pos_y_rew = 0
        self.total_vel_z_rew = 0
        self.total_yaw_rew = 0
        self.total_pitch_rew = 0
        self.total_roll_rew = 0

        self.total_work = 0
        self.total_acceleration = 0

        self.last_vel = np.zeros(3)
        self.last_ang_vel = np.zeros(3)

        self.robot_pos = np.array([0., 0., 0.])
        self.trajectory = [[np.array([0., 0., 0.]), np.array([0., 0., 0.]), np.array([0., 0., 0.]), np.array([0., 0., 0.]), 0]] * (self.max_steps + 1)

        self.joint_velocities = 0

        if animate:
            p.resetDebugVisualizerCamera(cameraDistance=2, cameraYaw=45, cameraPitch=-45, cameraTargetPosition=np.array([0., 0., 0.]), physicsClientId=self.client_ID)

    def process_input(self, ctrl):
        return ctrl

    def process_output(self, obs):
        return obs

    def step(self, ctrl):

        ctrl = self.process_input(ctrl)

        # Scale the action to correct range and publish to simulator (this just publishes the action, doesn't step the simulation yet)

        p.setJointMotorControlArray(bodyUniqueId=self.robot,
                                    jointIndices=range(NUM_JOINTS),
                                    controlMode=p.POSITION_CONTROL,
                                    targetPositions=ctrl,
                                    forces=[self.max_joint_force] * NUM_JOINTS,
                                    positionGains=[0.02] * NUM_JOINTS,
                                    velocityGains=[0.1] * NUM_JOINTS,
                                    physicsClientId=self.client_ID)

        r = 0

        # Step the simulation.
        last_time = time.time()
        for i in range(self.sim_steps_per_iter):
            p.stepSimulation(physicsClientId=self.client_ID)
            val = self.get_work()
            self.total_work += val
            r -= val / 2000
        now = time.time()

        if self.animation_speed != 0.0:
            if self.animate: time.sleep(self.animation_speed)

        # Get new observations (Note, not all of these are required and used)
        torso_pos, torso_quat, torso_vel, torso_angular_vel, joint_angles, joint_velocities, joint_torques, contacts = self.get_obs()
        xd, yd, zd = torso_vel # Unpack for clarity
        roll, pitch, yaw = p.getEulerFromQuaternion(torso_quat)

        self.trajectory[self.step_ctr] = [np.array(torso_pos), np.array([roll, pitch, yaw]), np.array(torso_vel), np.array(torso_angular_vel), now-last_time]

        self.robot_pos = torso_pos

        self.joint_velocities += sum(abs(np.array(joint_velocities)))

        # reward
        pos_x_rew = torso_pos[0] * self.pos_x_rew
        self.total_pos_x_rew += pos_x_rew

        vel_x_rew = -(xd - self.target_vel)**2 * self.vel_x_rew
        self.total_vel_x_rew += vel_x_rew

        time_rew = self.time_rew
        self.total_time_rew += time_rew

        # penalization
        yaw_rew = -np.square(yaw) * self.yaw_rew
        self.total_yaw_rew += yaw_rew

        roll_rew = -np.square(roll) * self.roll_rew
        self.total_roll_rew += roll_rew

        pitch_rew = -np.square(pitch) * self.pitch_rew
        self.total_pitch_rew += pitch_rew

        pos_y_rew = -np.square(torso_pos[1]) * self.pos_y_rew
        self.total_pos_y_rew += pos_y_rew

        vel_z_rew = -np.square(zd) * self.vel_z_rew
        self.total_vel_z_rew += vel_z_rew

        r += (pos_x_rew + vel_x_rew + time_rew + yaw_rew + roll_rew + pitch_rew + pos_y_rew + vel_z_rew) / self.max_steps
        # Scale joint angles and make the policy observation

        env_obs = np.concatenate((torso_pos, [roll, pitch, yaw], contacts, joint_angles, torso_vel, torso_angular_vel, joint_velocities)).astype(np.float32)

        self.step_ctr += 1

        env_obs = self.process_output(env_obs)

        # This condition terminates the episode
        done = self.step_ctr > self.max_steps or np.abs(roll) > 1.57 or np.abs(pitch) > 1.57
        if self.noise_level > 0:
            self.noise = (np.random.rand(len(env_obs)).astype(np.float32) * 2.0 - 1.0) * self.noise_level
            env_obs = env_obs + self.noise
        return env_obs, r, done, {}

    def get_obs(self):
        # Torso
        torso_pos, torso_quat = p.getBasePositionAndOrientation(self.robot, physicsClientId=self.client_ID) # xyz and quat: x,y,z,w
        torso_vel, torso_angular_vel = p.getBaseVelocity(self.robot, physicsClientId=self.client_ID)

        # Boolean values signifying if the given leg is in contact with the floor
        contacts = []
        for i in range(2, NUM_JOINTS, 3):
            contacts += [int(len(p.getContactPoints(self.robot, self.plane, i, -1, physicsClientId=self.client_ID)) > 0) * 2 - 1]

        # Joints
        obs = p.getJointStates(self.robot, range(NUM_JOINTS), physicsClientId=self.client_ID) # pos, vel, reaction(6), prev_torque
        joint_angles = []
        joint_velocities = []
        joint_torques = []
        for o in obs:
            joint_angles.append(o[0])
            joint_velocities.append(o[1])
            joint_torques.append(o[3])
        return torso_pos, torso_quat, torso_vel, torso_angular_vel, joint_angles, joint_velocities, joint_torques, contacts

    def get_work(self):
        obs = p.getJointStates(self.robot, range(NUM_JOINTS), physicsClientId=self.client_ID)  # pos, vel, reaction(6), prev_torque
        joint_torques = []
        for o in obs:
            joint_torques.append(o[3])

        return sum(abs(np.array(joint_torques)))

    def get_acceleration(self):
        torso_vel, torso_angular_vel = p.getBaseVelocity(self.robot, physicsClientId=self.client_ID)
        torso_vel = np.array(torso_vel)
        torso_angular_vel = np.array(torso_angular_vel)
        acc = sum(abs(torso_vel - self.last_vel))
        ang_acc = sum(abs(torso_angular_vel - self.last_ang_vel))
        self.last_vel = torso_vel
        self.last_ang_vel = torso_angular_vel
        return acc + ang_acc

    def get_basic_obs(self):
        torso_pos, torso_quat = p.getBasePositionAndOrientation(self.robot, physicsClientId=self.client_ID)  # xyz and quat: x,y,z,w
        obs = p.getJointStates(self.robot, range(NUM_JOINTS), physicsClientId=self.client_ID)  # pos, vel, reaction(6), prev_torque
        joint_angles = []
        for o in obs:
            joint_angles.append(o[0])

        return torso_pos, p.getEulerFromQuaternion(torso_quat), joint_angles

    def rads_to_norm(self, joints):
        sjoints = np.array(joints)
        sjoints = ((sjoints - self.joints_rads_low) / self.joints_rads_diff) * 2 - 1
        return sjoints

    def norm_to_rads(self, action):
        return (np.array(action) * 0.5 + 0.5) * self.joints_rads_diff + self.joints_rads_low

    def demo(self):
        self.reset()
        n_rep = 20

        acts = [[0, 0, 0]]

        while True:
            for a in acts:
                for j in range(n_rep):
                    self.step(a * NUM_LEGS)

    def load_terrain(self, path):

        terrain_collision_shape_id = p.createCollisionShape(shapeType=p.GEOM_MESH,
                                                            fileName=path,
                                                            flags=p.GEOM_FORCE_CONCAVE_TRIMESH |
                                                                  p.GEOM_CONCAVE_INTERNAL_EDGE,
                                                            meshScale=[1.0, 1.0, 1.0],
                                                            physicsClientId=self.client_ID)

        terrain_visual_shape = p.createVisualShape(shapeType=p.GEOM_MESH,
                                                   fileName=path,
                                                   rgbaColor=[38.0/255.0, 148.0/255.0, 208.0/255.0, 1],
                                                   specularColor=[1.0, 1.0, 1.0],
                                                   meshScale=[1.0, 1.0, 1.0],
                                                   physicsClientId=self.client_ID)

        terrain_id = p.createMultiBody(0, terrain_collision_shape_id, terrain_visual_shape, [0, 0, 0], [0, 0, 0, 1], physicsClientId=self.client_ID)

        return terrain_id

    def close(self):
        #p.removeBody(self.plane)
        p.disconnect(physicsClientId=self.client_ID)

    def reset(self):

        self.total_time_rew = 0
        self.total_pos_x_rew = 0
        self.total_pos_y_rew = 0
        self.total_vel_x_rew = 0
        self.total_vel_z_rew = 0
        self.total_yaw_rew = 0
        self.total_pitch_rew = 0
        self.total_roll_rew = 0

        self.total_work = 0
        self.total_acceleration = 0

        self.joint_velocities = 0

        self.step_ctr = 0  # Counts the amount of steps done in the current episode

        self.trajectory = [[np.array([0., 0., 0.]), np.array([0., 0., 0.]), np.array([0., 0., 0.]), np.array([0., 0., 0.]), 0]] * (self.max_steps + 1)

        # Reset the robot to initial position and orientation and null the motors
        joint_init_pos_list = self.norm_to_rads([0] * NUM_JOINTS)
        [p.resetJointState(self.robot, i, joint_init_pos_list[i], 0, physicsClientId=self.client_ID) for i in range(NUM_JOINTS)]
        p.resetBasePositionAndOrientation(self.robot, self.start_position, [0, 0, 0, 1], physicsClientId=self.client_ID)
        p.setJointMotorControlArray(bodyUniqueId=self.robot,
                                    jointIndices=range(NUM_JOINTS),
                                    controlMode=p.POSITION_CONTROL,
                                    targetPositions=[0] * NUM_JOINTS,
                                    forces=[self.max_joint_force] * NUM_JOINTS,
                                    physicsClientId=self.client_ID)

        # Step a few times so stuff settles down
        for i in range(10):
            p.stepSimulation(physicsClientId=self.client_ID)


        # Return initial obs
        obs, _, _, _ = self.step(np.zeros(self.act_dim))
        return obs


class StructuredHexapodEnv(HexapodEnv):

    def __init__(self, terrain=None, parameters=Parameters(), animate=False, max_steps=100, noise_level=0.0, start_position=[0., 0., 0.]):
        super(StructuredHexapodEnv, self).__init__(terrain, parameters, animate, max_steps, noise_level, start_position)

        # Limits of our joints
        self.joints_rads_low = []
        self.joints_rads_high = []
        for l_param in parameters.leg_params:
            self.joints_rads_low += l_param.joint_limits_min
            self.joints_rads_high += l_param.joint_limits_max

        self.joints_rads_low = np.array(self.joints_rads_low)
        self.joints_rads_high = np.array(self.joints_rads_high)

        self.joints_rads_diff = self.joints_rads_high - self.joints_rads_low

    # def step(self, ctrl):
    #
    #     p.setJointMotorControlArray(bodyUniqueId=self.robot,
    #                                 jointIndices=range(NUM_JOINTS),
    #                                 controlMode=p.POSITION_CONTROL,
    #                                 targetPositions=ctrl,
    #                                 forces=[self.max_joint_force] * NUM_JOINTS,
    #                                 positionGains=[0.02] * NUM_JOINTS,
    #                                 velocityGains=[0.1] * NUM_JOINTS,
    #                                 physicsClientId=self.client_ID)
    #
    #     # Step the simulation.
    #     for i in range(self.sim_steps_per_iter):
    #         p.stepSimulation(physicsClientId=self.client_ID)
    #         self.total_work += self.get_work()
    #         self.total_acceleration += self.get_acceleration()
    #
    #     if self.animation_speed != 0.0:
    #         if self.animate: time.sleep(self.animation_speed)
    #
    #     # Get new observations (Note, not all of these are required and used)
    #     torso_pos, torso_quat, torso_vel, torso_angular_vel, joint_angles, joint_velocities, joint_torques, contacts = self.get_obs()
    #     xd, yd, zd = torso_vel # Unpack for clarity
    #     roll, pitch, yaw = p.getEulerFromQuaternion(torso_quat)
    #
    #     self.robot_pos = torso_pos
    #
    #     # reward
    #     pos_x_rew = (torso_pos[0] - self.start_position[0]) * self.pos_x_rew
    #     self.total_pos_x_rew += pos_x_rew
    #
    #     vel_x_rew = xd * self.vel_x_rew
    #     self.total_vel_x_rew += vel_x_rew
    #
    #     time_rew = self.time_rew
    #     self.total_time_rew += time_rew
    #
    #     # penalization
    #     yaw_rew = -np.square(yaw) * self.yaw_rew
    #     self.total_yaw_rew += yaw_rew
    #
    #     roll_rew = -np.square(roll) * self.roll_rew
    #     self.total_roll_rew += roll_rew
    #
    #     pitch_rew = -np.square(pitch) * self.pitch_rew
    #     self.total_pitch_rew += pitch_rew
    #
    #     pos_y_rew = -np.square(torso_pos[1]) * self.pos_y_rew
    #     self.total_pos_y_rew += pos_y_rew
    #
    #     vel_z_rew = -np.square(zd) * self.vel_z_rew
    #     self.total_vel_z_rew += vel_z_rew
    #
    #     r = (pos_x_rew + vel_x_rew + time_rew + yaw_rew + roll_rew + pitch_rew + pos_y_rew + vel_z_rew) / self.max_steps
    #
    #     env_obs = np.concatenate((torso_pos, [roll, pitch, yaw], contacts, joint_angles)).astype(np.float32)
    #     self.step_ctr += 1
    #
    #     # This condition terminates the episode
    #     done = self.step_ctr > self.max_steps or np.abs(roll) > 1.57 or np.abs(pitch) > 1.57
    #
    #     if self.noise_level > 0:
    #         self.noise = (np.random.rand(len(env_obs)).astype(np.float32) * 2.0 - 1.0) * self.noise_level
    #         env_obs = env_obs + self.noise
    #
    #     return env_obs, r, done, {}


class UnstructuredHexapodEnv(HexapodEnv):

    def __init__(self, terrain=None, parameters=Parameters(), animate=False, max_steps=100, noise_level=0.0, start_position=[0., 0., 0.]):
        super(UnstructuredHexapodEnv, self).__init__(terrain, parameters, animate, max_steps, noise_level, start_position)

        # Limits of our joints. When using the * (multiply) operation on a list, it repeats the list that many times
        self.joints_rads_low = np.array([-0.5, -1.8, -1.4] * NUM_LEGS)
        self.joints_rads_high = np.array([0.5, 1.8, 0.8] * NUM_LEGS)

        self.joints_rads_diff = np.array(self.joints_rads_high) - np.array(self.joints_rads_low)

        self.target_vel = 0.4

    def process_input(self, ctrl):

        ctrl_noisy = ctrl + np.random.rand(self.act_dim).astype(np.float32) * 0.01 - 0.005

        ctrl_clipped = np.clip(ctrl_noisy, -1, 1)

        scaled_action = self.norm_to_rads(ctrl_clipped)

        return scaled_action

    def process_output(self, obs):
        scaled_joint_angles = self.rads_to_norm(obs[12:30])
        obs[12:30] = scaled_joint_angles[:]
        return obs[3:]
