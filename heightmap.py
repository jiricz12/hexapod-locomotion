import numpy as np


class Heightmap:
    def __init__(self, size_n, step):
        self.grid = np.array([[-np.inf for _ in range(size_n)] for _ in range(size_n)])
        self.grid_check = np.array([[False for _ in range(size_n)] for _ in range(size_n)])
        self.position = np.array([0., 0., 0.])
        self.step = step
        self.size_n = size_n
        self.corner = np.array([0, 0])
        self.lowest_value = np.inf

    def clear(self):
        self.lowest_value = np.inf
        for row in range(self.size_n):
            for col in range(self.size_n):
                self.grid[row][col] = -np.inf
                self.grid_check[row][col] = False

    def reset(self, position, points_arr):
        self.clear()
        self.position = position
        self.corner = np.array((self.position[0:2] / self.step - self.size_n / 2.0), np.int32)
        for p in points_arr:
            idx = np.array(p[0:2] / self.step - self.corner, np.int32)
            if 0 <= idx[0] < self.size_n and 0 <= idx[1] < self.size_n:
                if self.grid[idx[0]][idx[1]] < p[2]:
                    self.grid[idx[0]][idx[1]] = p[2]
                    self.grid_check[idx[0]][idx[1]] = True
                    self.lowest_value = min(self.lowest_value, p[2])
        updated = True
        i = 0
        #for i in range(10):
        while updated:
            i += 1
            updated = False
            for r in range(self.size_n):
                for c in range(self.size_n):
                    if not self.grid_check[r][c]:
                        new_val = 0
                        num_val = 0
                        for d in [[-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1], [-1, -1]]:
                            new_r = r+d[0]
                            new_c = c+d[1]
                            if 0 <= new_r < self.size_n and 0 <= new_c < self.size_n:
                                if not np.isinf(-self.grid[new_r][new_c]): # self.grid_check[new_r][new_c]:
                                    new_val += self.grid[new_r, new_c]
                                    num_val += 1
                        if num_val > 0:
                            new_val /= num_val
                            if abs(new_val - self.grid[r][c]) > 0.00001:
                                updated = True
                                self.grid[r][c] = new_val
        print(f"Heightmap approximated in {i} iterations.")

    def reset_all_to(self, position, height):
        self.position = position
        self.corner = np.array((self.position[0:2] / self.step - self.size_n / 2.0), np.int32)
        self.lowest_value = height
        for row in range(self.size_n):
            for col in range(self.size_n):
                self.grid[row][col] = height
                self.grid_check[row][col] = True

    def get_coords(self, point):
        return np.array(point[0:2] / self.step - self.corner, np.int32)

    def are_coords_in(self, coords):
        return 0 <= coords[0] < self.size_n and 0 <= coords[1] < self.size_n

    def get_height(self, point):
        idx = np.array(point[0:2] / self.step - self.corner, np.int32)
        if 0 <= idx[0] < self.size_n and 0 <= idx[1] < self.size_n:
            return self.grid[idx[0]][idx[1]]
        print("Invalid point:", point, idx)
        return None

    def set_size(self, size_n):
        self.size_n = size_n
        self.grid = np.array([[-np.inf for _ in range(self.size_n)] for _ in range(self.size_n)])
        self.corner = np.array((self.position / self.step - self.size_n / 2.0), np.int32)


if __name__ == "__main__":
    points = [np.random.random(3) * 2 - 1 for _ in range(50)]
    points.append(np.array([0.0, 0.0, 5]))
    print(points)
    h = Heightmap(5, 0.4)
    h.reset(np.array([0.0, 0.0]), points)
    print(h.corner)
    print(h.grid)
    print(h.get_height(np.array([0.4, 0.4])))