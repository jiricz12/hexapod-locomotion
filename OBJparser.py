import mesh_creator
from mesh_creator import SimplexTerrain
import my_math
import os

def terrain_to_obj(terrain, name):
    f = open(name+".obj", "w")

    points = terrain.get_points()

    for point in points:
        f.write(f"v {point[0]} {point[1]} {point[2]} {1.0}\n")

    f.write("\n")

    def get_idx(x, y):
        return terrain.n_x * y + x + 1

    for y in range(terrain.n_y-1):
        for x in range(terrain.n_x-1):
            f.write(f"f {get_idx(x,y)} {get_idx(x+1,y)} {get_idx(x,y+1)}\n")
            f.write(f"f {get_idx(x+1,y+1)} {get_idx(x,y+1)} {get_idx(x+1,y)}\n")

    f.close()


if __name__ == "__main__":

    t = SimplexTerrain("test", [0., 0., 0.], 10, 10, 0, 0, 0.5, [1.0, 1.0, 0.0])
    terrain_to_obj(t, "test")
