import src.robot
import numpy as np
import pickle
import os


class Policy:

    def __init__(self, name, layers, params=None):

        self.name = name
        self.layers = layers
        self.W = []
        self.num_params = 0
        for i in range(len(layers)-1):
            self.num_params += layers[i+1] * (layers[i] + 1)
            self.W.append(np.zeros((layers[i+1], layers[i] + 1)))

        if params is not None:
            self.set_params(params)

    def set_params(self, w):
        idx = 0
        for i in range(len(self.layers)-1):
            step = self.layers[i+1] * (self.layers[i] + 1)
            self.W[i] = np.array(w[idx:idx+step]).reshape((self.layers[i+1], self.layers[i] + 1))
            idx += step

    def get_params(self):
        res = np.array(self.W[0])
        for i in range(1, len(self.layers)-1):
            res = np.append(res, self.W[i])
        return res.flatten()

    def forward(self, x):
        for i in range(0, len(self.layers)-2):
            x = np.append(x, 1)
            x = self.W[i].dot(x)
            x = np.maximum(0, x)  # ReLU
        x = np.append(x, 1)
        x = self.W[len(self.W)-1].dot(x)
        return x

    def get_num_params(self):
        return self.num_params


def save_policy(path, policy):
    with open(path+"/"+policy.name+".pol", 'wb') as f:
        pickle.dump(policy, f)
        np.save(path+"/weights/"+policy.name, policy.get_params())


def load_policy(path):
    with open(path, 'rb') as f:
        policy = pickle.load(f)
        policy.name = path.split("/")[-1].split(".")[0]
    return policy


if __name__ == "__main__":
    p = Policy("test", [17, 8, 1])
    print(len(p.get_params()))



