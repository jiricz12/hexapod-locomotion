import numpy as np
import nevergrad as ng


class MyRandomSearchOptimizer:

    def __init__(self, w, sigma):
        self.w = w
        self.sigma = sigma
        self._intern_sigma = sigma
        self.number_of_solutions = 15 + int(len(w) / 20)
        self.min_loss = np.inf
        self.first = True

    def ask(self):
        solutions = [self.w + (np.random.rand(len(self.w)) * 2 - 1) * self._intern_sigma for _ in range(self.number_of_solutions)]
        if self.first:
            solutions[0] = self.w
            self.first = False
        return solutions

    def tell(self, solutions, scores):
        min_score = min(scores)
        if min_score < self.min_loss:
            self.min_loss = min_score
            idx = scores.index(min_score)
            self.w = solutions[idx]
            self._intern_sigma = self.sigma
        else:
            self._intern_sigma *= 1.5


class Solution:
    def __init__(self, params):
        self.args = [params.copy()]


class ConservativeSearch:
    def __init__(self, name, params, sigma, solutions_n, budget):
        self.name = "ConservativeSearch"
        self.w = params
        self.sigma = sigma
        self._intern_sigma = sigma
        self.global_solutions_n = solutions_n
        self.local_solutions_n = 5
        self.min_loss = np.inf
        self.first = True
        self.local = False
        self.number_of_global_steps = 3
        self.current_global_step = 0
        self.current_local_w_idx = 0

    def ask(self):
        if not self.local:
            solutions = [Solution(self.w + (np.random.rand(len(self.w)) * 2 - 1) * self._intern_sigma) for _ in range(self.global_solutions_n)]
            if self.first:
                solutions[0].args[0] = self.w.copy()
                self.first = False
            self.current_global_step += 1
            if self.current_global_step >= self.number_of_global_steps:
                self.current_global_step = 0
                self.local = True
        else:
            solutions = [Solution(self.w) for _ in range(self.local_solutions_n)]
            noise = (np.random.rand(self.local_solutions_n) * 2.0 - 1.0) * self._intern_sigma
            for i in range(self.local_solutions_n):
                solutions[i].args[0][self.current_local_w_idx] += noise[i]
            self.current_local_w_idx += 1
            if self.current_local_w_idx >= len(self.w):
                self.current_local_w_idx = 0
                self.local = False
        return solutions

    def tell(self, solutions, scores):
        min_score = min(scores)
        if min_score < self.min_loss:
            self.min_loss = min_score
            idx = scores.index(min_score)
            self.w = solutions[idx].args[0]
            self._intern_sigma = self.sigma
        elif not self.local:
            self._intern_sigma *= 1.5


class Optimizer:
    def __init__(self, name, params, solutions_n, budget):
        self.name = name
        self.params = params
        arrays = []
        for w, sigma in params:
            arr = ng.optimizers.p.Array(init=w)
            #arr.set_standardized_data(np.array(w))
            arr.set_mutation(sigma=sigma)
            arrays.append(arr)
        instru = ng.p.Instrumentation(*arrays)
        self.solutions_n = solutions_n
        self.budget = budget
        self.recommend = False
        if name != "ConservativeSearch":
            self.optim = ng.optimizers.registry[name](parametrization=instru, budget=self.budget)
        else:
            self.optim = ConservativeSearch(name, params, sigma, solutions_n, budget)

    def ask(self):
        if self.name != "ConservativeSearch":
            if self.recommend:
                return [self.optim.recommend()]
            else:
                return [self.optim.ask() for _ in range(self.solutions_n)]
        else:
            return self.optim.ask()

    def tell(self, solutions, scores):
        if self.name != "ConservativeSearch":
            if not self.recommend:
                for i in range(self.solutions_n):
                    self.optim.tell(solutions[i], scores[i])
            self.recommend = not self.recommend
        else:
            self.optim.tell(solutions, scores)

