import visualization
import robot
from policy import Policy
from mesh_creator import SimplexTerrain
from mesh_creator import StepTerrain
from robot import Action
import numpy as np
from src.environment.environments import StructuredHexapodEnv
import os
import policy
import matplotlib
from robot import Parameters
from robot import LegParameters


class Main:

    def __init__(self):

        self.gait_policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "policies/gait_policies")
        self.body_height_policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "policies/body_height_policies")
        self.leg_height_policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "policies/leg_height_policies")
        self.terrains_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "terrains")
        self.parameters_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "parameters")

        self.POLICY_ACTIVE = False
        self.RESET = False

        self.terrain = None
        self.parameters = robot.Parameters()
        self.hexapod = robot.Hexapod(self.parameters)
        self.state = robot.State()
        self.hexapod.reset(self.state.position, self.state.rotation, self.state.joint_angles)
        self.env = None
        self.controller = robot.Controller()
        self.control = robot.Control()

        self.window = visualization.Window()
        self.window.keyboard_handler.add_callback(self.control.proces_keyboard_input)

        self.structured_trainer_sim = []
        self.unstructured_trainer_sim = []
        self.structured_tester_sim = []
        self.unstructured_tester_sim = []
        self.structured_evaluator_sim = []
        self.unstructured_evaluator_sim = []

        #self.testers.append(testing.StructuredTester(None, robot.Parameters(), policy.load_policy(self.policy_path+"/StructuredFlat.pol"), True, 100, False))

    def start(self):

        while not self.window.should_close():
            self.window.begin_render()

            self.window.render_3d(self.hexapod, self.state, None, self.terrain)

            for sim in (*self.structured_trainer_sim, *self.structured_tester_sim, *self.structured_evaluator_sim, *self.unstructured_evaluator_sim):
                sim.step(self.window.ID)
                if sim.render:
                    self.window.render_3d(sim.hexapod if sim.render_hexapod else None,
                                          sim.alg_state,
                                          sim.heightmap if sim.render_heightmap else None,
                                          None if not sim.render_terrain else sim.terrain)

            for sim in (*self.unstructured_trainer_sim, *self.unstructured_tester_sim):
                sim.step()
            self.window.render_trajectories()

            self.window.render_gui(self)
            self.window.render_gui_3d_properties()
            self.window.end_render()

        self.window.clean_up()


if __name__ == "__main__":
    main = Main()
    main.start()
