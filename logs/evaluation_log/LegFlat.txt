Evaluation time [s]: 0:2:22
Terrain: Flat
Starting position: [0.0, 0.0, 0.25]
Number of iterations: 300
Evaluation iterations: 50
Delta time: 1.0
Noise level: 0.001

Gait policy: Raw algorithm
Body height policy: Raw algorithm
Leg height policy: Flat
Distance: 54.25279651047433
Smoothness: -22.38236507758923
Smoothness per distance: -41.25568913902525
Power consumption: 41.33895356062217
Power consumption per distance: 76.19690821401447
54.25 & -41.26 & 76.20 & -22.38 & 41.34
