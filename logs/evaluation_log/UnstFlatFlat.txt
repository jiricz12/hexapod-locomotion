Evaluation time [s]: 0:11:35
Evaluation iterations: 50
Number of iterations: 300
Noise level: 0.001
Start position: [0.0, 0.0, 0.25]
Terrain: Flat
Hexapod: Default
Policy:
Name: UnstFlat
Layers: [51, 18]
Number of parameters: 936

Distance: 135.21372228432622
Smoothness: -458.6084346762169
Smoothness per distance: -3.391729973322228
Power consumption: 83.67030632534102
Power consumption per distance: 0.6188004065844726
135.21 & -339.17 & 61.88 & -458.61 & 83.67
