Evaluation time [s]: 0:12:37
Terrain: Rocks
Starting position: (-1.940000057220459, 0.0, 0.3499999940395355)
Number of iterations: 300
Evaluation iterations: 50
Delta time: 1.0
Noise level: 0.001

Gait policy: Raw algorithm
Body height policy: Rocks
Leg height policy: Raw algorithm
Distance: 17.105870433682064
Smoothness: -42.723006430562435
Smoothness per distance: -249.7564014423921
Power consumption: 44.69091880657845
Power consumption per distance: 261.2607115191312
17.11 & -249.76 & 261.26 & -42.72 & 44.69
