Evaluation time [s]: 0:11:21
Terrain: Flat
Starting position: [0.0, 0.0, 0.25]
Number of iterations: 300
Evaluation iterations: 50
Delta time: 1.0
Noise level: 0.001

Gait policy: Flat
Body height policy: Raw algorithm
Leg height policy: Raw algorithm
Distance: 62.01223283597861
Smoothness: -21.898991273526377
Smoothness per distance: -35.31398608311503
Power consumption: 39.69032449324698
Power consumption per distance: 64.00402417088783
62.01 & -35.31 & 64.00 & -21.90 & 39.69
