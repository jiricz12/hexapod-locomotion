Evaluation time [s]: 0:12:40
Terrain: Bumps2
Starting position: (-0.8999999761581421, 0.3799999952316284, 0.30000001192092896)
Number of iterations: 300
Evaluation iterations: 50
Delta time: 0.3
Noise level: 0.005

Gait policy: GaitBumps2
Body height policy: Raw algorithm
Leg height policy: Raw algorithm
Distance: 2.2168573915958403
Smoothness: 3.9190177211547255
Power consumption: 66.74660979764677
Power consumption per distance: 30.10866195123095
2.22 & 3.92 & 66.75
