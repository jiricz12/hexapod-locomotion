Evaluation time [s]: 0:12:37
Terrain: Rocks
Starting position: (-1.940000057220459, 0.0, 0.3499999940395355)
Number of iterations: 300
Evaluation iterations: 50
Delta time: 1.0
Noise level: 0.001

Gait policy: Rocks
Body height policy: Raw algorithm
Leg height policy: Raw algorithm
Distance: 19.623155713223927
Smoothness: -44.79920894893176
Smoothness per distance: -228.29767853669858
Power consumption: 46.4614315539228
Power consumption per distance: 236.7683986863169
19.62 & -228.30 & 236.77 & -44.80 & 46.46
