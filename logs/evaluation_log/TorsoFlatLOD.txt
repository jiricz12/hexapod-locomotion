Evaluation time [s]: 0:11:21
Terrain: Flat
Starting position: [0.0, 0.0, 0.25]
Number of iterations: 300
Evaluation iterations: 50
Delta time: 1.0
Noise level: 0.001

Gait policy: Raw algorithm
Body height policy: Flat
Leg height policy: Raw algorithm
Distance: 46.04920897152623
Smoothness: -15.504328087435837
Smoothness per distance: -33.66904325549367
Power consumption: 39.562142625380865
Power consumption per distance: 85.91275183433328
46.05 & -33.67 & 85.91 & -15.50 & 39.56
