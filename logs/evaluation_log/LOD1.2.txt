Evaluation time [s]: 0:8:25
Terrain: Flat
Starting position: [0.0, 0.0, 0.25]
Number of iterations: 400
Evaluation iterations: 25
Delta time: 1.2000000476837158
Noise level: 0.001

Gait policy: Raw algorithm
Body height policy: Raw algorithm
Leg height policy: Raw algorithm
Distance: 37.52846008881937
Smoothness: -23.42702090296673
Smoothness per distance: -62.4246794233537
Power consumption: 40.45122218080489
Power consumption per distance: 107.78812156179112
37.53 & -62.42 & 107.79 & -23.43 & 40.45
