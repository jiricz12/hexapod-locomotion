Evaluation time [s]: 0:6:53
Terrain: Flat
Starting position: [0.0, 0.0, 0.25]
Number of iterations: 400
Evaluation iterations: 25
Delta time: 0.5
Noise level: 0.001

Gait policy: Raw algorithm
Body height policy: Raw algorithm
Leg height policy: Raw algorithm
Distance: 33.35841712369619
Smoothness: -6.190699836466832
Smoothness per distance: -18.5581342589222
Power consumption: 31.92548762378404
Power consumption per distance: 95.7044439650757
33.36 & -18.56 & 95.70 & -6.19 & 31.93
