\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = -3.0015879772617797, xmax = 0.34152754390550905,
			ymin = -0.06665175830063645, ymax = 1.9746268183718878,
			xtick distance = 0.40,
			ytick distance = 0.21,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/GaitBumps_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:GaitBumps}
\end{figure}
