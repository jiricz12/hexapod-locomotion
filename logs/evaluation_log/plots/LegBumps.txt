\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = -3.0194689105841106, xmax = 0.15256008739193652,
			ymin = -0.40841784677494636, ymax = 0.36085879100265555,
			xtick distance = 0.38,
			ytick distance = 0.08,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/LegBumps_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:LegBumps}
\end{figure}
