\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = -1.2359999656677245, xmax = -0.4606029693903497,
			ymin = -0.019035323133793774, ymax = 0.644500846874299,
			xtick distance = 0.09,
			ytick distance = 0.07,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/RawSteps_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:RawSteps}
\end{figure}
