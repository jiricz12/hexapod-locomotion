\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = -1.5479999542236327, xmax = -0.4702855323083176,
			ymin = -0.38694876272042006, ymax = 0.17989118734050794,
			xtick distance = 0.13,
			ytick distance = 0.06,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/GaitTorsoLegSteps_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:GaitTorsoLegSteps}
\end{figure}
