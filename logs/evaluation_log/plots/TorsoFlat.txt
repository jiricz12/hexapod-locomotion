\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = -0.00029245996622777256, xmax = 3.637021904077872,
			ymin = -0.0002902812872265893, ymax = 1.1949538618266968,
			xtick distance = 0.43,
			ytick distance = 0.12,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/TorsoFlat_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:TorsoFlat}
\end{figure}
