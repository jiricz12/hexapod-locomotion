\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = -3.0039504418506, xmax = 0.8202280219363208,
			ymin = -0.011319713909699481, ymax = 2.09087831419337,
			xtick distance = 0.46,
			ytick distance = 0.22,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/GaitTorsoLegBumps_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:GaitTorsoLegBumps}
\end{figure}
