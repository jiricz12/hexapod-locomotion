\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = 0, xmax = 1049,
			ymin = -0.28982186604354543, ymax = 47.10610902261361,
			xtick distance = 87.50,
			ytick distance = 5.92,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Epoch number},
			ylabel = {Score},
			legend pos=south east
		]
			\addplot[
				smooth,
				thick,
				blue
			] file[skip first] {data/UnstNN1_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:UnstNN1}
\end{figure}
