\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = 0, xmax = 104,
			ymin = 3.8142304159227516, ymax = 12.592987915368424,
			xtick distance = 8.75,
			ytick distance = 1.10,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/GaitTorsoLegSteps_plot_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:GaitTorsoLegSteps_plot}
\end{figure}
