\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = 0, xmax = 344,
			ymin = 0.5106031875440598, ymax = 34.3495554200037,
			xtick distance = 28.75,
			ytick distance = 4.23,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/GaitTorsoLegGaitRocks_plot_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:GaitTorsoLegGaitRocks_plot}
\end{figure}
