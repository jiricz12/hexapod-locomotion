\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = 0, xmax = 134,
			ymin = 0.014102809241323677, ymax = 24.092667947086507,
			xtick distance = 11.25,
			ytick distance = 3.01,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/GaitTorsoLegBigBumpsStuck_plot_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:GaitTorsoLegBigBumpsStuck_plot}
\end{figure}
