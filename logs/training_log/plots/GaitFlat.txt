\begin{figure}
	\centering
	\begin{tikzpicture}
		\begin{axis}[
			xmin = 0, xmax = 5930,
			ymin = -7.5468116998672485, ymax = 14.90032821893692,
			xtick distance = 494.25,
			ytick distance = 2.81,
			grid = both,
			minor tick num = 6,
			major grid style = {lightgray},
			minor grid style = {lightgray!25},
			width = \textwidth,
			height = 0.5\textwidth,
			xlabel = {Episode number},
			ylabel = {Reward},
			legend pos=south east
		]
			\addplot[
				thick,
				blue
			] file[skip first] {data/GaitFlat_data.dat};

			\legend{Plot from file}
		\end{axis}
	\end{tikzpicture}
	\caption{Caption}
	\label{fig:GaitFlat}
\end{figure}
