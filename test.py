import numpy as np
points = [np.array([0,0,0]),
          np.array([1,0,5]),
          np.array([0,1,1]),
          np.array([1,1,1]),
          np.array([0,2,2]),
          np.array([1,2,2])
          ]

res1 = np.array([0., 0., 0.])
for i in range(len(points)):
    for j in range(len(points)):
        for k in range(len(points)):
            if i != j and i != k and j != k:
                vec1 = (points[i] - points[j])
                vec2 = (points[k] - points[j])
                n = np.cross(vec1, vec2)
                if n[2] < 0:
                    res1 -= n
                else:
                    res1 += n

print("Res1:", res1 / np.linalg.norm(res1))

median = np.array([0., 0., 0.])
for p in points:
    median += p
median /= len(points)
res2 = np.array([0., 0., 0.])
for i in range(len(points)):
    for j in range(len(points)):
        if i != j:
            vec1 = points[i] - median
            vec2 = points[j] - median
            n = np.cross(vec1, vec2)
            if n[2] < 0:
                res2 -= n
            else:
                res2 += n

print("Res2:", res2 / np.linalg.norm(res2))
