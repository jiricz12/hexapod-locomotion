import glfw
import numpy as np
from OpenGL.GL import *
import time
import my_math
import renderer
import imgui
from imgui.integrations.glfw import GlfwRenderer

import mesh_creator

WINDOW_WIDTH = 1800
WINDOW_HEIGHT = 1080
MENU_WIDTH = 600


class VisualParameters:

    def __init__(self):
        self.leg_params = [LegVisualParameters(l_id) for l_id in ("lf", "lm", "lr", "rf", "rm", "rr")]
        self.render_terrain = True
        self.terrain_immediate_changes = False
        self.render_heightmap = False
        self.render_simulation = False
        self.render_range = True
        self.render_coordinates = False
        self.key_control = True
        self.render_wire = False
        self.render_hexapod = True
        self.render_hexapod_body = True
        self.body_alpha = 1.0
        self.leg_alpha = 1.0
        self.render_legs = True
        self.render_position = True
        self.render_target = True
        self.render_trajectory = False
        self.render_trajectory_lines = True
        self.render_trajectory_points = False
        self.trajectory_point_size = 1
        self.terrain_point_size = 3
        self.leg_thickness = 0.012
        self.joint_size = 0.02
        self.background_color = [0.2, 0.2, 0.2]
        self.light_position = np.array([100.0, np.pi / 4.0, np.pi / 4.0])
        self.light_color = [1.0, 1.0, 1.0]
        self.body_size = [0.202, 0.12, 0.07]
        self.body_color = [1.0, 0.5, 0.31]
        self.coordinate_system_thickness = 0.007
        self.coordinate_system_length = 0.06
        self.stance_color = [1.0, 1.0, 0.0]
        self.stuck_color = [1.0, 0.0, 0.0]
        self.swing_color = [0.0, 1.0, 0.0]
        self.joint_color = [1.0, 1.0, 1.0]
        self.terrain_color = [1.0, 1.0, 1.0]
        self.center_color = [1.0, 1.0, 0.0]
        self.range_color = [1.0, 1.0, 0.0]
        self.heightmap_color = [1.0, 0.5, 0.0]


class LegVisualParameters:
    def __init__(self, l_id):
        self.id = l_id
        self.render = False
        self.render_range = False
        self.render_coordinates = False


class Window:

    def __init__(self):
        self.ID = self.impl_glfw_init()
        imgui.create_context()
        self.impl = GlfwRenderer(self.ID)
        self.renderer = renderer.Renderer()
        self.font = imgui.get_io().fonts.add_font_from_file_ttf("fonts/Ubuntu-Regular.ttf", 20)
        self.impl.refresh_font_texture()

        self.keyboard_handler = KeyboardHandler(self.impl)
        self.mouse_button_handler = MouseButtonHandler(self.impl)
        self.mouse_pos_handler = MousePositionHandler(self.impl)
        self.scroll_handler = MouseScrollHandler(self.impl)
        glfw.set_key_callback(self.ID, self.keyboard_handler.keyboard_callback)
        glfw.set_mouse_button_callback(self.ID, self.mouse_button_handler.mouse_button_callback)
        glfw.set_cursor_pos_callback(self.ID, self.mouse_pos_handler.mouse_pos_callback)
        glfw.set_scroll_callback(self.ID, self.scroll_handler.scroll_callback)
        glfw.set_window_size_callback(self.ID, self.resize_callback)

        self.mouse_pos_handler.add_callback(self.renderer.camera.mouse_position_callback)
        self.scroll_handler.add_callback(self.renderer.camera.mouse_scroll_callback)

        self.v_par = VisualParameters()

        self.raw_cube = mesh_creator.create_cube(1.0, 1.0, 1.0)
        self.raw_circle = mesh_creator.create_circle(1.0, 1000)
        self.raw_offset_cube_x = mesh_creator.create_offsetted_cube(0.0, 1.0, -0.5, 0.5, -0.5, 0.5)

        self.last_time = 0
        self.frames = 0
        self.fps = 0

    def render_3d(self, hexapod, state, heightmap, terrain):

        glViewport(MENU_WIDTH, 0, max(WINDOW_WIDTH-MENU_WIDTH, 1), WINDOW_HEIGHT)

        if self.v_par.render_simulation:

            self.renderer.prepare_to_render(self.renderer.simple_shader, self.v_par, max(WINDOW_WIDTH - MENU_WIDTH, 1), WINDOW_HEIGHT)

            if state is not None and hexapod is not None:
                if self.v_par.render_coordinates:
                    t_mat = my_math.create_trans_mat(state.position, state.rotation, 1.0)
                    self.renderer.render_coordinate_system(t_mat, self.v_par.coordinate_system_thickness, self.v_par.coordinate_system_length, 2.0)

                for i in range(len(hexapod.legs)):
                    if self.v_par.render_coordinates or self.v_par.leg_params[i].render_coordinates:
                        leg = hexapod.legs[i]
                        self.renderer.render_coordinate_system(leg.mount_mat, self.v_par.coordinate_system_thickness, self.v_par.coordinate_system_length)
                        self.renderer.render_coordinate_system(leg.hip_mat, self.v_par.coordinate_system_thickness, self.v_par.coordinate_system_length)
                        self.renderer.render_coordinate_system(leg.knee_mat, self.v_par.coordinate_system_thickness, self.v_par.coordinate_system_length)
                        self.renderer.render_coordinate_system(leg.foot_mat, self.v_par.coordinate_system_thickness, self.v_par.coordinate_system_length)

                if hexapod is not None and self.v_par.render_hexapod:
                    for i in range(len(hexapod.legs)):
                        leg = hexapod.legs[i]
                        glPointSize(4.0)
                        if self.v_par.render_range or self.v_par.leg_params[i].render_range:
                            self.renderer.render_simple_entity(self.raw_circle, self.v_par.range_color, leg.center, np.array([0., 0., 0.]), leg.params.range, None,
                                                               GL_POINTS)
                        glPointSize(1.0)
            if terrain is not None:
                if self.v_par.render_terrain:
                    glPointSize(self.v_par.terrain_point_size)
                    self.renderer.render_simple_entity(terrain, self.v_par.terrain_color, [0., 0., 0.], [0., 0., 0.], 1., None, GL_POINTS)
                    glPointSize(1.0)

            if self.v_par.render_wire:
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

            self.renderer.prepare_to_render(self.renderer.basic_shader, self.v_par, max(WINDOW_WIDTH - MENU_WIDTH, 1), WINDOW_HEIGHT)

            if heightmap is not None:
                self.renderer.render_heightmap(heightmap, self.v_par.heightmap_color, 1.0)

            if state is not None and hexapod is not None:
                if self.renderer.camera.lock_to_focus_point:
                    self.renderer.camera.set_focus_point(state.position)

                if self.v_par.render_hexapod:
                    if self.v_par.render_hexapod_body:
                        self.renderer.render_entity(self.raw_cube, self.v_par.body_color, state.position, state.rotation,
                                                    self.v_par.body_size, None, self.v_par.body_alpha)

                    l_thick = self.v_par.leg_thickness
                    j_size = self.v_par.joint_size
                    for i in range(len(hexapod.legs)):
                        if self.v_par.render_legs or self.v_par.leg_params[i].render:
                            leg = hexapod.legs[i]
                            a = self.v_par.leg_alpha
                            leg_color = self.v_par.stuck_color if leg.swing == -1.0 and leg.stuck else self.v_par.stance_color if leg.swing == -1.0 else self.v_par.swing_color
                            self.renderer.render_entity(self.raw_cube, self.v_par.joint_color, [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], j_size, leg.mount_mat, a)
                            self.renderer.render_entity(self.raw_offset_cube_x, leg_color, [0.0, 0.0, 0.0],
                                                        [0.0, 0.0, leg.angles[0] + leg.params.dh_offsets[0]], [leg.params.seg_lens[0], l_thick, l_thick], leg.mount_mat, a)
                            self.renderer.render_entity(self.raw_cube, self.v_par.joint_color, [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], j_size, leg.hip_mat, a)
                            self.renderer.render_entity(self.raw_offset_cube_x, leg_color, [0.0, 0.0, 0.0],
                                                        [0.0, 0.0, leg.angles[1] + leg.params.dh_offsets[1]], [leg.params.seg_lens[1], l_thick, l_thick], leg.hip_mat, a)
                            self.renderer.render_entity(self.raw_cube, self.v_par.joint_color, [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], j_size, leg.knee_mat, a)
                            self.renderer.render_entity(self.raw_offset_cube_x, leg_color, [0.0, 0.0, 0.0],
                                                        [0.0, 0.0, leg.angles[2] + leg.params.dh_offsets[2]], [leg.params.seg_lens[2], l_thick, l_thick], leg.knee_mat, a)
                            foot_color = [1.0, 0.0, 0.0] if leg.on_range_edge else self.v_par.joint_color
                            self.renderer.render_entity(self.raw_cube, foot_color, [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], j_size, leg.foot_mat, a)
                            if self.v_par.render_target:
                                self.renderer.render_entity(self.raw_cube, [1.0, 0.0, 0.0], leg.target, np.array([0.0, 0.0, 0.0]), 0.015)
                            if self.v_par.render_position:
                                self.renderer.render_entity(self.raw_cube, [0.0, 0.0, 1.0], leg.position, np.array([0.0, 0.0, 0.0]), 0.02)
                            if state is not None:
                                if self.v_par.render_range or self.v_par.leg_params[i].render_range:
                                    self.renderer.render_entity(self.raw_cube, self.v_par.center_color, leg.center, state.rotation, 0.010)

            if self.v_par.render_wire:
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)

    def render_trajectories(self):
        self.renderer.prepare_to_render(self.renderer.simple_shader, self.v_par, max(WINDOW_WIDTH - MENU_WIDTH, 1), WINDOW_HEIGHT)

        if self.v_par.render_trajectory:
            if self.v_par.render_trajectory_lines:
                self.renderer.render_simple_entities(self.renderer.trajectories, GL_LINES)
            if self.v_par.render_trajectory_points:
                glPointSize(self.v_par.trajectory_point_size)
                self.renderer.render_simple_entities(self.renderer.trajectories, GL_POINTS)
                glPointSize(1.0)

    def begin_render(self):

        glfw.poll_events()

        self.impl.process_inputs()

        self.frames += 1
        now = time.time()
        if now - self.last_time >= 1.0:
            self.fps = self.frames
            self.frames = 0
            self.last_time = now
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    def render_gui(self, main):

        glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT)

        imgui.new_frame()
        imgui.push_font(self.font)

        #imgui.show_test_window()

        if self.v_par.render_simulation:
            imgui.set_next_window_size(MENU_WIDTH, WINDOW_HEIGHT)
        else:
            imgui.set_next_window_size(WINDOW_WIDTH, WINDOW_HEIGHT)

        imgui.set_next_window_position(0, 0)
        imgui.begin("Custom window", False, imgui.WINDOW_NO_MOVE | imgui.WINDOW_ALWAYS_AUTO_RESIZE | imgui.WINDOW_ALWAYS_HORIZONTAL_SCROLLBAR)

        changed, value = imgui.checkbox("Render simulation", self.v_par.render_simulation)
        if changed:
            self.v_par.render_simulation = value
        if self.v_par.render_simulation:
            imgui.same_line()
            changed, value = imgui.checkbox("Wire", self.v_par.render_wire)
            if changed:
                self.v_par.render_wire = value
        imgui.same_line()
        imgui.text(f"FPS: {self.fps}")

        imgui.separator()
        self.renderer.render_gui(main, self.v_par)

    def render_gui_3d_properties(self):
        if self.v_par.render_simulation:
            self.renderer.render_gui_3d_properties(self.v_par)

    def end_render(self):
        imgui.pop_font()
        imgui.end()
        imgui.render()
        self.impl.render(imgui.get_draw_data())
        glfw.swap_buffers(self.ID)

    def impl_glfw_init(self):

        if not glfw.init():
            print("Could not initialize OpenGL context")
            exit(1)

        # OS X supports only forward-compatible core profiles from 3.2
        glfw.window_hint(glfw.CONTEXT_VERSION_MAJOR, 3)
        glfw.window_hint(glfw.CONTEXT_VERSION_MINOR, 3)
        glfw.window_hint(glfw.OPENGL_PROFILE, glfw.OPENGL_CORE_PROFILE)

        glfw.window_hint(glfw.OPENGL_FORWARD_COMPAT, GL_TRUE)

        # Create a windowed mode window and its OpenGL context
        window = glfw.create_window(
            int(WINDOW_WIDTH), int(WINDOW_HEIGHT), "OpenGL", None, None
        )

        if not window:
            glfw.terminate()
            print("Could not initialize Window")
            exit(1)

        glfw.make_context_current(window)

        glfw.swap_interval(1)

        return window

    def clean_up(self):
        self.impl.shutdown()
        glfw.terminate()

    def should_close(self):
        return glfw.window_should_close(self.ID)

    def resize_callback(self, window, w, h):
        self.impl.resize_callback(window, w, h)
        global WINDOW_WIDTH
        global WINDOW_HEIGHT
        WINDOW_WIDTH = w
        WINDOW_HEIGHT = h
        glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT)


class CallbackHandler:

    def __init__(self, imgui_renderer):
        self.imgui_renderer = imgui_renderer
        self.callbacks = []

    def add_callback(self, cb):
        self.callbacks.append(cb)

    def remove_callback(self, cb):
        self.callbacks.remove(cb)


class KeyboardHandler(CallbackHandler):

    def __init__(self, imgui_renderer):
        super(KeyboardHandler, self).__init__(imgui_renderer)
        self.keys_down = [False] * 512

    def keyboard_callback(self, window, key, scancode, action, mods):
        self.imgui_renderer.keyboard_callback(window, key, scancode, action, mods)
        if action == glfw.PRESS:
            self.keys_down[key] = True
        elif action == glfw.RELEASE:
            self.keys_down[key] = False
        for cb in self.callbacks:
            cb(key, scancode, action)

    def is_button_down(self, key):
        return self.keys_down[key]


class MouseButtonHandler(CallbackHandler):

    def __init__(self, imgui_renderer):
        super(MouseButtonHandler, self).__init__(imgui_renderer)
        self.buttons_down = [False] * 10

    def mouse_button_callback(self, window, button, action, mods):
        if action == glfw.PRESS:
            self.buttons_down[button] = True
        elif action == glfw.RELEASE:
            self.buttons_down[button] = False
        for cb in self.callbacks:
            cb(button, action, mods)

    def is_button_down(self, button):
        return self.buttons_down[button]


class MousePositionHandler(CallbackHandler):

    def __init__(self, imgui_renderer):
        super(MousePositionHandler, self).__init__(imgui_renderer)
        self.pos = np.array([0., 0.])
        self.last = np.array([0., 0.])

    def mouse_pos_callback(self, window, x, y):
        self.imgui_renderer.mouse_callback(window, x, y)
        self.pos[0] = x
        self.pos[1] = y
        vel = self.pos - self.last
        self.last = self.pos.copy()
        for cb in self.callbacks:
            cb(window, x, y, vel[0], vel[1])

    def get_mouse_position(self):
        return self.pos


class MouseScrollHandler(CallbackHandler):

    def __init__(self, imgui_renderer):
        super(MouseScrollHandler, self).__init__(imgui_renderer)

    def scroll_callback(self, window, x_offset, y_offset):
        self.imgui_renderer.scroll_callback(window, x_offset, y_offset)
        for cb in self.callbacks:
            cb(window, x_offset, y_offset)
