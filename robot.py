import copy

import glfw

import pickle
from src import my_math
from enum import Enum
import numpy as np


class ControlInput(Enum):
    NOTHING = 0
    FORWARD = 1
    BACKWARD = 2
    LEFT = 3
    RIGHT = 4
    TURN_LEFT = 5
    TURN_RIGHT = 6


class Control:

    def __init__(self):
        self.ctrl = ControlInput.NOTHING

    def proces_keyboard_input(self, key, scancode, action):
        if action == glfw.PRESS:
            if key == glfw.KEY_W:
                self.ctrl = ControlInput.FORWARD
            if key == glfw.KEY_S:
                self.ctrl = ControlInput.BACKWARD
            if key == glfw.KEY_A:
                self.ctrl = ControlInput.LEFT
            if key == glfw.KEY_D:
                self.ctrl = ControlInput.RIGHT
        if action == glfw.RELEASE:
            if key == glfw.KEY_W and self.ctrl == ControlInput.FORWARD:
                self.ctrl = ControlInput.NOTHING
            if key == glfw.KEY_S and self.ctrl == ControlInput.BACKWARD:
                self.ctrl = ControlInput.NOTHING
            if key == glfw.KEY_A and self.ctrl == ControlInput.LEFT:
                self.ctrl = ControlInput.NOTHING
            if key == glfw.KEY_D and self.ctrl == ControlInput.RIGHT:
                self.ctrl = ControlInput.NOTHING


class Controller:

    def __init__(self):
        self.default_body_height = 0.15
        self.default_leg_height = 0.05

    def get_action(self, hexapod, ctrl, orientation, contacts, gait, window=None):
        act = Action()
        dirs = self.get_directions(orientation)
        if window is not None:
            ctrl = self.get_input(window)
        self.proces_input(act, ctrl, dirs, window)
        act.torso_height = self.default_body_height
        #act.rot_dir[2] -= orientation[2] * 0.1

        for i in range(len(hexapod.legs)):
            leg = hexapod.legs[i]
            l_act = act.leg_actions[i]
            l_act.swing = hexapod.legs[i].swing
            l_act.lift_height = self.default_leg_height
            if gait:
                pos_to_target = leg.target - leg.position
                pos_to_target_len = np.linalg.norm(pos_to_target)
                if leg.phase == 2 and contacts[i] > 0:
                    l_act.swing = -1.0
                if pos_to_target_len < leg.params.tolerance:
                    l_act.swing = -1.0

        if gait:
            grp1_sw = (np.array([hexapod.legs[0].swing, hexapod.legs[2].swing, hexapod.legs[4].swing]) > 0).max()
            grp2_sw = (np.array([hexapod.legs[1].swing, hexapod.legs[3].swing, hexapod.legs[5].swing]) > 0).max()
            grp1_st = np.array([hexapod.legs[0].is_stuck(act), hexapod.legs[2].is_stuck(act), hexapod.legs[4].is_stuck(act)]).max()
            grp2_st = np.array([hexapod.legs[1].is_stuck(act), hexapod.legs[3].is_stuck(act), hexapod.legs[5].is_stuck(act)]).max()
            grp1_ot = np.array([hexapod.legs[0].on_target(), hexapod.legs[2].on_target(), hexapod.legs[4].on_target()]).min()
            grp2_ot = np.array([hexapod.legs[1].on_target(), hexapod.legs[3].on_target(), hexapod.legs[5].on_target()]).min()

            # print("grp1_sw:", grp1_sw, end=" ")
            # print("grp2_sw:", grp2_sw, end=" ")
            # print("grp1_st:", grp1_st, end=" ")
            # print("grp2_st:", grp2_st, end=" ")
            # print()
            if grp1_st and not grp2_sw:
                self.swing(act, [0, 2, 4])
            elif grp2_st and not grp1_sw:
                self.swing(act, [1, 3, 5])
            # elif not grp1_sw and not grp2_sw:
            #     if (act.move_dir == [0., 0.]).max():
            #         if not grp1_ot:
            #             self.swing(act, [0, 2, 4])
            #         elif not grp2_ot:
            #             self.swing(act, [1, 3, 5])

        return act

    def swing(self, act, idx):
        for i in idx:
            act.leg_actions[i].swing = 1.0

    def proces_input(self, act, ctrl, dirs, window):
        act.move_dir = np.array([0., 0.])
        act.rot_dir = np.array([0., 0., 0.])

        if ctrl == ControlInput.FORWARD:
            act.move_dir = dirs[0]
        elif ctrl == ControlInput.BACKWARD:
            act.move_dir = -dirs[0]
        elif ctrl == ControlInput.LEFT:
            act.move_dir = dirs[1]
        elif ctrl == ControlInput.RIGHT:
            act.move_dir = -dirs[1]
        elif ctrl == ControlInput.TURN_RIGHT:
            act.rot_dir[2] = -1
        elif ctrl == ControlInput.TURN_LEFT:
            act.rot_dir[2] = 1

        if act.move_dir[0] != 0 or act.move_dir[1] != 0:
            act.move_dir = my_math.normalize(act.move_dir)

    def get_input(self, window):
        ctrl = ControlInput.NOTHING
        if glfw.get_key(window, glfw.KEY_LEFT_SHIFT) == glfw.PRESS:
            if glfw.get_key(window, glfw.KEY_A) == glfw.PRESS:
                ctrl = ControlInput.TURN_LEFT
            if glfw.get_key(window, glfw.KEY_D) == glfw.PRESS:
                ctrl = ControlInput.TURN_RIGHT
        else:
            if glfw.get_key(window, glfw.KEY_W) == glfw.PRESS:
                ctrl = ControlInput.FORWARD
            elif glfw.get_key(window, glfw.KEY_S) == glfw.PRESS:
                ctrl = ControlInput.BACKWARD
            elif glfw.get_key(window, glfw.KEY_A) == glfw.PRESS:
                ctrl = ControlInput.LEFT
            elif glfw.get_key(window, glfw.KEY_D) == glfw.PRESS:
                ctrl = ControlInput.RIGHT
        return ctrl

    def get_directions(self, orientation):
        mat = my_math.create_trans_mat([0., 0., 0.], orientation, 1.0)
        fwd = my_math.transform(mat, np.array([1., 0., 0.]))[0:2]
        left = my_math.transform(mat, np.array([0., 1., 0.]))[0:2]
        return [fwd, left]


class Action:

    def __init__(self, arr=None):
        if arr is not None:
            arr = np.array(arr)
            self.move_dir = arr[0:2]
            self.rot_dir = arr[2:5]
            self.torso_height = arr[5]
            self.leg_actions = []
            for i in range(6):
                self.leg_actions.append(LegAction(arr[6 + i * 2:6 + (i + 1) * 2]))
        else:
            self.move_dir = np.array([0., 0.])
            self.rot_dir = np.array([0., 0., 0.])
            self.torso_height = 0.15
            self.leg_actions = [LegAction() for _ in range(6)]

    def as_vector(self):
        arr = [*self.move_dir, *self.rot_dir, self.torso_height]
        for i in range(6):
            arr += self.leg_actions[i].as_vector()
        return arr


class LegAction:

    def __init__(self, arr=None):
        if arr is not None:
            self.swing = arr[0]
            self.lift_height = arr[1]
        else:
            self.lift_height = 0.05
            self.swing = -1

    def as_vector(self):
        return [self.swing, self.lift_height]


class State:
    def __init__(self, position=np.array([0., 0., 0.]), rotation=np.array([0., 0., 0.]), joint_angles=np.array([0.0 for _ in range(18)])):
        self.position = position
        self.rotation = rotation
        self.joint_angles = joint_angles


class Parameters:
    def __init__(self):
        self.name = "Default"
        self.leg_params = []
        self.leg_params.append(LegParameters("lf", np.deg2rad(45), [0.12, 0.062, 0.001]))
        self.leg_params.append(LegParameters("lm", np.deg2rad(90), [0.0, 0.1, 0.001]))
        self.leg_params.append(LegParameters("lr", np.deg2rad(135), [-0.12, 0.062, 0.001]))
        self.leg_params.append(LegParameters("rf", np.deg2rad(-45), [0.12, -0.062, 0.001]))
        self.leg_params.append(LegParameters("rm", np.deg2rad(-90), [0.0, -0.1, 0.001]))
        self.leg_params.append(LegParameters("rr", np.deg2rad(-135), [-0.12, -0.062, 0.001]))
        self.body_speed = 0.05
        self.body_turn_speed = 0.1
        self.body_height_above_terrain = 0.2
        self.body_size = [0.2, 0.2, 0.1]


class LegParameters:
    def __init__(self, leg_id, def_orientation, def_mount):
        self.id = leg_id
        self.range = 0.07
        self.def_orientation = def_orientation
        self.def_mount = def_mount
        self.def_center = np.array([0.20, 0.0, -0.172])
        self.seg_lens = np.array([0.0630, 0.1, 0.160])
        self.dh_offsets = np.array([0, 0, np.deg2rad(90)])
        self.speed = 0.05
        self.tolerance = 0.015
        self.leg_thickness = 0.03
        self.joint_limits_max = [np.pi, np.pi, np.pi]
        self.joint_limits_min = [-np.pi, -np.pi, -np.pi]


def save_parameters(path, params):
    with open(path + "/" + params.name + ".par", "wb") as f:
        pickle.dump(params, f)


def load_parameters(path):
    with open(path, "rb") as f:
        params = pickle.load(f)
        params.name = path.split("/")[-1].split(".")[0]
    return params


class Hexapod:

    def __init__(self, params):

        self.base_mat = None

        self.legs = []
        self.params = params

        for param in params.leg_params:
            self.legs.append(Leg(param))

        self.forward = np.array([1., 0., 0.])
        self.left = np.array([0., 1., 0.])

    def set_parameters(self, params):
        self.params = params
        i = 0
        for param in params.leg_params:
            self.legs[i].set_parameters(param)
            i += 1
        self.dkt()

    def set(self, position, rotation, joint_states):
        self.base_mat = my_math.create_trans_mat(position, rotation, 1.0)
        i = 0
        for leg in self.legs:
            leg.set(self.base_mat, joint_states[i * 3:(i + 1) * 3])
            i += 1

    def reset(self, position, rotation, joint_states):
        self.base_mat = my_math.create_trans_mat(position, rotation, 1.0)
        i = 0
        for leg in self.legs:
            leg.reset(self.base_mat, joint_states[i * 3:(i + 1) * 3])
            i += 1

    def step(self, env_state, control, heightmap, dt):
        next_state = copy.deepcopy(env_state)

        # align with pybullet simulation
        #self.base_mat = my_math.create_trans_mat(env_state.position, env_state.rotation, 1.0)
        #for i in range(len(self.legs)):
        #    self.legs[i].position_dkt(self.base_mat, env_state.joint_angles[i * 3:(i + 1) * 3])
        self.calculate_directions(env_state.rotation[2])

        normal_vec = my_math.get_rotation_mat3_z(-next_state.rotation[2]) @ np.array(self.calculate_terrain_normal())
        next_state.rotation[0] = np.arctan2(-normal_vec[1], normal_vec[2])  # roll
        next_state.rotation[1] = np.arctan2(normal_vec[0], normal_vec[2])  # pitch

        # calculate next state
        move_vec = np.array([0., 0.])
        if not self.is_some_leg_stuck(control):
            move_vec = control.move_dir * self.params.body_speed
            next_state.position[0:2] += move_vec * dt
            next_state.rotation += control.rot_dir * self.params.body_turn_speed * dt

        self.adjust_body_height(next_state, control)

        self.base_mat = my_math.create_trans_mat(next_state.position, next_state.rotation, 1.0)

        for i in range(len(self.legs)):
            next_state.joint_angles[i * 3:(i + 1) * 3] = self.legs[i].update(self.base_mat, control.leg_actions[i], heightmap, move_vec, control.move_dir, dt)
            self.legs[i].stuck = self.legs[i].is_stuck(control)

        return next_state, self.calculate_terrain_normal()

    def calculate_directions(self, rot_z):
        self.forward[0] = np.cos(rot_z)
        self.forward[1] = np.sin(rot_z)
        self.left = np.cross(self.forward, np.array([0., 0., 1.]))

    def dkt(self):
        for leg in self.legs:
            leg.dkt(self.base_mat, leg.angles)

    def adjust_body_height(self, state, action):
        mean_height = 0
        for leg in self.legs:
            mean_height += leg.target[2]
        state.position[2] = mean_height / len(self.legs) + action.torso_height

    def calculate_terrain_normal(self):
        median = np.array([0., 0., 0.])
        for leg in self.legs:
            median += leg.target
        median /= len(self.legs)
        normal = np.array([0., 0., 0.])
        for leg1 in self.legs:
            for leg2 in self.legs:
                n = np.cross(median - leg1.target, median - leg2.target)
                if n[2] < 0:
                    normal -= n
                else:
                    normal += n
        normal /= len(self.legs) ** 2
        return my_math.normalize(normal)

    def is_some_leg_stuck(self, action):
        for leg in self.legs:
            if leg.is_stuck(action):
                return True
        return False


class Leg:

    def __init__(self, params):
        self.params = params
        self.angles = np.array([0., 0., 0.])
        self.mount = np.array([0, 0, 0])
        self.mount_mat = None
        self.hip = np.array([0, 0, 0])
        self.hip_mat = None
        self.knee = np.array([0, 0, 0])
        self.knee_mat = None
        self.foot = np.array([0, 0, 0])
        self.foot_mat = None
        self.target = np.array([0, 0, 0])
        self.center = np.array([0, 0, 0])
        self.position = np.array([0, 0, 0])
        self.base_mat = my_math.create_trans_mat(self.params.def_mount, np.array([0, 0, self.params.def_orientation]), 1.0)
        self.swing = -1.0
        self.on_range_edge = False
        self.stuck = False
        self.phase = 0

    def set_parameters(self, params):
        self.params = params
        self.base_mat = my_math.create_trans_mat(self.params.def_mount, np.array([0, 0, self.params.def_orientation]), 1.0)

    def set(self, hexapod_mat, angles):
        self.angles = angles
        self.dkt(hexapod_mat, angles)

    def reset(self, hexapod_mat, angles):
        self.angles = angles
        self.dkt(hexapod_mat, angles)
        self.target = self.foot.copy()
        self.swing = -1.0

    def update(self, hexapod_mat, action, heightmap, body_move_dir, target_move_dir, dt):
        self.swing = action.swing
        self.update_mount_and_center(hexapod_mat)

        #if (target_move_dir[0] == 0.0 and target_move_dir[1] == 0.0) or action.swing > 0:
        self.set_target(target_move_dir, heightmap)

        if action.swing > 0:
            self.move(heightmap, action, body_move_dir, dt)
        else:
            self.phase = 0
            height = heightmap.get_height(self.position)
            if height is not None and height != -np.inf and not np.isnan(height):
                if abs(self.position[2] - height) > self.params.tolerance:
                    self.position[2] = height
        self.clamp_position_to_range(heightmap)
        self.ikt(hexapod_mat)
        self.dkt(hexapod_mat, self.angles)
        return self.angles

    def move(self, heightmap, action, move_dir, dt):
        pos_to_target = self.target - self.position
        pos_to_target_len = np.linalg.norm(pos_to_target)

        if self.phase == 0:
            height = heightmap.get_height(self.position)
            if height is None:
                height = self.position[2] - self.center[2]
            if self.position[2] < height + action.lift_height:
                self.position[2] += self.params.speed * dt
            else:
                self.phase = 1
        elif self.phase == 1:
            pos_to_target_2d = pos_to_target[0:2]
            pos_to_target_2d_len = np.linalg.norm(pos_to_target_2d)
            pos_to_target_2d = pos_to_target_2d / pos_to_target_2d_len
            body_speed = move_dir.dot(pos_to_target_2d)
            self.position[0:2] = my_math.snap(self.position[0:2] + pos_to_target_2d * (self.params.speed + body_speed) * dt,
                                              self.target[0:2],
                                              self.params.speed * dt)

            #self.position[2] = self.target[2] + action.lift_height

            if pos_to_target_2d_len < 0.02:
                self.phase = 2

        elif self.phase == 2:
            self.position = self.position + (pos_to_target / pos_to_target_len) * self.params.speed * dt

    def ikt(self, hexapod_mat):
        base_inv = my_math.inv_trans_mat(hexapod_mat @ self.base_mat)
        base_to_pos_3 = my_math.transform(base_inv, self.position)

        self.angles[0] = np.arctan2(base_to_pos_3[1], base_to_pos_3[0]) + self.params.dh_offsets[0]
        base_to_pos = (my_math.get_rotation_mat3_z(-self.angles[0]) @ base_to_pos_3)[0:3:2]
        hip_to_pos = base_to_pos - np.array([self.params.seg_lens[0], 0.0])
        hip_to_knee = np.array([1., 1.])
        hip_to_foot = hip_to_pos.copy()

        for i in range(20):
            hip_to_foot = hip_to_pos.copy()

            to_knee = my_math.normalize(hip_to_knee - hip_to_foot) * self.params.seg_lens[2]
            hip_to_knee = hip_to_foot + to_knee

            to_hip = my_math.normalize(-hip_to_knee) * self.params.seg_lens[1]
            hip = hip_to_knee + to_hip

            hip_to_knee -= hip
            hip_to_foot -= hip

        if np.cross(np.array([*(hip_to_foot - hip_to_knee), 0.]), np.array([*hip_to_knee, 0.]))[2] < 0:
            rej = my_math.rejection(hip_to_foot, hip_to_knee)
            hip_to_knee = hip_to_knee - 2 * rej

        self.angles[1] = -np.arctan2(hip_to_knee[1], hip_to_knee[0]) + self.params.dh_offsets[1]
        knee_to_foot = hip_to_foot - hip_to_knee
        self.angles[2] = -np.arctan2(knee_to_foot[1], knee_to_foot[0]) - self.angles[1] - self.params.dh_offsets[2]

    def dkt(self, hexapod_mat, angles):
        self.mount_mat = hexapod_mat @ self.base_mat
        self.mount = my_math.get_translation(self.mount_mat)
        self.center = my_math.transform(self.mount_mat, self.params.def_center)
        self.hip_mat = self.mount_mat @ my_math.DH(angles[0] + self.params.dh_offsets[0], 0, self.params.seg_lens[0], -np.pi / 2)
        self.hip = my_math.get_translation(self.hip_mat)
        self.knee_mat = self.hip_mat @ my_math.DH(angles[1] + self.params.dh_offsets[1], 0, self.params.seg_lens[1], 0)
        self.knee = my_math.get_translation(self.knee_mat)
        self.foot_mat = self.knee_mat @ my_math.DH(angles[2] + self.params.dh_offsets[2], 0, self.params.seg_lens[2], 0)
        self.foot = my_math.get_translation(self.foot_mat)
        if self.phase == 0:
            self.position = self.foot.copy()

    def position_dkt(self, hexapod_mat, angles):
        mount_mat = hexapod_mat @ self.base_mat
        hip_mat = mount_mat @ my_math.DH(angles[0] + self.params.dh_offsets[0], 0, self.params.seg_lens[0], -np.pi / 2)
        knee_mat = hip_mat @ my_math.DH(angles[1] + self.params.dh_offsets[1], 0, self.params.seg_lens[1], 0)
        pos_mat = knee_mat @ my_math.DH(angles[2] + self.params.dh_offsets[2], 0, self.params.seg_lens[2], 0)
        self.position = my_math.get_translation(pos_mat)

    def set_target(self, body_move_dir, heightmap):
        if body_move_dir[0] != 0 or body_move_dir[1] != 0:
            body_move_dir = my_math.normalize(body_move_dir)

        self.target[0:2] = self.center[0:2] + body_move_dir * self.params.range
        height = heightmap.get_height(self.target)
        if height is not None and not np.isnan(height) and height != -np.inf:
            self.target[2] = height
        else:
            self.target[2] = self.center[2]

    def update_mount_and_center(self, hexapod_mat):
        self.mount_mat = hexapod_mat @ self.base_mat
        self.mount = my_math.get_translation(self.mount_mat)
        self.center = my_math.transform(self.mount_mat, self.params.def_center)

    def clamp_position_to_range(self, heightmap):
        self.on_range_edge = False

        terrain_height = heightmap.get_height(self.position)
        if terrain_height is not None:
            if self.position[2] < terrain_height:
                self.position[2] = terrain_height

        center_to_pos = self.position[0:2] - self.center[0:2]
        len_2d = np.linalg.norm(center_to_pos)
        if len_2d > self.params.range:
            self.position[0:2] = self.center[0:2] + center_to_pos / len_2d * self.params.range
            self.on_range_edge = True

        hip_to_pos = self.position - self.hip
        len_3d = np.linalg.norm(hip_to_pos)
        if len_3d > self.params.seg_lens[1] + self.params.seg_lens[2]:
            self.position = self.hip + hip_to_pos / len_3d * (self.params.seg_lens[1] + self.params.seg_lens[2])
            self.on_range_edge = True

        if len_3d < abs(self.params.seg_lens[1] - self.params.seg_lens[2]):
            self.position = self.hip + hip_to_pos / len_3d * abs(self.params.seg_lens[1] - self.params.seg_lens[2])
            self.on_range_edge = True

    def is_stuck(self, action):
        # if self.swing > 0.0:
        #     return False
        center_to_pos_2d = (self.position - self.center)[0:2]
        center_to_pos_2d_len = np.linalg.norm(center_to_pos_2d)
        if center_to_pos_2d_len > self.params.range * 0.9:
            if action.move_dir[0] != 0 or action.move_dir[1] != 0:
                if np.dot(action.move_dir, center_to_pos_2d) < 0:
                    return True
            if action.rot_dir[2] != 0:
                normal = np.cross(self.center - self.mount, np.array([0., 0., 1.]))
                if np.dot(center_to_pos_2d, normal[0:2]) * np.sign(action.rot_dir[2]) > 0:
                    return True
        return False

    def on_target(self):
        return np.linalg.norm(self.target - self.position) <= self.params.tolerance

    def update_parameters(self):
        self.base_mat = my_math.create_trans_mat(self.params.def_mount, np.array([0, 0, self.params.def_orientation]), 1.0)
