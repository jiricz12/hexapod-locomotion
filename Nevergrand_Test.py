import numpy as np
import nevergrad as ng

# Let us define a function.
n = 2
vec = np.random.rand(n)
for op in sorted(ng.optimizers.registry.keys()):
    print(op)

def myfunction(arg1, ar):
    return np.linalg.norm(ar - vec)

# argument transformation
# Optimization of mixed (continuous and discrete) hyperparameters.
arg1 = ar = ng.p.Array(init=[0 for i in range(n)]).set_mutation(sigma=0.01).set_bounds(-1.0, 1.0)
# We apply a softmax for converting real numbers to discrete values.
arg2 = ng.p.Choice(["a", "c", "e"])  # 2nd arg. = positional discrete argument
value = ng.p.Scalar(init=1.0).set_mutation(sigma=2)  # the 4th arg. is a keyword argument with Gaussian prior
ar = ng.p.Array(init=[0 for i in range(n)]).set_mutation(sigma=0.01).set_bounds(-1.0, 1.0)

# create the parametrization
# the 3rd arg. is a positional arg. which will be kept constant to "blublu"
instru = ng.p.Instrumentation(arg1, ar)

print(instru.dimension)  # 5 dimensional space
print(vec)
budget = 120  # How many episode we will do before concluding.
for name in ["CMA"]:  #["RandomSearch", "ScrHammersleySearch", "TwoPointsDE", "PortfolioDiscreteOnePlusOne", "CMA", "PSO"]:
    optim = ng.optimizers.registry[name](parametrization=instru, budget=budget)
    for u in range(budget // 3):
        x1 = optim.ask()
        # Ask and tell can be asynchronous.
        # Just be careful that you "tell" something that was asked.
        # Here we ask 3 times and tell 3 times in order to fake asynchronicity
        x2 = optim.ask()
        x3 = optim.ask()
        # The three folowing lines could be parallelized.
        # We could also do things asynchronously, i.e. do one more ask
        # as soon as a training is over.
        print(x1.args)
        y1 = myfunction(*x1.args, **x1.kwargs)  # here we only defined an arg, so we could omit kwargs
        y2 = myfunction(*x2.args, **x2.kwargs)  # (keeping it here for the sake of consistency)
        y3 = myfunction(*x3.args, **x3.kwargs)
        optim.tell(x1, y1)
        optim.tell(x2, y2)
        optim.tell(x3, y3)
    recommendation = optim.recommend()
    print("* ", name, " provides a vector of parameters", recommendation.args,"with test error ",
          myfunction(*recommendation.args, **recommendation.kwargs))