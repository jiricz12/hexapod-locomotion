import numpy as np
from OpenGL.GL import *
import OpenGL.GL.shaders
import imgui
from robot import *
import visualization
import policy
import src.mesh_creator
import os
import optimizer
from sklearn.decomposition import PCA

from mesh_creator import StepTerrain

from src import robot
from simulations import StructuredTrainer
from simulations import UnstructuredTrainer
from simulations import StructuredTester
from simulations import UnstructuredTester
from simulations import StructuredEvaluator
from simulations import UnstructuredEvaluator
from simulations import Handler

WORLD_UP = np.array([0., 0., 1.])


class Shader:
    def __init__(self, vertex_path, fragment_path):
        def load_shader_data(path):
            data = None
            try:
                f = open(path, "r")
                data = f.read()
                f.close()
            except FileNotFoundError:
                print(f"Shader file with path: {vertex_path} does not exist!")
                exit(-1)
            return data

        vs = OpenGL.GL.shaders.compileShader(load_shader_data(vertex_path), GL_VERTEX_SHADER)
        fs = OpenGL.GL.shaders.compileShader(load_shader_data(fragment_path), GL_FRAGMENT_SHADER)
        self.id = OpenGL.GL.shaders.compileProgram(vs, fs)
        self.uniform_locations = {}

    def get_uniform_location(self, uniform_name):
        if uniform_name not in self.uniform_locations:
            uniform_location = glGetUniformLocation(self.id, uniform_name)
            if uniform_location == -1:
                print(f"Uniform name \"{uniform_name}\" not found!")
                exit(-1)
            self.uniform_locations[uniform_name] = uniform_location
        return self.uniform_locations[uniform_name]

    def use(self):
        glUseProgram(self.id)

    def load_float(self, uniform_name, val):
        glUniform1f(self.get_uniform_location(uniform_name), float(val))

    def load_vec3(self, uniform_name, val):
        glUniform3fv(self.get_uniform_location(uniform_name), 1, np.array(val, dtype=np.float32))

    def load_vec4(self, uniform_name, val):
        glUniform4fv(self.get_uniform_location(uniform_name), 1, np.array(val, dtype=np.float32))

    def load_mat4(self, uniform_name, val):
        glUniformMatrix4fv(self.get_uniform_location(uniform_name), 1, GL_TRUE, val)


class Entity:
    def __init__(self, model, color, position, rotation, scale, additional_transform=None, render_mode=GL_TRIANGLES, alpha=1.0):
        self.model = model
        self.color = color
        self.alpha = alpha
        self.position = position
        self.rotation = rotation
        self.scale = scale
        self.render_mode = render_mode
        self.additional_transform = additional_transform
        self.transformation_matrix = my_math.create_trans_mat(position, rotation, scale)
        self.updated = True

    def set_position(self, position):
        assert (len(position) == len(self.position))
        self.position[:] = position[:]
        self.updated = True

    def set_rotation(self, rotation):
        assert (len(rotation) == len(self.rotation))
        self.rotation[:] = rotation[:]
        self.updated = True

    def increment_rotation(self, rotation):
        self.rotation += rotation
        self.updated = True

    def set_scale(self, scale):
        if isinstance(scale, list):
            assert (isinstance(self.scale, list))
            assert (len(scale) == len(self.scale))
            self.scale[:] = scale[:]
        else:
            assert (not isinstance(self.scale, list))
            self.scale = scale
        self.updated = True

    def get_transformation(self):
        if self.updated:
            if self.additional_transform is not None:
                self.transformation_matrix = self.additional_transform @ my_math.create_trans_mat(self.position, self.rotation, self.scale)
            else:
                self.transformation_matrix = my_math.create_trans_mat(self.position, self.rotation, self.scale)
            self.updated = False
        return self.transformation_matrix


class Camera:

    def __init__(self, fov):
        self.position = np.array([0., 0., 0.])
        self.focus_point = np.array([0., 0., 0.])
        self.front = np.array([0., 0., 0.])
        self.right = np.array([0., 0., 0.])
        self.up = np.array([0., 0., 0.])
        self.fov = fov
        self.yaw = -0.68
        self.pitch = -0.5
        self.distance_from_focus_point = 3.0
        self.lock_to_focus_point = True
        self.near_plane = 0.5
        self.far_plane = 100
        self.calculate_position()

    def get_view_matrix(self):
        self.calculate_position()
        return my_math.get_view_matrix(self.position, self.position + self.front, self.up)

    def get_position(self):
        return self.position

    def get_fov(self):
        return self.fov

    def mouse_position_callback(self, window, x_coord, y_coord, vel_x, vel_y):
        if x_coord > visualization.MENU_WIDTH:
            if glfw.get_mouse_button(window, glfw.MOUSE_BUTTON_3):
                if not self.lock_to_focus_point and glfw.get_key(window, glfw.KEY_LEFT_SHIFT):
                    self.focus_point += vel_y / 300 * self.up
                    self.focus_point -= vel_x / 300 * self.right
                else:
                    self.yaw -= vel_x / 100
                    self.pitch = my_math.clamp(self.pitch - vel_y / 100.0, -np.pi / 2.0, np.pi / 2.0)

    def set_focus_point(self, new_point):
        self.focus_point = new_point.copy()

    def mouse_scroll_callback(self, window, x_offset, y_offset):
        if glfw.get_cursor_pos(window)[0] > visualization.MENU_WIDTH:
            self.distance_from_focus_point -= y_offset / 10
            self.yaw -= x_offset / 50

    def calculate_position(self):
        temp_pos = np.array([self.distance_from_focus_point, 0, 0, 0])
        rot_yaw = my_math.get_rotation_mat4_z(self.yaw)
        rot_pitch = my_math.get_rotation_mat4_y(self.pitch)
        self.position = rot_yaw @ rot_pitch @ temp_pos
        self.position = self.focus_point + self.position[0:3]

        to_focus_point = self.focus_point - self.position
        self.front = my_math.normalize(to_focus_point)
        self.right = np.cross(self.front, WORLD_UP)
        self.up = np.cross(self.right, self.front)


class Renderer:

    def __init__(self):

        self.camera = Camera(np.pi / 9.0)

        self.basic_shader = Shader("shaders_src/basic_lighting_vs.glsl",
                                   "shaders_src/basic_lighting_fs.glsl")

        self.simple_shader = Shader("shaders_src/lightCube_vs.glsl",
                                    "shaders_src/lightCube_fs.glsl")

        glEnable(GL_DEPTH_TEST)
        glEnable(GL_CULL_FACE)
        glClearColor(0.2, 0.2, 0.2, 1)

        self.graph_value_offset = 0
        self.graph_values_count = -1
        self.training_export_name = "Name"
        self.raw_cube = src.mesh_creator.create_cube(1.0, 1.0, 1.0)
        self.raw_offset_cube_x = src.mesh_creator.create_offsetted_cube(0.0, 1.0, -0.5, 0.5, -0.5, 0.5)
        self.raw_offset_cube_y = src.mesh_creator.create_offsetted_cube(-0.5, 0.5, 0.0, 1.0, -0.5, 0.5)
        self.raw_offset_cube_z = src.mesh_creator.create_offsetted_cube(-0.5, 0.5, -0.5, 0.5, 0.0, 1.0)

        self.trajectories = []

        self.graph_height = 80

        self.train_active = False
        self.use_gait_policy = True
        self.train_gait_policy = True
        self.use_torso_height_policy = False
        self.train_body_height_policy = True
        self.use_leg_height_policy = False
        self.train_leg_height_policy = True
        self.mode_idx = 0
        self.optimizers = ["CMA", "RandomSearch", "ScrHammersleySearch", "TwoPointsDE", "PortfolioDiscreteOnePlusOne", "PSO", "ConservativeSearch"]

        self.body_height_variation = 0

        self.evaluate_all = False

        self.traj_files = []
        self.traj_idx = 0

        self.gait_po_idx = 0
        self.gait_po_files = []
        self.gait_po_sel = None
        self.gait_sigma = 0.001
        self.solutions_n = 10
        self.budget = 2000
        self.optimizer_idx = 0
        self.global_solutions_n = 20
        self.local_solutions_n = 5
        self.global_steps = 3

        self.body_height_po_idx = 0
        self.body_height_po_files = []
        self.body_height_po_sel = None
        self.body_height_sigma = 0.005
        self.body_optimizer_idx = 0

        self.leg_height_po_idx = 0
        self.leg_height_po_files = []
        self.leg_height_po_sel = None
        self.leg_height_sigma = 0.005
        self.leg_optimizer_idx = 0

        self.t_idx = 0
        self.t_files = []
        self.t_sel = None
        self.animate = False
        self.start_pause = False
        self.save_period = 1000
        self.max_iters = 100
        self.eval_iters = 3
        self.layers = "17 1"
        self.mimic = False
        self.noise_level = 0.001
        self.start_pos = [0., 0., 0.25]
        self.lod = 1.0

        self.test_active = False

        self.eval_active = False
        self.export_trajectories = False

        self.crt_po_active = False
        self.crt_po_name = "default"
        self.crt_po_w_type = 0
        self.crt_po_type = 0
        self.crt_po_input_weights = ""
        self.crt_layers = [17, 6]
        self.crt_layers_txt = "17 6"
        self.crt_po_sel = None
        self.crt_po_idx = 0

        self.crt_t_active = False
        self.crt_t_show_changes = False
        self.crt_t_slope_x = 0
        self.crt_t_slope_y = 0
        self.crt_t_name = "default"
        self.crt_t_nx = 50
        self.crt_t_ny = 50
        self.crt_t_cube_height = 0.05
        self.crt_t_scale = [1., 1., 0.2]
        self.crt_t_step = 0.05
        self.crt_t_position = np.array([0., 0., 0.])
        self.crt_t_step_dist = [34, 25]
        self.crt_t_step_len = [7, 7]
        self.crt_t_step_arc = [0.0, 0.0]
        self.crt_t_step_height = 0.08
        self.crt_t_type_idx = 0
        self.crt_t_seed = 0

        self.crt_h_active = False
        self.crt_h_name = "default"
        self.h_files = []
        self.h_idx = 0
        self.h_sel = None

        self.tr_log_active = False
        self.tr_log_files = []
        self.tr_log_sel = None
        self.tr_log_idx = 0
        self.tr_log_disp_param_vec = False
        self.tr_log_max_rew = 0
        self.tr_log_min_rew = 0
        self.tr_log_min_C_value_X = 0
        self.tr_log_max_C_value_X = 0
        self.tr_log_min_C_value_Y = 0
        self.tr_log_max_C_value_Y = 0
        self.tr_log_C = None
        self.tr_log_image_w = 32
        self.tr_log_image_h = 32
        self.tr_log_image_view_w = 640
        self.tr_log_image_view_h = 640
        self.tr_log_image_name = "Default"
        self.tr_log_image_id = -1


    def prepare_to_render(self, shader, visual_params, frame_width, frame_height):
        shader.use()

        if shader == self.basic_shader:
            shader.load_vec3("lightPos", my_math.spherical_to_cartesian(visual_params.light_position))
            shader.load_vec3("lightColor", visual_params.light_color)
            shader.load_vec3("viewPos", self.camera.get_position())

        if frame_height > 0 and frame_width > 0:
            shader.load_mat4("projection", my_math.get_perspektive_matrix(self.camera.get_fov(),
                                                                          float(frame_width) / float(frame_height),
                                                                          self.camera.near_plane, self.camera.far_plane))
        shader.load_mat4("view", self.camera.get_view_matrix())

    def render_simple_entities(self, entities, render_mode=GL_TRIANGLES):

        for entity in entities:
            self.simple_shader.load_vec3("objectColor", entity.color)
            self.simple_shader.load_mat4("model", entity.get_transformation())

            entity.model.bind()
            glEnableVertexAttribArray(0)
            glDrawArrays(render_mode, 0, entity.model.vertex_count)
            glDisableVertexAttribArray(0)
            entity.model.unbind()

    def render_entity(self, model, color, position, rotation, scale, additional_transform=None, alpha=1.0, render_mode=GL_TRIANGLES):

        transformation_matrix = my_math.create_trans_mat(position, rotation, scale)
        if additional_transform is not None:
            transformation_matrix = additional_transform @ transformation_matrix

        if alpha != 1.0:
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        self.basic_shader.load_vec4("objectColor", [*color, alpha])
        self.basic_shader.load_mat4("model", transformation_matrix)

        model.bind()
        glEnableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        glDrawArrays(render_mode, 0, model.vertex_count)
        if alpha != 1.0:
            glDisable(GL_BLEND)
        glDisableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        model.unbind()


    def render_heightmap(self, hm, color, alpha):

        if alpha != 1.0:
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        self.basic_shader.load_vec4("objectColor", [*color, alpha])
        self.raw_offset_cube_z.bind()
        glEnableVertexAttribArray(0)
        glEnableVertexAttribArray(1)

        if hm.lowest_value != np.inf:
            for row in range(hm.size_n):
                for col in range(hm.size_n):
                    if hm.grid[row][col] != -np.inf:
                        pos = np.array([*((hm.corner + np.array([row, col]) + 0.5) * hm.step), hm.lowest_value - 0.1])
                        transformation_matrix = np.array([[hm.step, 0, 0, pos[0]],
                                                          [0, hm.step, 0, pos[1]],
                                                          [0, 0, hm.grid[row, col] - (hm.lowest_value - 0.1), pos[2]],
                                                          [0, 0, 0, 1]])
                        self.basic_shader.load_mat4("model", transformation_matrix)
                        glDrawArrays(GL_TRIANGLES, 0, self.raw_offset_cube_z.vertex_count)
        if alpha != 1.0:
            glDisable(GL_BLEND)
        glDisableVertexAttribArray(0)
        glEnableVertexAttribArray(1)
        self.raw_offset_cube_z.unbind()

    def render_simple_entity(self, model, color, position, rotation, scale, additional_transform=None, render_mode=GL_TRIANGLES):

        transformation_matrix = my_math.create_trans_mat(position, rotation, scale)
        if additional_transform is not None:
            transformation_matrix = additional_transform @ transformation_matrix

        self.simple_shader.load_mat4("model", transformation_matrix)
        self.simple_shader.load_vec3("objectColor", color)

        model.bind()
        glEnableVertexAttribArray(0)
        glDrawArrays(render_mode, 0, model.vertex_count)
        glDisableVertexAttribArray(0)
        model.unbind()

    def render_coordinate_system(self, t_matrix, thickness, length, scale=1.0):
        thickness *= scale
        length *= scale
        self.render_simple_entity(self.raw_cube, [1.0, 1.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [thickness * 1.3, thickness * 1.3, thickness * 1.3],
                                  t_matrix)
        self.render_simple_entity(self.raw_offset_cube_x, [1.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [length, thickness, thickness], t_matrix)
        self.render_simple_entity(self.raw_offset_cube_y, [0.0, 1.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [thickness, length, thickness], t_matrix)
        self.render_simple_entity(self.raw_offset_cube_z, [0.0, 0.0, 1.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [thickness, thickness, length], t_matrix)

    def render_gui(self, main, visual_params):

        if len(main.structured_trainer_sim) > 0:
            imgui.separator()
            for sim in main.structured_trainer_sim:
                imgui.push_id(str(sim))
                if sim.pause:
                    imgui.push_style_color(imgui.COLOR_HEADER, 1, 1, 0, 1)
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 0, 0, 1)
                if imgui.tree_node("Structured Trainer", imgui.TREE_NODE_FRAMED):
                    imgui.same_line()
                    imgui.text(f"Episode: {sim.handler.episode}")
                    if sim.pause:
                        imgui.pop_style_color(2)
                    changed, new_value = imgui.checkbox("Render simulation", sim.render)
                    if changed:
                        sim.render = new_value
                    imgui.text(f"ON time: {get_time_as_str(sim.time_passed)}")
                    imgui.text(f"Supervised learning: {sim.supervised}")

                    imgui.text("Optimizer: " + sim.handler.es.name)
                    if sim.gait_policy is not None:
                        if imgui.tree_node(f"Gait policy: {sim.gait_policy.name}"):
                            imgui.text(f"Gait phases:")
                            for ph in sim.sw:
                                imgui.text(str(ph))
                                imgui.same_line()
                                if ph > 0.0:
                                    imgui.push_style_color(imgui.COLOR_TEXT, 0.0, 1.0, 0.0)
                                    imgui.text("Swing")
                                else:
                                    imgui.push_style_color(imgui.COLOR_TEXT, 1.0, 0.0, 0.0)
                                    imgui.text("Stance")
                                imgui.pop_style_color(1)
                            imgui.tree_pop()
                    else:
                        imgui.text(f"Gait policy: Raw")

                    if sim.torso_policy is not None:
                        if imgui.tree_node(f"Torso height policy: {sim.torso_policy.name}"):
                            imgui.text(f"Predicted height: {sim.torso_height}")
                            imgui.tree_pop()
                    else:
                        imgui.text(f"Torso height policy: Raw")

                    if sim.leg_policy is not None:
                        if imgui.tree_node(f"Leg height policy: {sim.leg_policy.name}"):
                            for height in sim.leg_height:
                                imgui.text(str(height))
                            imgui.tree_pop()
                    else:
                        imgui.text(f"Leg height policy: Raw")

                    imgui.text(f"Episode: {sim.handler.episode}")
                    imgui.text(f"Solution: {sim.handler.solution_idx + 1}/{sim.handler.solutions_n}")
                    imgui.text(f"Evaluation iteration {sim.eval_iter + 1}/{sim.eval_iters}")
                    imgui.text(f"Iterations {sim.env.step_ctr}/{sim.env.max_steps}")
                    imgui.text(f"Best score: {-sim.handler.best_loss}")

                    #  TERRAIN INFO
                    terrain_info_gui(sim)

                    #  HEXAPOD INFO
                    hexapod_info_gui(sim)

                    #  HEIGHTMAP INFO
                    heightmap_info_gui(sim)

                    if imgui.tree_node("Environment"):
                        imgui.text("Observation:")
                        imgui.text(str(sim.obs))
                        if sim.env.noise_level > 0.0:
                            imgui.text("Noise:")
                            imgui.text(str(sim.env.noise))
                        imgui.tree_pop()

                    reward_info(sim)

                    if imgui.tree_node("Scores"):
                        if imgui.tree_node("Current"):
                            for val in sim.handler.losses:
                                imgui.text("{:.2f}".format(-val))
                            imgui.tree_pop()
                        if not sim.supervised:
                            if imgui.tree_node("Average"):
                                imgui.separator()
                                imgui.text("Transition: {:.3f}".format(sim.avr_transition_rew))
                                changed, new_value = imgui.drag_float("Transition reward", sim.transition_rew, 0.1)
                                if changed: sim.transition_rew = new_value
                                imgui.separator()
                                imgui.text("Good stance: {:.3f}".format(sim.avr_stance_in_rew))
                                changed, new_value = imgui.drag_float("Good stance reward", sim.stance_in_rew, 0.1)
                                if changed: sim.stance_in_rew = new_value
                                imgui.separator()
                                imgui.text("Bad stance distance: {:.3f}".format(sim.avr_stance_out_dist_rew))
                                changed, new_value = imgui.drag_float("Bad stance distance", sim.stance_out_dist_rew, 0.1)
                                if changed: sim.stance_out_dist_rew = new_value
                                imgui.separator()
                                imgui.text("Good swing: {:.3f}".format(sim.avr_swing_in_rew))
                                changed, new_value = imgui.drag_float("Good swing", sim.swing_in_rew, 0.1)
                                if changed: sim.swing_in_rew = new_value
                                imgui.separator()
                                imgui.text("Tripod gait: {:.3f}".format(sim.avr_tripod_rew))
                                changed, new_value = imgui.drag_float("Tripod gait", sim.tripod_rew, 0.1)
                                if changed: sim.tripod_rew = new_value

                                imgui.separator()
                                imgui.tree_pop()
                        if imgui.tree_node("Graphs"):
                            imgui.plot_lines("Average score", np.array(sim.handler.average_scores, dtype=np.float32),
                                             graph_size=(imgui.get_content_region_available_width(), self.graph_height))
                            imgui.tree_pop()
                        imgui.tree_pop()

                    if imgui.tree_node("Weights"):
                        weights = sim.handler.best_w
                        imgui.text(str(weights))
                        imgui.text(f"Median: {sum(weights) / len(weights)}")
                        imgui.text(f"Min: {min(weights)}")
                        imgui.text(f"Max: {max(weights)}")
                        imgui.tree_pop()

                    if imgui.button("Save best"):
                        idx = 0
                        if sim.gait_policy is not None and sim.use_gait_pol:
                            next_idx = idx+sim.gait_policy.get_num_params()
                            temp = policy.Policy(sim.gait_policy.name, sim.gait_policy.layers, sim.handler.best_w[idx:next_idx])
                            policy.save_policy(main.gait_policy_path, temp)
                            idx = next_idx
                        if sim.torso_policy is not None and sim.use_torso_pol:
                            next_idx = idx+sim.torso_policy.get_num_params()
                            temp = policy.Policy(sim.torso_policy.name, sim.torso_policy.layers, sim.handler.best_w[idx:next_idx])
                            policy.save_policy(main.body_height_policy_path, temp)
                            idx = next_idx
                        if sim.leg_policy is not None and sim.use_leg_pol:
                            next_idx = idx+sim.leg_policy.get_num_params()
                            temp = policy.Policy(sim.leg_policy.name, sim.leg_policy.layers, sim.handler.best_w[idx:next_idx])
                            policy.save_policy(main.leg_height_policy_path, temp)

                    changed, new_value = imgui.drag_int("N iterations per solution", sim.env.max_steps, 1, 0, 1000000)
                    if changed:
                        sim.env.max_steps = new_value
                    changed, new_value = imgui.drag_int("Evaluation iterations", sim.eval_iters, 1, 0, 1000000)
                    if changed:
                        sim.eval_iters = new_value

                    if sim.env.animate:
                        changed, new_value = imgui.drag_float("Animation ms per iter", sim.env.animation_speed, 0.001, 0.0, 10, "%.3f", 1.0)
                        if changed:
                            sim.env.animation_speed = new_value

                    if imgui.tree_node("Export training data"):

                        changed, new_value = imgui.input_text("##Name", sim.export_name, 256)
                        if changed:
                            sim.export_name = new_value

                        imgui.same_line()
                        if imgui.button("Export"):
                            self.export_training_data(sim)
                            export_plot(sim.export_name + "_plot", sim.handler.average_scores)
                        imgui.new_line()
                        if imgui.button("Export whole training"):
                            export_whole_training(sim.export_name, sim.handler.training_log)
                        imgui.tree_pop()
                    imgui.new_line()

                    control_buttons(sim, main.structured_trainer_sim, "training")

                    imgui.tree_pop()
                else:
                    imgui.same_line()
                    imgui.text(f"Episode: {sim.handler.episode}")
                    if sim.pause:
                        imgui.pop_style_color(2)

                imgui.pop_id()
            imgui.separator()

        if len(main.structured_tester_sim) > 0:
            imgui.separator()
            for sim in main.structured_tester_sim:
                imgui.push_id(str(sim))
                if sim.pause:
                    imgui.push_style_color(imgui.COLOR_HEADER, 1, 1, 0, 1)
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 0, 0, 1)
                if imgui.tree_node("Structured Tester", imgui.TREE_NODE_FRAMED):
                    if sim.pause:
                        imgui.pop_style_color(2)
                    changed, new_value = imgui.checkbox("Render tester", sim.render)
                    if changed:
                        sim.render = new_value

                    #  TERRAIN INFO
                    terrain_info_gui(sim)

                    #  HEXAPOD INFO
                    hexapod_info_gui(sim)

                    #  HEIGHTMAP
                    heightmap_info_gui(sim)

                    imgui.text(f"ON time: {get_time_as_str(sim.time_passed)}")
                    imgui.text(str(sim.reward))
                    imgui.text(f"Iterations {sim.env.step_ctr}/{sim.env.max_steps}")

                    if imgui.tree_node("Gait transition"):
                        imgui.text(f"Total average score: {sim.get_gait_score()}")
                        imgui.separator()
                        imgui.text("Transition: {:.3f}".format(sim.avr_transition_rew))
                        changed, new_value = imgui.drag_float("Transition reward", sim.transition_rew, 0.1)
                        if changed: sim.transition_rew = new_value
                        imgui.separator()
                        imgui.text("Good stance: {:.3f}".format(sim.avr_stance_in_rew))
                        changed, new_value = imgui.drag_float("Good stance reward", sim.stance_in_rew, 0.1)
                        if changed: sim.stance_in_rew = new_value
                        imgui.separator()
                        imgui.text("Bad stance distance: {:.3f}".format(sim.avr_stance_out_dist_rew))
                        changed, new_value = imgui.drag_float("Bad stance distance", sim.stance_out_dist_rew, 0.1)
                        if changed: sim.stance_out_dist_rew = new_value
                        imgui.separator()
                        imgui.text("Good swing: {:.3f}".format(sim.avr_swing_in_rew))
                        changed, new_value = imgui.drag_float("Good swing", sim.swing_in_rew, 0.1)
                        if changed: sim.swing_in_rew = new_value
                        imgui.separator()
                        imgui.text("Tripod gait: {:.3f}".format(sim.avr_tripod_rew))
                        changed, new_value = imgui.drag_float("Tripod gait", sim.tripod_rew, 0.1)
                        if changed: sim.tripod_rew = new_value
                        imgui.separator()
                        imgui.tree_pop()

                    changed, new_value = imgui.drag_int("Iterations per solution", sim.env.max_steps, 1, 0, 1000000)
                    if changed:
                        sim.env.max_steps = new_value

                    changed, new_value = imgui.drag_float("LOD", sim.lod, 0.001, 0.0, 10, "%.3f", 1.0)
                    if changed:
                        sim.lod = new_value

                    changed, new_value = imgui.drag_float("Noise level", sim.env.noise_level, 0.0001, 0.0, 1.0, "%.3f", 1.0)
                    if changed:
                        sim.env.noise_level = new_value

                    changed, new_value = imgui.drag_int("Simulation steps per iter", sim.env.sim_steps_per_iter, 1, 0, 1000)
                    if changed:
                        sim.env.sim_steps_per_iter = new_value

                    changed, new_value = imgui.drag_float("Animation ms per iter", sim.env.animation_speed, 0.001, 0.0, 1.0, "%.3f", 1.0)
                    if changed:
                        sim.env.animation_speed = new_value

                    changed, value = imgui.checkbox("Use gait policy", sim.use_gait_pol)
                    if changed:
                        sim.use_gait_pol = value

                    changed, value = imgui.checkbox("Use body height policy", sim.use_torso_pol)
                    if changed:
                        sim.use_torso_pol = value

                    changed, value = imgui.checkbox("Use leg height policy", sim.use_leg_pol)
                    if changed:
                        sim.use_leg_pol = value

                    changed, value = imgui.checkbox("Auto reset", sim.auto_reset)
                    if changed:
                        sim.auto_reset = value

                    if imgui.tree_node("Details"):
                        if imgui.tree_node(f"Policy: {sim.gait_policy.name}"):
                            imgui.text(f"{sim.gait_policy.get_params()}")
                            imgui.tree_pop()

                        imgui.tree_pop()
                    imgui.new_line()
                    if imgui.button("Reset"):
                        sim.reset()

                    control_buttons(sim, main.structured_tester_sim, "testing")

                    imgui.tree_pop()
                elif sim.pause:
                    imgui.pop_style_color(2)
                imgui.pop_id()
            imgui.separator()

        if len(main.structured_evaluator_sim) > 0:
            imgui.separator()
            for sim in main.structured_evaluator_sim:
                imgui.push_id(str(sim))
                if sim.pause:
                    imgui.push_style_color(imgui.COLOR_HEADER, 0, 1, 0, 1)
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 0, 0, 1)
                if imgui.tree_node("Structured Evaluator", imgui.TREE_NODE_FRAMED):
                    if sim.pause:
                        imgui.pop_style_color(2)
                    imgui.same_line()
                    imgui.text(f"{sim.eval_iter + 1}/{sim.eval_iters}")
                    changed, new_value = imgui.checkbox("Render simulation", sim.render)
                    if changed:
                        sim.render = new_value
                    imgui.text(f"ON time: {get_time_as_str(sim.time_passed)}")

                    pol_name = sim.gait_policy.name if sim.gait_policy is not None else "Raw"
                    imgui.text(f"Gait policy: {pol_name}")
                    pol_name = sim.torso_policy.name if sim.torso_policy is not None else "Raw"
                    imgui.text(f"Torso height policy: {pol_name}")
                    pol_name = sim.leg_policy.name if sim.leg_policy is not None else "Raw"
                    imgui.text(f"Leg height policy: {pol_name}")
                    imgui.text(f"Evaluation iteration {sim.eval_iter + 1}/{sim.eval_iters}")
                    imgui.text(f"Iterations {sim.env.step_ctr}/{sim.env.max_steps}")
                    if imgui.tree_node("Score"):
                        imgui.text(f"Distance: {sim.get_distance()}")
                        imgui.text(f"Smoothness: {sim.get_smoothness()}")
                        imgui.text(f"Work: {sim.get_work()}")
                        imgui.tree_pop()

                    reward_info(sim)

                    #  TERRAIN INFO
                    terrain_info_gui(sim)

                    #  HEXAPOD INFO
                    hexapod_info_gui(sim)

                    #  HEIGHTMAP INFO
                    heightmap_info_gui(sim)

                    if imgui.tree_node("Environment"):
                        imgui.text("Observation:")
                        imgui.text(str(sim.obs))
                        if sim.env.noise_level > 0.0:
                            imgui.text("Noise:")
                            imgui.text(str(sim.env.noise))
                        imgui.tree_pop()

                    changed, new_value = imgui.drag_float("LOD", sim.lod, 0.001, 0.0, 10, "%.3f", 1.0)
                    if changed:
                        sim.lod = new_value

                    if sim.env.animate:
                        changed, new_value = imgui.drag_float("Animation ms per iter", sim.env.animation_speed, 0.001, 0.0, 10, "%.3f", 1.0)
                        if changed:
                            sim.env.animation_speed = new_value

                    changed, new_value = imgui.input_text("##Name", sim.export_name, 256)
                    if changed:
                        sim.export_name = new_value

                    imgui.same_line()
                    if imgui.button("Export"):
                        export_trajectory_plot(sim)
                        f = open("logs/evaluation_log/" + sim.export_name + ".txt", "w")
                        f.write(f"Evaluation time [s]: {get_time_as_str(sim.time_passed)}\n")
                        f.write("Terrain: " + (sim.terrain.name if sim.terrain is not None else "Flat")+"\n")
                        f.write(f"Starting position: {sim.env.start_position}\n")
                        f.write(f"Number of iterations: {sim.env.max_steps}\n")
                        f.write(f"Evaluation iterations: {sim.eval_iters}\n")
                        f.write(f"Delta time: {sim.lod}\n")
                        f.write(f"Noise level: {sim.env.noise_level}\n\n")
                        f.write("Gait policy: " + str(sim.gait_policy.name if sim.gait_policy is not None else "Raw algorithm") + "\n")
                        f.write("Body height policy: " + str(sim.torso_policy.name if sim.torso_policy is not None else "Raw algorithm") + "\n")
                        f.write("Leg height policy: " + str(sim.leg_policy.name if sim.leg_policy is not None else "Raw algorithm") + "\n")
                        export_evaluation_score(f, sim)
                        f.close()
                    if imgui.button("Export trajectory"):
                        with open("logs/evaluation_log/trajectories/" + sim.export_name + ".trj", "wb") as f:
                            pickle.dump(sim.trajectories, f)

                    imgui.new_line()

                    control_buttons(sim, main.structured_evaluator_sim, "evaluation")

                    imgui.tree_pop()
                else:
                    imgui.same_line()
                    imgui.text(f"{sim.eval_iter + 1}/{sim.eval_iters}")
                    if sim.pause:
                        imgui.pop_style_color(2)

                imgui.pop_id()
            imgui.separator()

        if len(main.unstructured_trainer_sim) > 0:
            imgui.separator()
            for sim in main.unstructured_trainer_sim:
                imgui.push_id(str(sim))
                if sim.pause:
                    imgui.push_style_color(imgui.COLOR_HEADER, 1, 1, 0, 1)
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 0, 0, 1)
                if imgui.tree_node("Unstructured Trainer", imgui.TREE_NODE_FRAMED):
                    if sim.pause:
                        imgui.pop_style_color(2)
                    changed, new_value = imgui.checkbox("Render simulation", sim.render)
                    if changed:
                        sim.render = new_value
                    imgui.text(f"ON time: {get_time_as_str(sim.time_passed)}")

                    imgui.text("Policy: " + str(sim.policy.name))

                    imgui.text(f"Episode: {sim.handler.episode}")
                    imgui.text(f"Solution: {sim.handler.solution_idx + 1}/{sim.handler.solutions_n}")
                    imgui.text(f"Iterations {sim.env.step_ctr}/{sim.env.max_steps}")
                    imgui.text(f"Evaluation iteration {sim.eval_iter + 1}/{sim.eval_iters}")
                    imgui.text(f"Best score: {-sim.handler.best_loss}")

                    #  TERRAIN INFO
                    terrain_info_gui(sim)

                    if imgui.tree_node("Environment"):
                        imgui.text("Observation:")
                        imgui.text(str(sim.obs))
                        imgui.text("Action:")
                        imgui.text(str(sim.act))
                        if sim.env.noise_level > 0.0:
                            imgui.text("Noise:")
                            imgui.text(str(sim.env.noise))
                        imgui.tree_pop()

                    if imgui.tree_node("Scores"):
                        if imgui.tree_node("Current"):
                            for score in sim.handler.losses:
                                imgui.text(str(-score))
                            imgui.tree_pop()
                        if imgui.tree_node("Graphs"):
                            graph_width = imgui.get_content_region_available_width()
                            imgui.plot_lines("Average score", np.array(sim.handler.average_scores, dtype=np.float32),
                                             graph_size=(graph_width, 80), values_offset=self.graph_value_offset, values_count=self.graph_values_count)
                            imgui.tree_pop()
                        imgui.tree_pop()

                    if imgui.tree_node("Weights"):
                        if imgui.tree_node("Current best weights"):
                            imgui.text(str(sim.handler.best_w))
                            imgui.tree_pop()
                        if imgui.tree_node("Graphs"):
                            graph_width = imgui.get_content_region_available_width()
                            graph_height = 80
                            imgui.plot_lines("Weights median", np.array(sim.handler.weights_median, dtype=np.float32),
                                             graph_size=(graph_width, graph_height))
                            imgui.plot_lines("Weights min", np.array(sim.handler.weights_min, dtype=np.float32), graph_size=(graph_width, graph_height))
                            imgui.plot_lines("Weights max", np.array(sim.handler.weights_max, dtype=np.float32), graph_size=(graph_width, graph_height))
                            imgui.tree_pop()
                        imgui.tree_pop()

                    reward_info(sim)

                    changed, new_value = imgui.drag_int("N iterations per solution", sim.env.max_steps, 1, 0, 1000000)
                    if changed:
                        sim.env.max_steps = new_value
                    changed, new_value = imgui.drag_int("Evaluation iterations", sim.eval_iters, 1, 0, 1000000)
                    if changed:
                        sim.eval_iters = new_value

                    if sim.env.animate:
                        changed, new_value = imgui.drag_float("Animation ms per iter", sim.env.animation_speed, 0.001, 0.0, 10, "%.3f", 1.0)
                        if changed:
                            sim.env.animation_speed = new_value

                    if imgui.tree_node("Export training data"):

                        changed, new_value = imgui.input_text("##Name", sim.export_name, 256)
                        if changed:
                            sim.export_name = new_value

                        imgui.same_line()
                        if imgui.button("Export"):
                            export_plot(sim.export_name, sim.handler.average_scores)
                            f = open("logs/training_log/" + sim.export_name + ".txt", "w")
                            f.write(f"Training time [s]: {get_time_as_str(sim.time_passed)}\n")
                            f.write(f"Optimizer: {sim.handler.es.name}\n")
                            f.write(f"Budget: {sim.handler.es.budget}\n")
                            f.write(f"Number of solutions: {sim.handler.es.solutions_n}\n")
                            f.write(f"Number of episodes: {sim.handler.episode}\n")
                            f.write(f"Evaluation iterations: {sim.eval_iters}\n")
                            f.write(f"Number of iterations: {sim.env.max_steps}\n")
                            f.write(f"Noise level: {sim.env.noise_level}\n")
                            f.write(f"Start position: {sim.env.start_position}\n")
                            f.write(f"Terrain: {sim.terrain.name}\n")
                            f.write(f"Hexapod: {sim.hexapod_params.name}\n")
                            f.write(f"Best training score: {-sim.handler.best_loss}\n")
                            f.write(f"{sim.handler.average_scores}\n\n")
                            f.write("Policy:\n")
                            f.write(f"Name: {sim.policy.name}\n")
                            f.write(f"Number of parameters: {sim.policy.get_num_params()}\n")
                            f.write("Parameters:\n")
                            f.write(str(sim.handler.best_w) + "\n\n")
                            f.close()

                        if imgui.button("Export whole training"):
                            export_whole_training(sim.export_name, sim.handler.training_log)

                        imgui.tree_pop()


                    imgui.new_line()

                    if imgui.button("Save best"):
                        temp = policy.Policy(sim.policy.name, sim.policy.layers, sim.handler.best_w)
                        policy.save_policy(main.gait_policy_path, temp)

                    control_buttons(sim, main.unstructured_trainer_sim, "training")

                    imgui.tree_pop()
                elif sim.pause:
                    imgui.pop_style_color(2)

                imgui.pop_id()
            imgui.separator()

        if len(main.unstructured_evaluator_sim) > 0:
            imgui.separator()
            for sim in main.unstructured_evaluator_sim:
                imgui.push_id(str(sim))
                if sim.pause:
                    imgui.push_style_color(imgui.COLOR_HEADER, 0, 1, 0, 1)
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 0, 0, 1)
                if imgui.tree_node("Unstructured Evaluator", imgui.TREE_NODE_FRAMED):
                    if sim.pause:
                        imgui.pop_style_color(2)
                    imgui.same_line()
                    imgui.text(f"{sim.eval_iter + 1}/{sim.eval_iters}")
                    changed, new_value = imgui.checkbox("Render simulation", sim.render)
                    if changed:
                        sim.render = new_value
                    imgui.text(f"ON time: {get_time_as_str(sim.time_passed)}")

                    imgui.text("Policy: " + str(sim.policy.name))

                    #  TERRAIN INFO
                    terrain_info_gui(sim)

                    if imgui.tree_node("Environment"):
                        imgui.text("Observation:")
                        imgui.text(str(sim.obs))
                        if sim.env.noise_level > 0.0:
                            imgui.text("Noise:")
                            imgui.text(str(sim.env.noise))
                        imgui.tree_pop()

                    imgui.text(f"Iterations {sim.env.step_ctr}/{sim.env.max_steps}")
                    imgui.text(f"Evaluation iteration {sim.eval_iter + 1}/{sim.eval_iters}")

                    if imgui.tree_node("Score"):
                        imgui.text(f"Distance: {sim.get_distance()}")
                        imgui.text(f"Smoothness: {sim.get_smoothness()}")
                        imgui.text(f"Work: {sim.get_work()}")
                        imgui.tree_pop()

                    reward_info(sim)

                    if sim.env.animate:
                        changed, new_value = imgui.drag_float("Animation ms per iter", sim.env.animation_speed, 0.001, 0.0, 10, "%.3f", 1.0)
                        if changed:
                            sim.env.animation_speed = new_value

                    changed, new_value = imgui.input_text("##Name", sim.export_name, 256)
                    if changed:
                        sim.export_name = new_value

                    imgui.same_line()
                    if imgui.button("Export"):
                        f = open("logs/evaluation_log/" + sim.export_name + ".txt", "w")
                        f.write(f"Evaluation time [s]: {get_time_as_str(sim.time_passed)}\n")
                        f.write(f"Evaluation iterations: {sim.eval_iters}\n")
                        f.write(f"Number of iterations: {sim.env.max_steps}\n")
                        f.write(f"Noise level: {sim.env.noise_level}\n")
                        f.write(f"Start position: {sim.env.start_position}\n")
                        f.write(f"Terrain: {sim.terrain.name}\n")
                        f.write(f"Hexapod: {sim.hexapod_params.name}\n")
                        f.write("Policy:\n")
                        f.write(f"Name: {sim.policy.name}\n")
                        f.write(f"Layers: {sim.policy.layers}\n")
                        f.write(f"Number of parameters: {sim.policy.get_num_params()}\n\n")
                        export_evaluation_score(f, sim)
                        f.close()
                    if imgui.button("Export trajectory"):
                        with open("logs/evaluation_log/trajectories/" + sim.export_name + ".trj", "wb") as f:
                            pickle.dump(sim.trajectories, f)
                    imgui.new_line()

                    control_buttons(sim, main.unstructured_evaluator_sim, "evaluation")

                    imgui.tree_pop()
                else:
                    imgui.same_line()
                    imgui.text(f"{sim.eval_iter + 1}/{sim.eval_iters}")
                    if sim.pause:
                        imgui.pop_style_color(2)

                imgui.pop_id()
            imgui.separator()

        if self.crt_t_active:

            changed, item = imgui.combo("Existing", self.t_idx, self.t_files)
            if changed:
                self.t_idx = item
                self.t_sel = src.mesh_creator.load_terrain(main.terrains_path + "/" + self.t_files[self.t_idx])
                main.terrain = self.t_sel
                print(self.t_sel)
                if isinstance(self.t_sel, src.mesh_creator.StepTerrain):
                    self.crt_t_type_idx = 1
                else:
                    self.crt_t_type_idx = 0

            imgui.separator()
            changed, new_value = imgui.input_text("Name", self.t_sel.name, 256)
            if changed:
                self.crt_t_name = new_value
                if self.t_sel is not None:
                    self.t_sel.name = new_value

            changed, item = imgui.combo("Type", self.crt_t_type_idx, ["Simplex", "Step", "Cube"])
            if changed:
                self.crt_t_type_idx = item
                if item == 0:
                    self.t_sel = src.mesh_creator.SimplexTerrain(self.crt_t_name, self.crt_t_position, self.crt_t_nx, self.crt_t_ny, self.crt_t_slope_x,
                                                                 self.crt_t_slope_y, self.crt_t_step, self.crt_t_scale)
                elif item == 1:
                    self.t_sel = src.mesh_creator.StepTerrain(self.crt_t_name, self.crt_t_position, self.crt_t_nx, self.crt_t_ny, self.crt_t_slope_x,
                                                              self.crt_t_slope_y, self.crt_t_step, self.crt_t_scale, self.crt_t_step_height,
                                                              self.crt_t_step_dist, self.crt_t_step_len, self.crt_t_step_arc)
                elif item == 2:
                    self.t_sel = src.mesh_creator.CubeTerrain(self.crt_t_name, self.crt_t_position, self.crt_t_nx, self.crt_t_ny, self.crt_t_slope_x,
                                                              self.crt_t_slope_y, self.crt_t_step, self.crt_t_scale, self.crt_t_cube_height)
                main.terrain = self.t_sel

            changed, value = imgui.checkbox("Show changes", self.crt_t_show_changes)
            if changed:
                self.crt_t_show_changes = value

            if not self.crt_t_show_changes:
                if imgui.button("Regenerate"):
                    self.t_sel.regenerate()

            changed, new_values = imgui.drag_float3("Position", *self.t_sel.position, 0.001, 0.0, 0.0, "%.2f", 1.0)
            if changed:
                self.crt_t_position = np.array(new_values)
                self.t_sel.set_position(new_values)

            changed, new_values = imgui.drag_float2("Slope", self.t_sel.slope_x, self.t_sel.slope_y, 0.0005, -1.0, 1.0, "%.3f", 1.0)
            if changed:
                self.crt_t_slope_x = new_values[0]
                self.crt_t_slope_y = new_values[1]
                self.t_sel.set_slope(*new_values)

            changed, new_values = imgui.drag_float("Distance", self.t_sel.step, 0.001, 0.0, 100000, "%.3f", 1.0)
            if changed:
                self.crt_t_step = new_values
                self.t_sel.set_step(new_values)

            changed, new_values = imgui.drag_float3("Scale", *self.t_sel.scale, 0.001, 0.00001, 1000, "%.3f", 1.0)
            if changed:
                self.crt_t_scale = new_values
                self.t_sel.set_scale(new_values)

            changed, new_values = imgui.drag_int2("Size N", *self.t_sel.get_size_n(), 1, 1, 1000)
            if changed:
                self.crt_t_nx = new_values[0]
                self.crt_t_ny = new_values[1]
                self.t_sel.set_size(*new_values)

            if self.crt_t_type_idx == 0 or self.crt_t_type_idx == 2:
                changed, new_values = imgui.drag_int("Seed", self.t_sel.seed, 1, 0, 1000000)
                if changed:
                    self.crt_t_seed = new_values
                    self.t_sel.set_seed(new_values)

                if self.crt_t_type_idx == 2:

                    changed, new_values = imgui.drag_float("Cube height", self.t_sel.cube_height, 0.001, 0.0, 10000000.0, "%.3f", 1.0)
                    if changed:
                        self.crt_t_cube_height = new_values
                        self.t_sel.set_cube_height(new_values)

            elif self.crt_t_type_idx == 1:
                changed, new_values = imgui.drag_int2("Step dist", *self.t_sel.step_dist, 1, 0, 1000)
                if changed:
                    self.crt_t_step_dist = new_values
                    self.t_sel.set_step_dist(*new_values)

                changed, new_values = imgui.drag_int2("Step length", *self.t_sel.step_len, 1, 2, 1000)
                if changed:
                    self.crt_t_step_len = new_values
                    self.t_sel.set_step_len(*new_values)

                changed, new_values = imgui.drag_float("Step height", self.t_sel.step_height, 0.001, -10000000.0, 10000000.0, "%.3f", 1.0)
                if changed:
                    self.crt_t_step_height = new_values
                    self.t_sel.set_step_height(new_values)

                changed, new_values = imgui.drag_float2("Arc", *self.t_sel.arc, 0.00001, -10.0, 10.0, "%.5f", 1.0)
                if changed:
                    self.crt_t_step_arc = new_values
                    self.t_sel.set_arc(*new_values)



            imgui.text("Center of mass: " + str(self.t_sel.origin))

            name_len = len(self.crt_t_name) > 0
            if not name_len:
                imgui.push_style_color(imgui.COLOR_TEXT, 1.0, 0.0, 0.0)
                imgui.text("Name must contain characters!")
                imgui.pop_style_color(1)

            if self.crt_t_show_changes:
                self.t_sel.regenerate()

            if name_len:
                if imgui.button("Create"):
                    self.t_sel.regenerate()
                    src.mesh_creator.save_terrain(main.terrains_path, self.t_sel)
                    self.crt_t_active = False
                    main.terrain = None
                imgui.same_line()

            if imgui.button("Cancel"):
                self.crt_t_active = False

        elif self.crt_h_active:

            changed, item = imgui.combo("Existing", self.h_idx, self.h_files)
            if changed:
                self.h_idx = item
                self.h_sel = robot.load_parameters(main.parameters_path + "/" + self.h_files[self.h_idx])
                main.hexapod.set_parameters(self.h_sel)
            imgui.separator()

            changed, new_value = imgui.input_text("Name", self.h_sel.name, 256)
            if changed:
                self.crt_h_name = new_value
                if self.h_sel is not None:
                    self.h_sel.name = new_value

            i = 0
            for params in self.h_sel.leg_params:
                imgui.push_id(params.id)
                if imgui.tree_node(params.id):
                    changed, new_value = imgui.drag_float3("Coxa joint pos", *params.def_mount, 0.001, -10, 10, "%.3f", 1.0)
                    if changed:
                        params.def_mount = [*new_value]

                    changed, new_value = imgui.drag_float2("Center position", params.def_center[0], params.def_center[2], 0.001, -10, 10, "%.3f", 1.0)
                    if changed:
                        params.def_center[0] = new_value[0]
                        params.def_center[2] = new_value[1]

                    changed, new_value = imgui.drag_float3("Segment length", *params.seg_lens, 0.001, 0, 10, "%.3f", 1.0)
                    if changed:
                        params.seg_lens = [*new_value]

                    changed, new_value = imgui.drag_float("Range", params.range, 0.001, 0, 10, "%.3f", 1.0)
                    if changed:
                        params.range = new_value

                    changed, new_value = imgui.slider_float("Orientation", np.rad2deg(params.def_orientation), -180, 180, "%.1f")
                    if changed:
                        params.def_orientation = np.deg2rad(new_value)

                    changed, new_value = imgui.slider_float3("Theta (HD angle offset)", *np.rad2deg(params.dh_offsets), -180, 180)
                    if changed:
                        params.dh_offsets[:] = [*np.deg2rad(new_value)]

                    imgui.new_line()
                    changed, new_value = imgui.slider_float3("Angles", *np.rad2deg(main.state.joint_angles[i * 3: (i + 1) * 3]), -180, 180)
                    if changed:
                        new_value = np.deg2rad(new_value)
                        main.state.joint_angles[i * 3] = new_value[0]
                        main.state.joint_angles[i * 3 + 1] = new_value[1]
                        main.state.joint_angles[i * 3 + 2] = new_value[2]

                    if imgui.button("Reset angles"):
                        main.state.joint_angles[i * 3: (i + 1) * 3] = [0., 0., 0.]

                    imgui.tree_pop()
                i += 1

                imgui.pop_id()
            name_len = len(self.crt_h_name) > 0
            if not name_len:
                imgui.push_style_color(imgui.COLOR_TEXT, 1.0, 0.0, 0.0)
                imgui.text("Name must contain characters!")
                imgui.pop_style_color(1)

            main.hexapod.set_parameters(self.h_sel)

            if name_len:
                if imgui.button("Create"):
                    robot.save_parameters(main.parameters_path, self.h_sel)
                    self.crt_h_active = False
                    main.terrain = None
                imgui.same_line()

            if imgui.button("Cancel"):
                self.crt_h_active = False

        elif self.crt_po_active:

            path = main.gait_policy_path if self.crt_po_type == 0 else main.body_height_policy_path if self.crt_po_type == 1 else main.leg_height_policy_path
            files = self.gait_po_files if self.crt_po_type == 0 else self.body_height_po_files if self.crt_po_type == 1 else self.leg_height_po_files

            changed, item = imgui.combo("Policy type", self.crt_po_type, ["Gait", "Body height", "Leg height"])
            if changed:
                self.crt_po_type = item
                self.crt_po_idx = 0
                new_pol = policy.load_policy(path + "/" + files[self.crt_po_idx])
                self.crt_po_sel = new_pol
                self.crt_po_name = new_pol.name
                self.crt_layers = new_pol.layers
                s = ""
                for layer in new_pol.layers:
                    s += str(layer) + " "
                self.crt_layers_txt = s

            imgui.text("Name:")
            imgui.same_line()
            changed, new_value = imgui.input_text("##Name", self.crt_po_name, 256)
            if changed:
                self.crt_po_name = new_value

            if self.crt_po_w_type != 2:
                changed, new_value = imgui.input_text("Layers", self.crt_layers_txt, 256)
                if changed:
                    self.crt_layers_txt = new_value
                    self.crt_layers = []
                    for layer in self.crt_layers_txt.split(" "):
                        if layer != '':
                            self.crt_layers.append(int(layer))
            else:
                imgui.text("Layers: " + str(self.crt_layers))

            num_params = 0
            for i in range(len(self.crt_layers) - 1):
                num_params += self.crt_layers[i + 1] * (self.crt_layers[i] + 1)

            imgui.text(f"Number of weights: {num_params}")

            changed, item = imgui.combo("Weights", self.crt_po_w_type, ["Zeros", "Random", "Existing", "Custom"])
            if changed:
                self.crt_po_w_type = item
                if self.crt_po_w_type == 2:
                    self.crt_po_idx = 0
                    new_pol = policy.load_policy(path + "/" + files[self.crt_po_idx])
                    self.crt_po_sel = new_pol
                    self.crt_po_name = new_pol.name
                    s = ""
                    for layer in new_pol.layers:
                        s += str(layer) + " "
                    self.crt_layers_txt = s

            if not len(self.crt_po_name) > 0:
                imgui.push_style_color(imgui.COLOR_TEXT, 1.0, 0.0, 0.0)
                imgui.text("Name must contain characters!")
                imgui.pop_style_color(1)

            if self.crt_po_w_type == 2:
                imgui.text("choose policies:")
                imgui.same_line()
                changed, item = imgui.combo("##comb", self.crt_po_idx, files)
                if changed:
                    self.crt_po_idx = item
                    new_pol = policy.load_policy(path + "/" + files[self.crt_po_idx])
                    self.crt_po_sel = new_pol
                    self.crt_po_name = new_pol.name
                    self.crt_layers = new_pol.layers
                    s = ""
                    for layer in new_pol.layers:
                        s += str(layer) + " "
                    self.crt_layers_txt = s
                if self.crt_po_sel is not None:
                    if imgui.tree_node("Weights:"):
                        imgui.text(str(self.crt_po_sel.get_params()))
                        imgui.tree_pop()

            if self.crt_po_w_type == 3:
                changed, new_value = imgui.input_text("Weights", self.crt_po_input_weights, 10000000)
                if changed:
                    self.crt_po_input_weights = new_value

            if imgui.button("Create"):
                layers = []
                for layer in self.crt_layers_txt.split(" "):
                    if layer != '':
                        layers.append(int(layer))

                p = policy.Policy(self.crt_po_name, layers)
                if self.crt_po_w_type == 0:
                    p.set_params(np.zeros(p.get_num_params()))
                elif self.crt_po_w_type == 1:
                    p.set_params(np.random.rand(p.get_num_params()))
                elif self.crt_po_w_type == 2:
                    p.set_params(self.crt_po_sel.get_params())
                elif self.crt_po_w_type == 3:
                    weights = []
                    for w in self.crt_po_input_weights.split(" "):
                        weights.append(float(w))
                    p.set_params(weights)
                policy.save_policy(path, p)
                self.crt_po_active = False
            imgui.same_line()

            if imgui.button("Cancel"):
                self.crt_po_active = False

        elif self.train_active:

            changed, item = imgui.combo("Mode", self.mode_idx, ["Structured", "Unstructured"])
            if changed:
                self.mode_idx = item

            imgui.separator()

            changed, item = imgui.combo("Optimizer", self.optimizer_idx, self.optimizers)
            if changed:
                self.optimizer_idx = item

            changed, new_value = imgui.drag_int("Number of solutions", self.solutions_n, 1, 1, 10000)
            if changed:
                self.solutions_n = new_value

            changed, new_value = imgui.drag_int("Budget", self.budget, 50, 1, 1000000)
            if changed:
                self.budget = new_value

            if self.mode_idx == 0:
                imgui.separator()
                imgui.push_id("gait")
                imgui.separator()

                changed, new_value = imgui.checkbox("Use gait policy", self.use_gait_policy)
                if changed:
                    self.use_gait_policy = new_value

                if self.use_gait_policy:

                    changed, new_value = imgui.checkbox("Train gait policy", self.train_gait_policy)
                    if changed:
                        self.train_gait_policy = new_value

                    if self.train_gait_policy:
                        changed, value = imgui.checkbox("Supervised learning", self.mimic)
                        if changed:
                            self.mimic = value

                    if self.train_gait_policy:

                        changed, new_value = imgui.drag_float("Gait Sigma", self.gait_sigma, 0.0001, 0.0, 10, "%.4f", 1.0)
                        if changed:
                            self.gait_sigma = new_value

                    changed, item = imgui.combo("Gait Policy", self.gait_po_idx, self.gait_po_files)
                    if changed:
                        self.gait_po_idx = item
                        self.gait_po_sel = policy.load_policy(main.gait_policy_path + "/" + self.gait_po_files[self.gait_po_idx])
                    if self.gait_po_sel is not None:
                        w = self.gait_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.gait_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                imgui.pop_id()
                imgui.push_id("torso")
                imgui.separator()

                changed, new_value = imgui.checkbox("Use torso height policy", self.use_torso_height_policy)
                if changed:
                    self.use_torso_height_policy = new_value

                if self.use_torso_height_policy:

                    changed, new_value = imgui.checkbox("Train torso height policy", self.train_body_height_policy)
                    if changed:
                        self.train_body_height_policy = new_value

                    if self.train_body_height_policy:

                        changed, new_value = imgui.drag_float("Torso Height Sigma", self.body_height_sigma, 0.0001, 0.0, 10, "%.4f", 1.0)
                        if changed:
                            self.body_height_sigma = new_value

                    changed, item = imgui.combo("Torso Height Policy", self.body_height_po_idx, self.body_height_po_files)
                    if changed:
                        self.body_height_po_idx = item
                        self.body_height_po_sel = policy.load_policy(main.body_height_policy_path + "/" + self.body_height_po_files[self.body_height_po_idx])
                    if self.body_height_po_sel is not None:
                        w = self.body_height_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.body_height_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                    imgui.new_line()

                imgui.pop_id()
                imgui.push_id("leg")
                imgui.separator()

                changed, new_value = imgui.checkbox("Use leg height policy", self.use_leg_height_policy)
                if changed:
                    self.use_leg_height_policy = new_value

                if self.use_leg_height_policy:

                    changed, new_value = imgui.checkbox("Train leg height policy", self.train_leg_height_policy)
                    if changed:
                        self.train_leg_height_policy = new_value

                    if self.train_leg_height_policy:

                        changed, new_value = imgui.drag_float("Leg Height Sigma", self.leg_height_sigma, 0.0001, 0.0, 10, "%.4f", 1.0)
                        if changed:
                            self.leg_height_sigma = new_value

                    changed, item = imgui.combo("Leg Height Policy", self.leg_height_po_idx, self.leg_height_po_files)
                    if changed:
                        self.leg_height_po_idx = item
                        self.leg_height_po_sel = policy.load_policy(main.leg_height_policy_path + "/" + self.leg_height_po_files[self.leg_height_po_idx])
                    if self.leg_height_po_sel is not None:
                        w = self.leg_height_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.leg_height_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                imgui.pop_id()
                imgui.separator()

            else:

                changed, item = imgui.combo("Policy", self.gait_po_idx, self.gait_po_files)
                if changed:
                    self.gait_po_idx = item
                    self.gait_po_sel = policy.load_policy(main.gait_policy_path + "/" + self.gait_po_files[self.gait_po_idx])
                if self.gait_po_sel is not None:
                    w = self.gait_po_sel.get_params()
                    imgui.text(f"Number of weights: {len(w)}")
                    imgui.text("Layers: " + str(self.gait_po_sel.layers))
                    if imgui.tree_node("Weights"):
                        imgui.text(f"Median: {sum(w) / len(w)}")
                        imgui.text(f"Min: {min(w)}")
                        imgui.text(f"Max: {max(w)}")
                        imgui.text(str(w))
                        imgui.tree_pop()

                changed, new_value = imgui.drag_float("Sigma", self.gait_sigma, 0.0001, 0.0, 10, "%.4f", 1.0)
                if changed:
                    self.gait_sigma = new_value

            changed, item = imgui.combo("Terrain", self.t_idx, self.t_files)
            if changed:
                self.t_idx = item
                self.t_sel = src.mesh_creator.load_terrain(main.terrains_path + "/" + self.t_files[self.t_idx])
                main.terrain = self.t_sel

            changed, item = imgui.combo("Hexapod", self.h_idx, self.h_files)
            if changed:
                self.h_idx = item
                self.h_sel = robot.load_parameters(main.parameters_path + "/" + self.h_files[self.h_idx])
                main.hexapod.set_parameters(self.h_sel)

            changed, new_value = imgui.drag_float3("Start position", *self.start_pos, 0.01, -1000, 1000, "%.3f", 1.0)
            if changed:
                self.start_pos = new_value
                main.state.position = np.array(new_value)
                main.hexapod.reset(main.state.position, main.state.rotation, main.state.joint_angles)

            if self.mode_idx == 0:

                changed, new_value = imgui.drag_float("LOD", self.lod, 0.01, 0.001, 1000, "%.3f", 1.0)
                if changed:
                    self.lod = new_value

                changed, new_value = imgui.drag_float("Body height variation", self.body_height_variation, 0.001, 0.0, 0.1, "%.3f", 1.0)
                if changed:
                    self.body_height_variation = new_value

            changed, new_value = imgui.drag_int("Maximum iterations per solution", self.max_iters, 5, 1, 1000000)
            if changed:
                self.max_iters = new_value

            changed, new_value = imgui.drag_int("Evaluation iterations", self.eval_iters, 1, 1, 1000000)
            if changed:
                self.eval_iters = new_value

            changed, new_value = imgui.drag_float("Noise level", self.noise_level, 0.0001, 0.0, 1, "%.3f", 1.0)
            if changed:
                self.noise_level = new_value

            changed, value = imgui.checkbox("Animate", self.animate)
            if changed:
                self.animate = value

            changed, value = imgui.checkbox("Pause", self.start_pause)
            if changed:
                self.start_pause = value

            if imgui.button("Start"):
                if self.mode_idx == 0:

                    params = []
                    export_name = ""
                    if self.use_gait_policy:
                        params.append([self.gait_po_sel.get_params(), self.gait_sigma])
                        export_name += "Gait"
                    if self.use_torso_height_policy:
                        params.append([self.body_height_po_sel.get_params(), self.body_height_sigma])
                        export_name += "Torso"
                    if self.use_leg_height_policy:
                        params.append([self.leg_height_po_sel.get_params(), self.leg_height_sigma])
                        export_name += "Leg"

                    optim = optimizer.Optimizer(self.optimizers[self.optimizer_idx], params, self.solutions_n, self.budget)
                    policy_handler = Handler(optim)

                    gait_pol = self.gait_po_sel if self.train_gait_policy and self.use_gait_policy else None
                    torso_pol = self.body_height_po_sel if self.train_body_height_policy and self.use_torso_height_policy else None
                    leg_pol = self.leg_height_po_sel if self.train_leg_height_policy and self.use_leg_height_policy else None

                    sim = StructuredTrainer(self.t_sel,
                                            self.start_pos,
                                            self.h_sel,
                                            self.max_iters,
                                            self.animate,
                                            self.noise_level,
                                            gait_pol,
                                            torso_pol,
                                            leg_pol,
                                            self.eval_iters,
                                            self.mimic,
                                            policy_handler,
                                            self.lod)
                    sim.export_name = export_name
                    sim.body_height_variation = self.body_height_variation
                    main.structured_trainer_sim.append(sim)
                else:
                    optim = optimizer.Optimizer(self.optimizers[self.optimizer_idx], [[self.gait_po_sel.get_params(), self.gait_sigma]], self.solutions_n, self.budget)
                    policy_handler = Handler(optim)

                    sim = UnstructuredTrainer(self.t_sel,
                                              self.start_pos,
                                              self.h_sel,
                                              policy_handler,
                                              self.gait_po_sel,
                                              self.animate,
                                              self.max_iters,
                                              self.eval_iters,
                                              self.noise_level)
                    sim.export_name = self.gait_po_sel.name + self.t_sel.name
                    main.unstructured_trainer_sim.append(sim)

                if self.start_pause:
                    sim.pause = True

                main.terrain = None
                self.train_active = False

            imgui.same_line()
            if imgui.button("Cancel"):
                self.train_active = False
                main.terrain = None

        elif self.test_active:

            changed, item = imgui.combo("Mode", self.mode_idx, ["Structured", "Unstructured"])
            if changed:
                self.mode_idx = item

            imgui.separator()

            if self.mode_idx == 0:

                changed, new_value = imgui.checkbox("Use gait policy", self.use_gait_policy)
                if changed:
                    self.use_gait_policy = new_value

                if self.use_gait_policy:

                    imgui.push_id("gait")
                    changed, item = imgui.combo("Gait Policy", self.gait_po_idx, self.gait_po_files)
                    if changed:
                        self.gait_po_idx = item
                        self.gait_po_sel = policy.load_policy(main.gait_policy_path + "/" + self.gait_po_files[self.gait_po_idx])
                    if self.gait_po_sel is not None:
                        w = self.gait_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.gait_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                    if self.mode_idx == 0:
                        changed, new_value = imgui.input_text("Leg layers", self.layers, 256)
                        if changed:
                            self.layers = new_value

                    imgui.new_line()
                    imgui.pop_id()

                    layers = []
                    for layer in self.layers.split(" "):
                        if layer != '':
                            layers.append(int(layer))
                    num_params_per_leg = 0
                    for i in range(len(layers) - 1):
                        num_params_per_leg += layers[i + 1] * (layers[i] + 1)
                    imgui.text(f"Parameters per leg: {num_params_per_leg}")
                    imgui.text(f"Total number of parameters: {num_params_per_leg * (3 if self.bilateral_weight_sharing else 6)}")

                changed, new_value = imgui.checkbox("Use body height policy", self.use_torso_height_policy)
                if changed:
                    self.use_torso_height_policy = new_value

                if self.use_torso_height_policy:

                    imgui.push_id("body")
                    changed, item = imgui.combo("Body Height Policy", self.body_height_po_idx, self.body_height_po_files)
                    if changed:
                        self.body_height_po_idx = item
                        self.body_height_po_sel = policy.load_policy(main.body_height_policy_path + "/" + self.body_height_po_files[self.body_height_po_idx])
                    if self.body_height_po_sel is not None:
                        w = self.body_height_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.body_height_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                    imgui.new_line()
                    imgui.pop_id()

                changed, new_value = imgui.checkbox("Use leg height policy", self.use_leg_height_policy)
                if changed:
                    self.use_leg_height_policy = new_value

                if self.use_leg_height_policy:

                    imgui.push_id("leg")
                    changed, item = imgui.combo("Leg Height Policy", self.leg_height_po_idx, self.leg_height_po_files)
                    if changed:
                        self.leg_height_po_idx = item
                        self.leg_height_po_sel = policy.load_policy(main.leg_height_policy_path + "/" + self.leg_height_po_files[self.leg_height_po_idx])
                    if self.leg_height_po_sel is not None:
                        w = self.leg_height_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.leg_height_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                    imgui.pop_id()
                imgui.separator()

            else:
                imgui.push_id("gait")
                changed, item = imgui.combo("Policy", self.gait_po_idx, self.gait_po_files)
                if changed:
                    self.gait_po_idx = item
                    self.gait_po_sel = policy.load_policy(main.gait_policy_path + "/" + self.gait_po_files[self.gait_po_idx])
                if self.gait_po_sel is not None:
                    w = self.gait_po_sel.get_params()
                    imgui.text(f"Number of weights: {len(w)}")
                    imgui.text("Layers: " + str(self.gait_po_sel.layers))
                    if imgui.tree_node("Weights"):
                        imgui.text(f"Median: {sum(w) / len(w)}")
                        imgui.text(f"Min: {min(w)}")
                        imgui.text(f"Max: {max(w)}")
                        imgui.text(str(w))
                        imgui.tree_pop()

                imgui.pop_id()

            changed, item = imgui.combo("Terrain", self.t_idx, self.t_files)
            if changed:
                self.t_idx = item
                self.t_sel = src.mesh_creator.load_terrain(main.terrains_path + "/" + self.t_files[self.t_idx])
                main.terrain = self.t_sel

            changed, item = imgui.combo("Hexapod", self.h_idx, self.h_files)
            if changed:
                self.h_idx = item
                self.h_sel = robot.load_parameters(main.parameters_path + "/" + self.h_files[self.h_idx])
                main.hexapod.set_parameters(self.h_sel)

            changed, new_value = imgui.drag_float3("Start position", *self.start_pos, 0.01, -1000, 1000, "%.3f", 1.0)
            if changed:
                self.start_pos = new_value
                main.state.position = np.array(new_value)
                main.hexapod.reset(main.state.position, main.state.rotation, main.state.joint_angles)

            changed, new_value = imgui.drag_int("Maximum iterations per solution", self.max_iters, 1, 1, 1000000)
            if changed:
                self.max_iters = new_value

            changed, new_value = imgui.drag_float("Noise level", self.noise_level, 0.0001, 0.0, 1, "%.3f", 1.0)
            if changed:
                self.noise_level = new_value

            changed, value = imgui.checkbox("Auto reset", self.auto_reset)
            if changed:
                self.auto_reset = value

            changed, value = imgui.checkbox("Animate", self.animate)
            if changed:
                self.animate = value

            changed, value = imgui.checkbox("Pause", self.start_pause)
            if changed:
                self.start_pause = value

            if imgui.button("Start"):
                if self.mode_idx == 0:
                    layers = []
                    for layer in self.layers.split(" "):
                        if layer != '':
                            layers.append(int(layer))

                    num_gait_handlers = 1 if self.gait_train_all_together else len(main.hexapod.legs) // 2 if self.bilateral_weight_sharing else len(
                        main.hexapod.legs)

                    names = [self.gait_po_sel.name] if self.gait_train_all_together else [
                        main.hexapod.legs[i].params.id + "_" + main.hexapod.legs[i + 3].params.id for i in
                        range(num_gait_handlers)] if self.bilateral_weight_sharing else [main.hexapod.legs[i].params.id for i in range(num_gait_handlers)]

                    w = self.gait_po_sel.get_params()
                    w = w.reshape((num_gait_handlers, int(len(w) / num_gait_handlers)))

                    gait_handlers = [Handler(policy.Policy(names[i], layers, w[i])) for i in range(num_gait_handlers)]

                    body_height_handler = Handler(self.body_height_po_sel)
                    leg_height_handler = Handler(self.leg_height_po_sel)

                    sim = StructuredTester(self.t_sel, self.start_pos, self.h_sel, self.max_iters, self.animate, self.noise_level, gait_handlers,
                                           self.bilateral_weight_sharing,
                                           self.use_gait_policy, body_height_handler, self.use_torso_height_policy, leg_height_handler,
                                           self.use_leg_height_policy, self.auto_reset)
                    main.structured_tester_sim.append(sim)
                else:
                    policy_handler = Handler(self.gait_po_sel)

                    sim = UnstructuredTester(self.t_sel, self.start_pos, self.h_sel, policy_handler, self.max_iters, self.animate, self.noise_level,
                                             self.auto_reset)
                    main.unstructured_tester_sim.append(sim)
                self.test_active = False
                main.terrain = None

            imgui.same_line()
            if imgui.button("Cancel"):
                self.test_active = False
                main.terrain = None

        elif self.eval_active:

            changed, item = imgui.combo("Mode", self.mode_idx, ["Structured", "Unstructured"])
            if changed:
                self.mode_idx = item

            imgui.separator()

            if self.mode_idx == 0:

                changed, new_value = imgui.checkbox("Evaluate all", self.evaluate_all)
                if changed:
                    self.evaluate_all = new_value

                changed, new_value = imgui.checkbox("Use gait policy", self.use_gait_policy)
                if changed:
                    self.use_gait_policy = new_value

                if self.use_gait_policy or self.evaluate_all:

                    imgui.push_id("gait")
                    changed, item = imgui.combo("Gait Policy", self.gait_po_idx, self.gait_po_files)
                    if changed:
                        self.gait_po_idx = item
                        self.gait_po_sel = policy.load_policy(main.gait_policy_path + "/" + self.gait_po_files[self.gait_po_idx])
                    if self.gait_po_sel is not None:
                        w = self.gait_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.gait_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                    imgui.new_line()
                    imgui.pop_id()

                changed, new_value = imgui.checkbox("Use torso height policy", self.use_torso_height_policy)
                if changed:
                    self.use_torso_height_policy = new_value

                if self.use_torso_height_policy or self.evaluate_all:

                    imgui.push_id("torso")
                    changed, item = imgui.combo("Torso Height Policy", self.body_height_po_idx, self.body_height_po_files)
                    if changed:
                        self.body_height_po_idx = item
                        self.body_height_po_sel = policy.load_policy(main.body_height_policy_path + "/" + self.body_height_po_files[self.body_height_po_idx])
                    if self.body_height_po_sel is not None:
                        w = self.body_height_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.body_height_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                    imgui.new_line()
                    imgui.pop_id()

                changed, new_value = imgui.checkbox("Use leg height policy", self.use_leg_height_policy)
                if changed:
                    self.use_leg_height_policy = new_value

                if self.use_leg_height_policy or self.evaluate_all:

                    imgui.push_id("leg")
                    changed, item = imgui.combo("Leg Height Policy", self.leg_height_po_idx, self.leg_height_po_files)
                    if changed:
                        self.leg_height_po_idx = item
                        self.leg_height_po_sel = policy.load_policy(main.leg_height_policy_path + "/" + self.leg_height_po_files[self.leg_height_po_idx])
                    if self.leg_height_po_sel is not None:
                        w = self.leg_height_po_sel.get_params()
                        imgui.text(f"Number of weights: {len(w)}")
                        imgui.text("Layers: " + str(self.leg_height_po_sel.layers))
                        if imgui.tree_node("Weights"):
                            imgui.text(f"Median: {sum(w) / len(w)}")
                            imgui.text(f"Min: {min(w)}")
                            imgui.text(f"Max: {max(w)}")
                            imgui.text(str(w))
                            imgui.tree_pop()

                    imgui.pop_id()
                imgui.separator()

            elif self.mode_idx == 1:
                changed, item = imgui.combo("Policy", self.gait_po_idx, self.gait_po_files)
                if changed:
                    self.gait_po_idx = item
                    self.gait_po_sel = policy.load_policy(main.gait_policy_path + "/" + self.gait_po_files[self.gait_po_idx])
                if self.gait_po_sel is not None:
                    w = self.gait_po_sel.get_params()
                    imgui.text(f"Number of weights: {len(w)}")
                    imgui.text("Layers: " + str(self.gait_po_sel.layers))
                    if imgui.tree_node("Weights"):
                        imgui.text(f"Median: {sum(w) / len(w)}")
                        imgui.text(f"Min: {min(w)}")
                        imgui.text(f"Max: {max(w)}")
                        imgui.text(str(w))
                        imgui.tree_pop()

            changed, item = imgui.combo("Terrain", self.t_idx, self.t_files)
            if changed:
                self.t_idx = item
                self.t_sel = src.mesh_creator.load_terrain(main.terrains_path + "/" + self.t_files[self.t_idx])
                main.terrain = self.t_sel

            changed, item = imgui.combo("Hexapod", self.h_idx, self.h_files)
            if changed:
                self.h_idx = item
                self.h_sel = robot.load_parameters(main.parameters_path + "/" + self.h_files[self.h_idx])
                main.hexapod.set_parameters(self.h_sel)

            if self.mode_idx == 0:
                changed, new_value = imgui.drag_float("LOD", self.lod, 0.01, 0.001, 1000, "%.3f", 1.0)
                if changed:
                    self.lod = new_value

            changed, new_value = imgui.drag_float3("Start position", *self.start_pos, 0.01, -1000, 1000, "%.3f", 1.0)
            if changed:
                self.start_pos = new_value
                main.state.position = np.array(new_value)
                main.hexapod.reset(main.state.position, main.state.rotation, main.state.joint_angles)

            changed, new_value = imgui.drag_int("Maximum iterations per solution", self.max_iters, 1, 1, 1000000)
            if changed:
                self.max_iters = new_value

            changed, new_value = imgui.drag_int("Evaluation iterations", self.eval_iters, 1, 1, 1000000)
            if changed:
                self.eval_iters = new_value

            changed, new_value = imgui.drag_float("Noise level", self.noise_level, 0.0001, 0.0, 1, "%.3f", 1.0)
            if changed:
                self.noise_level = new_value

            changed, value = imgui.checkbox("Animate", self.animate)
            if changed:
                self.animate = value

            changed, value = imgui.checkbox("Pause", self.start_pause)
            if changed:
                self.start_pause = value

            if imgui.button("Start"):
                if self.mode_idx == 0:

                    gait_pol = self.gait_po_sel if (self.train_gait_policy and self.use_gait_policy) or self.evaluate_all else None
                    torso_pol = self.body_height_po_sel if (self.train_body_height_policy and self.use_torso_height_policy) or self.evaluate_all else None
                    leg_pol = self.leg_height_po_sel if (self.train_leg_height_policy and self.use_leg_height_policy) or self.evaluate_all else None

                    def create_simulation(gait, torso, leg):
                        export_file_name = ""
                        if gait is not None:
                            export_file_name += "Gait"
                        if torso is not None:
                            export_file_name += "Torso"
                        if leg is not None:
                            export_file_name += "Leg"
                        if export_file_name == "":
                            export_file_name = "Raw"

                        simulation = StructuredEvaluator(self.t_sel,
                                                         self.start_pos,
                                                         self.h_sel,
                                                         self.max_iters,
                                                         self.animate and not self.evaluate_all,
                                                         self.noise_level,
                                                         gait,
                                                         torso,
                                                         leg,
                                                         self.eval_iters,
                                                         self.lod)
                        simulation.export_name = export_file_name+self.t_sel.name
                        main.structured_evaluator_sim.append(simulation)

                    if self.evaluate_all:
                        create_simulation(None, None, None)
                        create_simulation(gait_pol, None, None)
                        create_simulation(None, torso_pol, None)
                        create_simulation(None, None, leg_pol)
                    else:
                        create_simulation(gait_pol, torso_pol, leg_pol)
                else:

                    sim = UnstructuredEvaluator(self.t_sel,
                                                self.start_pos,
                                                self.h_sel,
                                                self.gait_po_sel,
                                                self.max_iters,
                                                self.animate,
                                                self.noise_level,
                                                self.eval_iters)
                    sim.export_name = self.gait_po_sel.name + self.t_sel.name
                    main.unstructured_evaluator_sim.append(sim)

                if self.start_pause:
                    sim.pause = True

                main.terrain = None
                self.eval_active = False

            imgui.same_line()
            if imgui.button("Cancel"):
                self.eval_active = False
                main.terrain = None

        elif self.tr_log_active:
            changed, item = imgui.combo("##Files", self.tr_log_idx, self.tr_log_files)
            if changed:
                self.tr_log_idx = item
            imgui.same_line()
            if imgui.button("Load"):
                self.tr_log_sel = np.load("logs/training_log/whole_training/"+self.tr_log_files[self.tr_log_idx], allow_pickle=True)
                self.tr_log_sel = sorted(self.tr_log_sel, key=lambda log_item: log_item[0], reverse=True)
                self.tr_log_max_rew = self.tr_log_sel[0][0]
                self.tr_log_min_rew = self.tr_log_sel[-1][0]
            imgui.same_line()
            if imgui.button("Back"):
                self.tr_log_active = False
                self.tr_log_C = None

            if self.tr_log_sel is not None:

                if imgui.button("Process"):
                    A = np.array([arr[1].tolist() for arr in self.tr_log_sel])
                    pca = PCA(n_components=2)
                    pca.fit(A)
                    X = pca.components_
                    self.tr_log_C = X @ np.transpose(A)
                    self.tr_log_max_C_value_X = max(self.tr_log_C[0])
                    self.tr_log_min_C_value_X = min(self.tr_log_C[0])
                    self.tr_log_max_C_value_Y = max(self.tr_log_C[1])
                    self.tr_log_min_C_value_Y = min(self.tr_log_C[1])
                    self.tr_log_C = np.transpose(self.tr_log_C)

                if imgui.tree_node("Image"):

                    update_image = False
                    changed, val = imgui.drag_int2("Image size", self.tr_log_image_w, self.tr_log_image_h, 1, 1, 1000000)
                    if changed:
                        self.tr_log_image_w = val[0]
                        self.tr_log_image_h = val[1]
                        if self.tr_log_image_id != -1:
                            update_image = True

                    if imgui.button("Generate image"):
                        update_image = True

                    if update_image:
                        color_component_n = 3
                        max_pixel_value = 1.0
                        buffer = np.zeros(self.tr_log_image_w * self.tr_log_image_h * color_component_n, dtype=np.float32)
                        for i in range(len(self.tr_log_C)):
                            x = int(my_math.map(self.tr_log_C[i][0], self.tr_log_min_C_value_X, self.tr_log_max_C_value_X, 0, self.tr_log_image_w-1))
                            y = int(my_math.map(self.tr_log_C[i][1], self.tr_log_min_C_value_Y, self.tr_log_max_C_value_Y, 0, self.tr_log_image_h-1))
                            pix = float(my_math.map(self.tr_log_sel[i][0], self.tr_log_min_rew, self.tr_log_max_rew, 0, max_pixel_value))

                            idx = (self.tr_log_image_w * y + x) * color_component_n
                            buffer[idx + 0] = -(pix - 1)**2 + 1
                            buffer[idx + 1] = -pix**2 + 1
                            buffer[idx + 2] = 0

                        if self.tr_log_image_id == -1:
                            self.tr_log_image_id = glGenTextures(1)
                        glBindTexture(GL_TEXTURE_2D, self.tr_log_image_id)
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
                        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
                        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, self.tr_log_image_w, self.tr_log_image_h, 0, GL_RGB, GL_FLOAT, buffer)
                        glBindTexture(GL_TEXTURE_2D, 0)

                    imgui.tree_pop()

                    if self.tr_log_image_id != -1:
                        changed, val = imgui.drag_int2("Preview size", self.tr_log_image_view_w, self.tr_log_image_view_h, 1, 0, 10000)
                        if changed:
                            self.tr_log_image_view_w = val[0]
                            self.tr_log_image_view_h = val[1]

                        imgui.image(self.tr_log_image_id, self.tr_log_image_view_w, self.tr_log_image_view_h, border_color=(0.5, 0.5, 0.5, 1))

                if imgui.tree_node("Data"):

                    imgui.text(f"Min reward: {self.tr_log_min_rew}")
                    imgui.text(f"Max reward: {self.tr_log_max_rew}")
                    imgui.text(f"Amount: {len(self.tr_log_sel)}")

                    changed, value = imgui.checkbox("Display parameter vectors", self.tr_log_disp_param_vec)
                    if changed:
                        self.tr_log_disp_param_vec = value

                    if self.tr_log_disp_param_vec:
                        imgui.columns(2, "Table")
                        imgui.set_column_offset(1, 200)
                    else:
                        imgui.columns(1, "Table")

                    imgui.text("Rewards")
                    imgui.next_column()
                    if self.tr_log_disp_param_vec:
                        imgui.text("Parameter vectors")
                        imgui.next_column()
                    imgui.separator()
                    for item in self.tr_log_sel:
                        imgui.text(str(item[0]))
                        imgui.next_column()
                        if self.tr_log_disp_param_vec:
                            imgui.text(str(item[1]).replace('\n', ''))
                            imgui.next_column()
                        imgui.separator()
                    imgui.columns(1)
                    imgui.tree_pop()

                if self.tr_log_C is not None:
                    if imgui.tree_node("PCA"):
                        imgui.columns(2)
                        imgui.text("X")
                        imgui.next_column()
                        imgui.text("Y")
                        imgui.next_column()
                        imgui.separator()
                        imgui.separator()
                        imgui.text(f"Min: {self.tr_log_min_C_value_X}")
                        imgui.text(f"Max: {self.tr_log_max_C_value_X}")
                        imgui.next_column()
                        imgui.text(f"Min: {self.tr_log_min_C_value_Y}")
                        imgui.text(f"Max: {self.tr_log_max_C_value_Y}")
                        imgui.next_column()
                        imgui.separator()
                        imgui.separator()

                        for item in self.tr_log_C:
                            imgui.text(str(item[0]))
                            imgui.next_column()
                            imgui.text(str(item[1]))
                            imgui.next_column()
                            imgui.separator()
                        imgui.columns(1)
                        imgui.tree_pop()


        else:
            if imgui.button("Start training"):
                self.train_active = True
                self.gait_po_files, self.gait_po_idx, self.gait_po_sel = load_policies(main.gait_policy_path, self.gait_po_idx)
                self.body_height_po_files, self.body_height_po_idx, self.body_height_po_sel = load_policies(main.body_height_policy_path,
                                                                                                            self.body_height_po_idx)
                self.leg_height_po_files, self.leg_height_po_idx, self.leg_height_po_sel = load_policies(main.leg_height_policy_path,
                                                                                                         self.leg_height_po_idx)
                self.load_terrains(main)
                self.load_hexapods(main)

            if imgui.button("Start testing"):
                self.test_active = True
                self.gait_po_files, self.gait_po_idx, self.gait_po_sel = load_policies(main.gait_policy_path, self.gait_po_idx)
                self.body_height_po_files, self.body_height_po_idx, self.body_height_po_sel = load_policies(main.body_height_policy_path,
                                                                                                            self.body_height_po_idx)
                self.leg_height_po_files, self.leg_height_po_idx, self.leg_height_po_sel = load_policies(main.leg_height_policy_path,
                                                                                                         self.leg_height_po_idx)
                self.load_terrains(main)
                self.load_hexapods(main)

            if imgui.button("Start evaluating"):
                self.eval_active = True
                self.gait_po_files, self.gait_po_idx, self.gait_po_sel = load_policies(main.gait_policy_path, self.gait_po_idx)
                self.body_height_po_files, self.body_height_po_idx, self.body_height_po_sel = load_policies(main.body_height_policy_path,
                                                                                                            self.body_height_po_idx)
                self.leg_height_po_files, self.leg_height_po_idx, self.leg_height_po_sel = load_policies(main.leg_height_policy_path,
                                                                                                         self.leg_height_po_idx)
                self.load_terrains(main)
                self.load_hexapods(main)
                main.state.position = np.array(self.start_pos)
                main.hexapod.reset(main.state.position, main.state.rotation, main.state.joint_angles)

            if imgui.button("Training analysis"):
                self.tr_log_active = True
                self.tr_log_files = os.listdir("logs/training_log/whole_training")

            '''TERRAIN////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Terrain", imgui.TREE_NODE_FRAMED):
                imgui.unindent()

                if imgui.button("Create terrain"):
                    self.t_files = os.listdir(main.terrains_path)
                    self.t_files.sort()
                    self.crt_t_active = True
                    self.crt_t_type_idx = 0
                    self.t_sel = src.mesh_creator.SimplexTerrain(self.crt_t_name, self.crt_t_position, self.crt_t_nx, self.crt_t_ny, self.crt_t_slope_x,
                                                                 self.crt_t_slope_y, self.crt_t_step, self.crt_t_scale)
                    main.terrain = self.t_sel

                changed, value = imgui.checkbox("Render terrain", visual_params.render_terrain)
                if changed:
                    visual_params.render_terrain = value

                imgui.indent()
                imgui.tree_pop()
            '''HEXAPOD////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Hexapod", imgui.TREE_NODE_FRAMED):
                if imgui.button("Create hexapod"):
                    self.crt_h_active = True
                    main.terrain = main.default_plane_terrain
                    self.load_hexapods(main)
                imgui.tree_pop()

            '''POLICY////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Policy", imgui.TREE_NODE_FRAMED):
                imgui.unindent()

                if imgui.button("New policy"):
                    self.crt_po_active = True
                    self.gait_po_files, self.gait_po_idx, self.gait_po_sel = load_policies(main.gait_policy_path, self.gait_po_idx)
                    self.body_height_po_files, self.body_height_po_idx, self.body_height_po_sel = load_policies(main.body_height_policy_path,
                                                                                                                self.body_height_po_idx)
                    self.leg_height_po_files, self.leg_height_po_idx, self.leg_height_po_sel = load_policies(main.leg_height_policy_path,
                                                                                                             self.leg_height_po_idx)
                    self.crt_po_idx = 0

                imgui.indent()
                imgui.tree_pop()


    def render_gui_3d_properties(self, visual_params):

        if imgui.tree_node("Camera", imgui.TREE_NODE_FRAMED):

            changed, new_values = imgui.checkbox("Lock to robot", self.camera.lock_to_focus_point)
            if changed:
                self.camera.lock_to_focus_point = new_values

            changed, new_values = imgui.slider_float("Yaw", self.camera.yaw, -10, 10, "%.2f", 1.0)
            if changed:
                self.camera.yaw = new_values

            changed, new_values = imgui.slider_float("Pitch", self.camera.pitch, -np.pi / 2.0, np.pi / 2.0, "%.2f", 1.0)
            if changed:
                self.camera.pitch = new_values

            changed, new_values = imgui.slider_float("Zoom", self.camera.distance_from_focus_point, self.camera.near_plane, 50.0, "%.2f", 1.0)
            if changed:
                self.camera.distance_from_focus_point = new_values

            changed, new_values = imgui.slider_float("FOV", np.rad2deg(self.camera.fov), 0.01, 360, "%.2f", 1.0)
            if changed:
                self.camera.fov = np.deg2rad(new_values)

            changed, new_values = imgui.drag_float("Near plane", self.camera.near_plane, 0.01, 0.0001, 1000000, "%.2f", 1.0)
            if changed:
                self.camera.near_plane = new_values

            changed, new_values = imgui.drag_float("Far plane", self.camera.far_plane, 0.01, 0.0001, 100000000, "%.2f", 1.0)
            if changed:
                self.camera.far_plane = new_values

            imgui.tree_pop()

        if imgui.tree_node("Appearance", imgui.TREE_NODE_FRAMED):

            changed, new_values = imgui.color_edit3("Background color", *visual_params.background_color)
            if changed:
                visual_params.background_color = new_values
                glClearColor(*new_values, 1.0)

            '''LIGHT////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Light"):

                changed, new_values = imgui.color_edit3("Color", *visual_params.light_color)
                if changed:
                    visual_params.light_color = np.array(new_values)

                imgui.text("Position (spherical coordinates)")

                changed, new_values = imgui.drag_float("Radial distance", visual_params.light_position[0], 1.0, 0, 0, "%.1f", 1.0)
                if changed:
                    visual_params.light_position[0] = new_values

                changed, new_values = imgui.drag_float("Polar angle", np.rad2deg(visual_params.light_position[1]), 1.0, 0, 0, "%.3f", 1.0)
                if changed:
                    visual_params.light_position[1] = np.deg2rad(new_values)

                changed, new_values = imgui.slider_float("Azimuthal angle",
                                                         np.rad2deg(visual_params.light_position[2]), 0, 180, "%.2f", 1.0)
                if changed:
                    visual_params.light_position[2] = np.deg2rad(new_values)

                imgui.tree_pop()

            '''COORDINATE SYSTEMS////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Coordinate systems"):

                changed, new_values = imgui.drag_float("Thickness", visual_params.coordinate_system_thickness, 0.001, 0, 100, "%.3f", 1.0)
                if changed:
                    visual_params.coordinate_system_thickness = new_values

                changed, new_values = imgui.drag_float("Axis length", visual_params.coordinate_system_length, 0.001, 0, 100, "%.3f", 1.0)
                if changed:
                    visual_params.coordinate_system_length = new_values

                imgui.tree_pop()
            '''TERRAIN////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Terrain"):

                changed, value = imgui.checkbox("Render terrain", visual_params.render_terrain)
                if changed:
                    visual_params.render_terrain = value

                changed, new_values = imgui.drag_int("Point size", visual_params.terrain_point_size, 1, 1, 20)
                if changed:
                    visual_params.terrain_point_size = new_values

                changed, new_values = imgui.color_edit3("Terrain color", *visual_params.terrain_color)
                if changed:
                    visual_params.terrain_color = new_values

                imgui.tree_pop()

            '''HEIGHTMAP////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Heightmap"):

                changed, new_values = imgui.color_edit3("Heightmap color", *visual_params.heightmap_color)
                if changed:
                    visual_params.heightmap_color = new_values

                imgui.tree_pop()

            '''TRAJECTORIES////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Trajectories"):
                changed, val = imgui.checkbox("Render trajectories", visual_params.render_trajectory)
                if changed:
                    visual_params.render_trajectory = val

                changed, val = imgui.checkbox("Lines", visual_params.render_trajectory_lines)
                if changed:
                    visual_params.render_trajectory_lines = val

                changed, val = imgui.checkbox("Points", visual_params.render_trajectory_points)
                if changed:
                    visual_params.render_trajectory_points = val

                if visual_params.render_trajectory_points:
                    changed, new_values = imgui.drag_int("Trajectory point size", visual_params.trajectory_point_size, 1, 1, 20)
                    if changed:
                        visual_params.trajectory_point_size = new_values

                changed, item = imgui.combo("##traj", self.traj_idx, self.traj_files)
                if changed:
                    self.traj_idx = item
                imgui.same_line()
                if imgui.button("Add"):
                    with open("logs/evaluation_log/trajectories/"+self.traj_files[self.traj_idx], "rb") as f:
                        trajectories = pickle.load(f)
                    raw_moder = src.mesh_creator.create_trajectory(trajectories)
                    self.trajectories.append(Entity(raw_moder, [1., 1., 1.], [0., 0., 0.], [0., 0., 0.], 1.0, None, GL_LINES))
                imgui.same_line()
                if imgui.button("Refresh"):
                    self.traj_files = os.listdir("logs/evaluation_log/trajectories")
                    self.traj_files.sort()

                i = 0
                for traj in self.trajectories:
                    imgui.push_id(str(i))
                    if imgui.tree_node("Trajectory "+str(i)):
                        changed, new_values = imgui.color_edit3("Color", *traj.color)
                        if changed:
                            traj.color = new_values

                        changed, new_values = imgui.drag_float("Scale", traj.scale, 0.01, 0, 0, "%.3f", 1.0)
                        if changed:
                            traj.set_scale(new_values)
                        imgui.tree_pop()
                    i += 1
                    imgui.pop_id()
                imgui.tree_pop()

            '''HEXAPOD////////////////////////////////////////////////////////////////////////////////////////'''
            if imgui.tree_node("Hexapod"):

                changed, new_values = imgui.checkbox("Render hexapod", visual_params.render_hexapod)
                if changed:
                    visual_params.render_hexapod = new_values
                changed, new_values = imgui.checkbox("Render body", visual_params.render_hexapod_body)
                if changed:
                    visual_params.render_hexapod_body = new_values
                changed, new_values = imgui.checkbox("Render legs", visual_params.render_legs)
                if changed:
                    visual_params.render_legs = new_values
                changed, new_values = imgui.checkbox("Render range", visual_params.render_range)
                if changed:
                    visual_params.render_range = new_values
                changed, new_values = imgui.checkbox("Render position", visual_params.render_position)
                if changed:
                    visual_params.render_position = new_values
                changed, new_values = imgui.checkbox("Render target", visual_params.render_target)
                if changed:
                    visual_params.render_target = new_values
                changed, new_values = imgui.checkbox("Render coordinate systems", visual_params.render_coordinates)
                if changed:
                    visual_params.render_coordinates = new_values

                changed, new_values = imgui.drag_float("Leg thickness", visual_params.leg_thickness, 0.001, 0, 100, "%.3f", 1.0)
                if changed:
                    visual_params.leg_thickness = new_values

                changed, new_values = imgui.drag_float("Leg joint size", visual_params.joint_size, 0.001, 0, 100, "%.3f", 1.0)
                if changed:
                    visual_params.joint_size = new_values

                changed, new_values = imgui.color_edit4("Body color", *visual_params.body_color, visual_params.body_alpha)
                if changed:
                    visual_params.body_color = new_values[0:3]
                    visual_params.body_alpha = new_values[3]

                changed, new_values = imgui.color_edit3("Joint color", *visual_params.joint_color)
                if changed:
                    visual_params.joint_color = new_values

                changed, new_values = imgui.color_edit3("Center color", *visual_params.center_color)
                if changed:
                    visual_params.center_color = new_values

                changed, new_values = imgui.color_edit3("Range color", *visual_params.range_color)
                if changed:
                    visual_params.range_color = new_values

                imgui.text("Leg state colors:")
                changed, new_values = imgui.color_edit3("Stuck color", *visual_params.stuck_color)
                if changed:
                    visual_params.background_color = new_values

                changed, new_values = imgui.color_edit3("Stance color", *visual_params.stance_color)
                if changed:
                    visual_params.stance_color = new_values

                changed, new_values = imgui.color_edit3("Swing color", *visual_params.swing_color)
                if changed:
                    visual_params.swing_color = new_values

                changed, new_values = imgui.slider_float("Leg color alpha", visual_params.leg_alpha, 0.0, 1.0)
                if changed:
                    visual_params.leg_alpha = new_values

                for l_params in visual_params.leg_params:
                    imgui.push_id(l_params.id)
                    if imgui.tree_node(l_params.id):
                        if not visual_params.render_legs:
                            changed, new_values = imgui.checkbox("Render", l_params.render)
                            if changed:
                                l_params.render = new_values

                        if not visual_params.render_range:
                            changed, new_values = imgui.checkbox("Render range", l_params.render_range)
                            if changed:
                                l_params.render_range = new_values

                        if not visual_params.render_coordinates:
                            changed, new_values = imgui.checkbox("Render coordinates", l_params.render_coordinates)
                            if changed:
                                l_params.render_coordinates = new_values

                        imgui.tree_pop()
                    imgui.pop_id()

                imgui.tree_pop()

            imgui.tree_pop()

    def load_terrains(self, main):
        self.t_files = os.listdir(main.terrains_path)
        self.t_files.sort()
        if "Flat.ter" in self.t_files:
            self.t_files.remove("Flat.ter")
            self.t_files = ["Flat.ter"] + self.t_files
        main.state.position = np.array(self.start_pos)
        main.hexapod.reset(main.state.position, main.state.rotation, main.state.joint_angles)
        if len(self.t_files) > 0:
            if 0 > self.t_idx >= len(self.t_files):
                self.t_idx = 0
            self.t_sel = src.mesh_creator.load_terrain(main.terrains_path + "/" + self.t_files[self.t_idx])
            main.terrain = self.t_sel
        else:
            self.t_sel = None
            self.t_idx = 0
            main.terrain = None

    def load_hexapods(self, main):
        self.h_files = os.listdir(main.parameters_path)
        self.h_files.sort()
        self.h_sel = None
        if len(self.h_files) > 0:
            if 0 <= self.h_idx < len(self.h_files):
                self.h_sel = policy.load_policy(main.parameters_path + "/" + self.h_files[self.h_idx])
            else:
                self.h_idx = 0
                self.h_sel = policy.load_policy(main.parameters_path + "/" + self.h_files[self.h_idx])

    def export_training_data(self, sim):
        f = open("logs/training_log/" + sim.export_name + ".txt", "w")
        f.write(f"Training time [s]: {get_time_as_str(sim.time_passed)}\n")
        f.write(f"Optimizer: {sim.handler.es.name}\n")
        f.write(f"Budget: {sim.handler.es.budget}\n")
        f.write(f"Number of solutions: {sim.handler.es.solutions_n}\n")
        f.write(f"Number of episodes: {sim.handler.episode}\n")
        f.write(f"Evaluation iterations: {sim.eval_iters}\n")
        f.write(f"Number of iterations: {sim.env.max_steps}\n")
        f.write(f"LOD: {sim.lod}\n")
        f.write(f"Noise level: {sim.env.noise_level}\n")
        f.write(f"Start position: {sim.env.start_position}\n")
        f.write(f"Terrain: {sim.terrain.name}\n")
        f.write(f"Hexapod: {sim.hexapod.params.name}\n")
        f.write(f"{sim.handler.average_scores}\n\n")
        idx = 0

        def print_policy(pol, idx):
            f.write(f"Name: {pol.name}\n")
            f.write(f"Number of parameters: {pol.get_num_params()}\n")
            f.write(f"Layers: {pol.layers}\n")
            f.write(f"Sigma: {sim.handler.es.params[idx][1]}\n")
            f.write("Parameters:\n")
            f.write(str(sim.handler.best_w) + "\n\n")

        if sim.gait_policy is not None:
            f.write(f"Gait Policy:\n")
            print_policy(sim.gait_policy, idx)
            idx += 1
        else:
            f.write(f"Gait Policy: None\n\n")

        if sim.torso_policy is not None:
            f.write(f"Torso Policy:\n")
            print_policy(sim.torso_policy, idx)
            idx += 1
        else:
            f.write(f"Torso Policy: None\n\n")

        if sim.leg_policy is not None:
            f.write(f"Leg Policy:\n")
            print_policy(sim.leg_policy, idx)
        else:
            f.write(f"Leg Policy: None\n\n")

        f.close()

    def policy_handler_info(self, policy_handler, node_name, policy_path):
        if imgui.tree_node(node_name):
            if imgui.tree_node("Scores"):
                if imgui.tree_node("Best"):
                    imgui.text(f"{-policy_handler.best_loss}")
                    imgui.tree_pop()
                if imgui.tree_node("Current"):
                    for val in policy_handler.losses:
                        imgui.text("{:.2f}".format(-val))
                    imgui.tree_pop()
                if imgui.tree_node("Graphs"):
                    imgui.plot_lines("Average score", np.array(policy_handler.average_scores, dtype=np.float32),
                                     graph_size=(imgui.get_content_region_available_width(), self.graph_height), values_offset=self.graph_value_offset,
                                     values_count=self.graph_values_count)
                    imgui.tree_pop()
                imgui.tree_pop()

            if imgui.tree_node("Weights"):
                if imgui.tree_node("Best weights"):
                    imgui.text(str(policy_handler.best_w))
                    imgui.text(f"Median: {sum(policy_handler.best_w) / len(policy_handler.best_w)}")
                    imgui.text(f"Min: {min(policy_handler.best_w)}")
                    imgui.text(f"Max: {max(policy_handler.best_w)}")
                    imgui.tree_pop()
                if imgui.tree_node("Graphs"):
                    graph_width = imgui.get_content_region_available_width()
                    imgui.plot_lines("Weights median", np.array(policy_handler.weights_median, dtype=np.float32), graph_size=(graph_width, self.graph_height))
                    imgui.plot_lines("Weights min", np.array(policy_handler.weights_min, dtype=np.float32), graph_size=(graph_width, self.graph_height))
                    imgui.plot_lines("Weights max", np.array(policy_handler.weights_max, dtype=np.float32), graph_size=(graph_width, self.graph_height))
                    imgui.tree_pop()
                imgui.tree_pop()

            if policy_path is not None:
                if imgui.button("Save best"):
                    temp = policy.Policy(policy_handler.policy.name, policy_handler.policy.layers, policy_handler.best_w)
                    policy.save_policy(policy_path, temp)

            imgui.tree_pop()


def get_time_as_str(seconds):
    seconds = int(seconds)
    h = seconds // 3600
    seconds %= 3600
    m = seconds // 60
    seconds %= 60
    s = seconds
    return "{:d}:{:d}:{:d}".format(h, m, s)


def export_plot(file_name, score):
    f = open("logs/training_log/plots/" + file_name + ".txt", "w")

    min_y = min(score)
    max_y = max(score)
    f.write(r'\begin{figure}' + '\n')
    f.write('\t' + r'\centering' + '\n')
    f.write('\t' + r'\begin{tikzpicture}' + '\n')
    f.write('\t\t' + r'\begin{axis}[' + '\n')
    f.write(f'\t\t\txmin = 0, xmax = {len(score) - 1},\n')
    f.write(f'\t\t\tymin = {min_y}, ymax = {max_y},\n')
    f.write(f'\t\t\txtick distance = ' + "{:.2f},".format(len(score) / 12.0) + '\n')
    f.write(f'\t\t\tytick distance = ' + "{:.2f},".format((max_y - min_y) / 8.0) + '\n')
    f.write(f'\t\t\tgrid = both,\n')
    f.write(f'\t\t\tminor tick num = {6},\n')
    f.write('\t\t\tmajor grid style = {lightgray},\n')
    f.write('\t\t\tminor grid style = {lightgray!25},\n')
    f.write('\t\t\t' + r'width = \textwidth,' + '\n')
    f.write('\t\t\t' + r'height = 0.5\textwidth,' + '\n')
    f.write('\t\t\txlabel = {Episode number},\n')
    f.write('\t\t\tylabel = {Reward},\n')
    f.write('\t\t\tlegend pos=south east\n')
    f.write('\t\t]\n')
    f.write('\t\t\t' + r'\addplot[' + '\n')
    f.write('\t\t\t\tthick,\n')
    f.write('\t\t\t\tblue\n')
    f.write('\t\t\t] file[skip first] {data/' + file_name + '_data.dat};\n\n')
    f.write('\t\t\t' + r'\legend{Plot from file}' + '\n')
    f.write('\t\t' + r'\end{axis}' + '\n')
    f.write('\t' + r'\end{tikzpicture}' + '\n')
    f.write('\t' + r'\caption{Caption}' + '\n')
    f.write('\t' + r'\label{fig:' + file_name + '}' + '\n')
    f.write(r'\end{figure}' + '\n')

    f.close()
    f = open("logs/training_log/plots/" + file_name + "_data.dat", "w")
    f.write("Epoch number\tScore\n")
    for i in range(len(score)):
        f.write("{:d}\t {:.5f}".format(i, score[i]))
        if i < len(score) - 1:
            f.write('\n')
    f.close()


def load_policies(path, idx):
    po_files = os.listdir(path)
    po_files.remove('weights')
    po_files.sort()
    po_sel = None
    if len(po_files) > 0:
        if 0 <= idx < len(po_files):
            po_sel = policy.load_policy(path + "/" + po_files[idx])
        else:
            idx = 0
            po_sel = policy.load_policy(path + "/" + po_files[idx])
    return po_files, idx, po_sel


def heightmap_info_gui(sim):
    if imgui.tree_node("Heightmap"):
        changed, new_value = imgui.checkbox("Render heightmap", sim.render_heightmap)
        if changed:
            sim.render_heightmap = new_value

        changed, new_values = imgui.drag_int("Size", sim.heightmap.size_n, 1, 0, 1000)
        if changed:
            sim.heightmap.set_size(new_values)

        changed, new_values = imgui.drag_float("Tile size", sim.heightmap.step, 0.001, 0.0, 100000, "%.3f", 1.0)
        if changed:
            sim.heightmap.step = new_values
        imgui.tree_pop()


def terrain_info_gui(sim):
    if imgui.tree_node("Terrain"):
        changed, new_value = imgui.checkbox("Render terrain", sim.render_terrain)
        if changed:
            sim.render_terrain = new_value

        imgui.text(f"Terrain: {sim.terrain.name}")

        imgui.tree_pop()


def reward_info(sim):
    if imgui.tree_node("Rewards"):
        if imgui.tree_node("Average"):
            imgui.text(f"time rew: {sim.env.total_time_rew / sim.env.step_ctr}")
            imgui.text(f"pos x rew: {sim.env.total_pos_x_rew / sim.env.step_ctr}")
            imgui.text(f"vel x rew: {sim.env.total_vel_x_rew / sim.env.step_ctr}")
            imgui.text(f"pos y pen: {sim.env.total_pos_y_rew / sim.env.step_ctr}")
            imgui.text(f"vel z pen: {sim.env.total_vel_z_rew / sim.env.step_ctr}")
            imgui.text(f"yaw pen: {sim.env.total_yaw_rew / sim.env.step_ctr}")
            imgui.text(f"pitch pen: {sim.env.total_pitch_rew / sim.env.step_ctr}")
            imgui.text(f"roll pen: {sim.env.total_roll_rew / sim.env.step_ctr}")
            imgui.text(f"Power consumption: {sim.env.total_work / sim.env.step_ctr}")
            imgui.text(f"Smoothness: {sim.env.total_acceleration / sim.env.step_ctr}")
            imgui.tree_pop()

        if imgui.tree_node("Setup"):
            changed, new_values = imgui.drag_float("time rew", sim.env.time_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.time_rew = new_values
            changed, new_values = imgui.drag_float("pos x rew", sim.env.pos_x_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.pos_x_rew = new_values
            changed, new_values = imgui.drag_float("vel x rew", sim.env.vel_x_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.vel_x_rew = new_values
            changed, new_values = imgui.drag_float("pos y pen", sim.env.pos_y_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.pos_y_rew = new_values
            changed, new_values = imgui.drag_float("vel z pen", sim.env.vel_z_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.vel_z_rew = new_values
            changed, new_values = imgui.drag_float("yaw pen", sim.env.yaw_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.yaw_rew = new_values
            changed, new_values = imgui.drag_float("pitch pen", sim.env.pitch_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.pitch_rew = new_values
            changed, new_values = imgui.drag_float("roll pen", sim.env.roll_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            if changed:
                sim.env.roll_rew = new_values
            # changed, new_values = imgui.drag_float("transition pen", trainer.transition_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            # if changed:
            #     trainer.transition_rew = new_values
            # changed, new_values = imgui.drag_float("gait pen", trainer.gait_rew, 0.01, 0.0, 100000, "%.3f", 1.0)
            # if changed:
            #     trainer.gait_rew = new_values
            imgui.tree_pop()
        imgui.tree_pop()


def hexapod_info_gui(sim):
    if imgui.tree_node("Hexapod"):
        changed, new_value = imgui.checkbox("Render hexapod", sim.render_hexapod)
        if changed:
            sim.render_hexapod = new_value

        pos, rot, _ = sim.env.get_basic_obs()
        imgui.text("Position: x: {:.2f}, y: {:.2f}, z: {:.2f}".format(*pos))
        imgui.text("Rotation [rad]: x: {:.2f}, y: {:.2f}, z: {:.2f}".format(*np.array(rot)))
        imgui.text("Rotation [deg]: x: {:.2f}, y: {:.2f}, z: {:.2f}".format(*np.rad2deg(np.array(rot))))

        changed, new_values = imgui.drag_float("Default body height", sim.controller.default_body_height, 0.001, 0, 10, "%.3f", 1.0)
        if changed:
            sim.controller.default_body_height = new_values

        changed, new_values = imgui.drag_float("Default leg height", sim.controller.default_leg_height, 0.001, 0, 10, "%.3f", 1.0)
        if changed:
            sim.controller.default_leg_height = new_values

        if imgui.tree_node("All phases"):
            for i in range(len(sim.hexapod.legs)):
                imgui.text(sim.hexapod.legs[i].params.id)
                imgui.same_line()
                if sim.hexapod.legs[i].swing == -1:
                    imgui.push_style_color(imgui.COLOR_TEXT, 1, 0, 0)
                    imgui.text("Stance")
                elif sim.hexapod.legs[i].swing == 1:
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 1, 0)
                    imgui.text("Swing")
                imgui.same_line()
                imgui.pop_style_color(1)
                if sim.sw[i] == -1:
                    imgui.push_style_color(imgui.COLOR_TEXT, 1, 0, 0)
                    imgui.text("Stance")
                elif sim.sw[i] == 1:
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 1, 0)
                    imgui.text("Swing")
                imgui.pop_style_color(1)
            imgui.tree_pop()

        for i in range(len(sim.hexapod.legs)):
            leg = sim.hexapod.legs[i]
            imgui.push_id(str(leg))
            if imgui.tree_node(f"Leg {leg.params.id}"):
                imgui.text("Joint angles [rad]: {:.2f}, {:.2f}, {:.2f}".format(*leg.angles))
                imgui.text("Joint angles [deg]: {:.2f}, {:.2f}, {:.2f}".format(*np.rad2deg(np.array(leg.angles))))
                imgui.text("Phase: ")
                imgui.same_line()
                if leg.swing == -1:
                    imgui.push_style_color(imgui.COLOR_TEXT, 1, 0, 0)
                    imgui.text("Stance")
                elif leg.swing == 1:
                    imgui.push_style_color(imgui.COLOR_TEXT, 0, 1, 0)
                    imgui.text("Swing")
                imgui.pop_style_color(1)

                imgui.text(f"Inner phase: {leg.phase}")
                imgui.text(f"Stuck {leg.stuck}")
                imgui.text(f"On range edge: {leg.on_range_edge}")
                imgui.text(f"Threshold: {leg.params.tolerance}")
                imgui.text(f"Position-Target distance: {np.linalg.norm(leg.target - leg.position)}")
                imgui.text(f"Position: {leg.position}")
                imgui.text(f"Target: {leg.target}")
                imgui.text(f"Center: {leg.center}")
                imgui.text(f"Mount: {leg.mount}")

                imgui.tree_pop()
            imgui.pop_id()
        imgui.tree_pop()


def control_buttons(sim, arr, text):
    imgui.push_style_color(imgui.COLOR_BUTTON, 0.6, 0.6, 0.0)
    if sim.pause:
        if imgui.button("Resume " + text):
            sim.pause = False
    else:

        if imgui.button("Pause " + text):
            sim.pause = True

    imgui.push_style_color(imgui.COLOR_BUTTON, 0.6, 0.0, 0.0)
    if imgui.button("Stop " + text):
        sim.close()
        arr.remove(sim)
    imgui.pop_style_color(2)


def export_evaluation_score(f, sim):
    dist = sim.get_distance()
    smooth = sim.get_smoothness()
    smooth_per_dist = 100 * smooth / dist
    power = sim.get_work()
    power_per_dist = 100 * power / dist

    f.write(f"Distance: {dist}\n")
    f.write(f"Smoothness: {smooth}\n")
    f.write(f"Smoothness per distance: {smooth_per_dist}\n")
    f.write(f"Power consumption: {power}\n")
    f.write(f"Power consumption per distance: {power_per_dist}\n")
    f.write("{:.2f} & {:.2f} & {:.2f} & {:.2f} & {:.2f}\n".format(dist, smooth_per_dist, power_per_dist, smooth, power))


def export_trajectory_plot(sim):
    file_name = sim.export_name
    traj = sim.env.trajectory
    traj_x_min = 100000000000
    traj_y_min = 100000000000
    traj_x_max = -100000000000
    traj_y_max = -100000000000

    for state in traj:
        traj_x_max = max(state[0][0], traj_x_max)
        traj_y_max = max(state[0][1], traj_y_max)
        traj_x_min = min(state[0][0], traj_x_min)
        traj_y_min = min(state[0][1], traj_y_min)

    print(traj_x_min)
    print(traj_y_min)
    print(traj_x_max)
    print(traj_y_max)

    f = open("logs/evaluation_log/plots/" + sim.export_name + ".txt", "w")
    f.write(r'\begin{figure}' + '\n')
    f.write('\t' + r'\centering' + '\n')
    f.write('\t' + r'\begin{tikzpicture}' + '\n')
    f.write('\t\t' + r'\begin{axis}[' + '\n')
    f.write(f'\t\t\txmin = {traj_x_min*1.2}, xmax = {traj_x_max*1.2},\n')
    f.write(f'\t\t\tymin = {traj_y_min*1.2}, ymax = {traj_y_max*1.2},\n')
    f.write(f'\t\t\txtick distance = ' + "{:.2f},".format((traj_x_max - traj_x_min) / 7.0) + '\n')
    f.write(f'\t\t\tytick distance = ' + "{:.2f},".format((traj_y_max - traj_y_min) / 8.0) + '\n')
    f.write(f'\t\t\tgrid = both,\n')
    f.write(f'\t\t\tminor tick num = {6},\n')
    f.write('\t\t\tmajor grid style = {lightgray},\n')
    f.write('\t\t\tminor grid style = {lightgray!25},\n')
    f.write('\t\t\t' + r'width = \textwidth,' + '\n')
    f.write('\t\t\t' + r'height = 0.5\textwidth,' + '\n')
    f.write('\t\t\txlabel = {Episode number},\n')
    f.write('\t\t\tylabel = {Reward},\n')
    f.write('\t\t\tlegend pos=south east\n')
    f.write('\t\t]\n')
    f.write('\t\t\t' + r'\addplot[' + '\n')
    f.write('\t\t\t\tthick,\n')
    f.write('\t\t\t\tblue\n')
    f.write('\t\t\t] file[skip first] {data/' + file_name + '_data.dat};\n\n')
    f.write('\t\t\t' + r'\legend{Plot from file}' + '\n')
    f.write('\t\t' + r'\end{axis}' + '\n')
    f.write('\t' + r'\end{tikzpicture}' + '\n')
    f.write('\t' + r'\caption{Caption}' + '\n')
    f.write('\t' + r'\label{fig:' + file_name + '}' + '\n')
    f.write(r'\end{figure}' + '\n')

    f.close()

    f = open("logs/evaluation_log/plots/" + file_name + "_data.dat", "w")
    f.write("x\ty\n")
    i = 0
    for state in traj:
        f.write("{:.5f}\t {:.5f}".format(state[0][0], state[0][1]))
        if i < len(traj) - 1:
            f.write('\n')
        i += 1
    f.close()


def export_whole_training(file_name, arr):
    arr = np.asarray(arr, dtype=object)
    np.save("logs/training_log/whole_training/" + file_name, arr)
