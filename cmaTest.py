import cma
import numpy as np
from scipy.optimize import rosen, differential_evolution


if __name__ == "__main__":
    desired_mat = np.array([[1,2,3,4],
                            [2,3,20,1],
                            [3,4,1,2],
                            [4,1,2,3]]).flatten()
    mat = np.zeros(len(desired_mat))

    es = cma.CMAEvolutionStrategy(mat.flatten(), 0.1)
    print(es.opts)

    done = False
    itr = 0
    while not done:
        itr += 1
        solutions = es.ask()
        scores = [np.sum(np.square(desired_mat-solution)) for solution in solutions]
        for i in range(len(scores)):
            if scores[i] <= 1e-20:
                print(scores[i])
                print("Solution:", solutions[i])
                print("Iterations:", itr)
                done = True
                break
        es.tell(solutions, scores)
