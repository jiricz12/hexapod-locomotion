import copy
import my_math
import numpy as np
import time
from heightmap import Heightmap
import robot
from src.environment.environments import StructuredHexapodEnv
from src.environment.environments import UnstructuredHexapodEnv
import optimizer
from policy import Policy

SWING = 1.0
STANCE = -1.0


class Simulation:

    def __init__(self, terrain, hexapod_params):
        self.export_name = None
        self.env = None
        self.hexapod_params = hexapod_params
        self.pause = False
        self.render = False
        self.terrain = terrain
        self.render_terrain = True
        self.last_time = time.time()
        self.time_passed = 0
        self.obs = None
        self.act = None

    def step(self):
        if not self.pause:
            self.time_passed += time.time() - self.last_time
        self.last_time = time.time()

    def reset(self):
        self.obs = self.env.reset()

    def close(self):
        self.env.close()


class StructuredSimulation(Simulation):

    def __init__(self, terrain, start_position, params, max_steps, animate, noise_level, gait_policy, body_policy, leg_policy, lod):
        super(StructuredSimulation, self).__init__(terrain, params)
        self.use_gait_pol = True
        self.use_torso_pol = True
        self.use_leg_pol = True
        self.env = StructuredHexapodEnv(terrain, params, animate, max_steps, noise_level, start_position)
        self.controller = robot.Controller()
        self.body_move_dir = robot.ControlInput.FORWARD
        if terrain.name == "Flat":
            self.heightmap = Heightmap(int(max(terrain.n_x, terrain.n_y)), terrain.step)
            self.heightmap.reset_all_to(np.array(start_position), 0.0)
        else:
            self.heightmap = Heightmap(int(max(terrain.n_x, terrain.n_y)), terrain.step)
            self.heightmap.reset(terrain.origin, terrain.get_points())
        self.render_heightmap = False
        self.hexapod = robot.Hexapod(params)
        self.render_hexapod = True
        self.sw = [-1.0] * len(self.hexapod.legs)
        self.lod = lod
        self.env_state = None
        self.alg_state = None
        self.ctrl_act = None
        self.surface_normal = np.array([0., 0., 1.])
        self.transition_rew = 1.0
        self.torso_height = 0
        self.leg_height = [0] * len(self.hexapod.legs)
        self.eval_iters = 1
        self.body_height_variation = 0
        self.body_height_for_sim = 0

        self.transition_rew = -1.0
        self.stance_in_rew = 1.0
        self.swing_in_rew = 5.0
        self.tripod_rew = -1.0
        self.stance_out_dist_rew = -20.0

        self.avr_transition_rew = 0
        self.avr_stance_in_rew = 0
        self.avr_swing_in_rew = 0
        self.avr_tripod_rew = 0
        self.avr_stance_out_dist_rew = 0

        self.gait_policy = gait_policy
        self.torso_policy = body_policy
        self.leg_policy = leg_policy

    def reset(self):
        super().reset()
        self.env_state = robot.State(self.obs[0:3], self.obs[3:6], self.obs[12:30])
        self.alg_state = copy.deepcopy(self.env_state)
        self.hexapod.reset(np.array(self.obs[0:3]), np.array(self.obs[3:6]), self.obs[12:30])
        self.sw = [-1.0] * len(self.hexapod.legs)
        self.body_height_for_sim = (np.random.rand() * 2.0 - 1.5) * self.body_height_variation
        self.avr_transition_rew = 0
        self.avr_stance_in_rew = 0
        self.avr_swing_in_rew = 0
        self.avr_tripod_rew = 0
        self.avr_stance_out_dist_rew = 0

    def body(self, body_move_dir, supervised, window=None):

        self.hexapod.set(np.array(self.obs[0:3]), np.array(self.obs[3:6]), self.obs[12:30])
        self.ctrl_act = self.controller.get_action(self.hexapod, body_move_dir, self.obs[3:6], self.obs[6:12], True, window)
        self.ctrl_act.torso_height += self.body_height_for_sim

        if self.terrain is None:
            self.heightmap.reset_all_to(np.array(self.obs[0:3]), 0)

        stuck = [-1.0] * len(self.hexapod.legs)
        contacts = [-1.0] * len(self.hexapod.legs)
        gait_phases = [-1.0] * len(self.hexapod.legs)

        for i in range(len(self.hexapod.legs)):
            if self.hexapod.legs[i].is_stuck(self.ctrl_act):
                stuck[i] = 1.0
            gait_phases[i] = self.hexapod.legs[i].swing
            contacts[i] = self.obs[6 + i]

        if self.use_gait_pol and self.gait_policy is not None:
            # overwrite controller swing phases by the policy

            idx_group_1 = [0, 4, 2]
            idx_group_2 = [3, 5, 1]

            for i in range(len(self.ctrl_act.leg_actions)):

                indices = idx_group_1 + idx_group_2 if i % 2 == 0 else idx_group_2 + idx_group_1
                individual_swing = [0] * len(self.hexapod.legs)
                individual_stuck = [0] * len(self.hexapod.legs)
                individual_contacts = [0] * len(self.hexapod.legs)

                j = 0
                for idx in indices:
                    individual_swing[j] = gait_phases[idx]
                    individual_stuck[j] = stuck[idx]
                    individual_contacts[j] = contacts[idx]
                    j += 1

                my_math.shift_left(idx_group_1, 1)
                my_math.shift_right(idx_group_2, 1)

                target_distance = np.linalg.norm(self.hexapod.legs[i].target - self.hexapod.legs[i].position)
                # obs: swing phases, stuck, contact, target_dist, joint angles
                # obs: 6, 6, 6, 1, 3, 3 => 25
                joint_dir = 1.0 if i < 3 else -1.0
                obs = np.array(np.concatenate((individual_swing,
                                               individual_stuck,
                                               individual_contacts,
                                               [target_distance],
                                               self.surface_normal,
                                               [joint_dir * self.obs[12 + i * 3]],
                                               self.obs[13 + i * 3:13 + i * 3 + 2])))

                action = self.gait_policy.forward(obs)

                if len(action) > 1:
                    self.sw[i] = SWING if action[0] > action[1] else STANCE
                else:
                    self.sw[i] = SWING if action[0] > 0.0 else STANCE

                if supervised:

                    if self.sw[i] == self.ctrl_act.leg_actions[i].swing:
                        gait_handler = self.gait_handlers[i % len(self.gait_handlers)]
                        gait_handler.losses[gait_handler.solution_idx] -= 1.0 / self.eval_iters
                else:

                    grp1 = [0, 2, 4]
                    grp2 = [1, 3, 5]
                    grp1_sw = (np.array([self.ctrl_act.leg_actions[0].swing, self.ctrl_act.leg_actions[2].swing, self.ctrl_act.leg_actions[4].swing]) > 0).max()
                    grp2_sw = (np.array([self.ctrl_act.leg_actions[1].swing, self.ctrl_act.leg_actions[3].swing, self.ctrl_act.leg_actions[5].swing]) > 0).max()

                    if self.sw[i] == SWING and stuck[i] == 1.0:
                        rew = self.swing_in_rew / self.eval_iters
                        self.avr_swing_in_rew += rew / self.env.max_steps
                        #gait_handler.losses[gait_handler.solution_idx] -= rew

                    if self.sw[i] == STANCE and contacts[i] < 0.0 and self.hexapod.legs[i].phase != 2:
                        rew = self.stance_out_dist_rew * target_distance / self.eval_iters
                        self.avr_stance_out_dist_rew += rew / self.env.max_steps
                        #gait_handler.losses[gait_handler.solution_idx] -= rew

                    if self.sw[i] == STANCE and contacts[i] > 0 and stuck[i] < 0.0:
                        rew = self.stance_in_rew / self.eval_iters
                        self.avr_stance_in_rew += rew / self.env.max_steps
                        #gait_handler.losses[gait_handler.solution_idx] -= rew

                    if self.sw[i] == SWING:
                        if (i in grp1 and not grp2_sw) or (i in grp2 and not grp1_sw):
                            self.ctrl_act.leg_actions[i].swing = SWING
                        else:
                            rew = self.tripod_rew / self.eval_iters
                            self.avr_tripod_rew += rew / self.env.max_steps
                            #gait_handler.losses[gait_handler.solution_idx] -= rew
                    else:
                        self.ctrl_act.leg_actions[i].swing = STANCE

        if self.use_torso_pol and self.torso_policy is not None:
            terrain_body_height = self.heightmap.get_height(self.env_state.position)
            # 6, 6, 6, 3, 3, 1 => 25
            obs = np.concatenate((gait_phases,
                                  stuck,
                                  contacts,
                                  self.surface_normal,
                                  self.obs[3:6],
                                  [terrain_body_height]))
            self.torso_height = self.torso_policy.forward(obs)
            self.ctrl_act.torso_height += self.torso_height

        if self.use_leg_pol and self.leg_policy is not None:
            for i in range(len(self.ctrl_act.leg_actions)):
                current_leg_height = self.heightmap.get_height(self.hexapod.legs[i].position)
                # 3, 3, 1, 1 => 8
                joint_dir = 1.0 if i < 3 else -1.0
                obs = np.concatenate((self.surface_normal,
                                      [joint_dir * self.obs[12 + i * 3]],
                                      self.obs[13 + i * 3:13 + i * 3 + 2],
                                      [contacts[i]],
                                      [current_leg_height]))
                self.leg_height[i] = self.leg_policy.forward(obs)
                self.ctrl_act.leg_actions[i].lift_height += self.leg_height[i]

        self.alg_state, self.surface_normal = self.hexapod.step(self.env_state, self.ctrl_act, self.heightmap, self.lod)

        self.act = self.alg_state.joint_angles

        # Step environment
        self.obs, r, done, _ = self.env.step(self.act)

        # if not mimic and self.use_gait_pol:
        #     for i in range(len(self.ctrl_act.leg_actions)):
        #         gait_handler = self.gait_handlers[i % len(self.gait_handlers)]
        #         gait_handler.losses[gait_handler.solution_idx] -= r

        # if self.use_body_pol:
        #     self.body_height_handler.losses[self.body_height_handler.solution_idx] -= r

        # if self.use_leg_pol:
        #     self.leg_height_handler.losses[self.leg_height_handler.solution_idx] -= r

        self.env_state = robot.State(np.array(self.obs[0:3]), np.array(self.obs[3:6]), self.obs[12:30])

        return done, r


class StructuredTrainer(StructuredSimulation):

    def __init__(self, terrain, start_position, params, max_steps, animate, noise_level, gait_policy, body_policy, leg_policy, eval_iters, supervised, handler, lod):
        super(StructuredTrainer, self).__init__(terrain, start_position, params, max_steps, animate, noise_level, gait_policy, body_policy, leg_policy, lod)
        self.eval_iters = eval_iters
        self.eval_iter = 0
        self.supervised = supervised
        self.handler = handler
        self.first_eval()
        self.handler.begin_episode()
        self.begin = True

    def first_eval(self):
        for ev in range(self.eval_iters):
            self.reset()
            done = False
            while not done:
                done, _ = self.body(self.body_move_dir, self.supervised)
            rew = (self.env.robot_pos[0] - self.env.start_position[0]) * 15
            self.handler.give_reward(rew)
            self.handler.best_loss = -rew

    def update_policy_params(self):
        idx = 0
        if self.gait_policy is not None:
            self.gait_policy.set_params(self.handler.get_current_solution()[idx])
            idx += 1
        if self.torso_policy is not None:
            self.torso_policy.set_params(self.handler.get_current_solution()[idx])
            idx += 1
        if self.leg_policy is not None:
            self.leg_policy.set_params(self.handler.get_current_solution()[idx])

    def step(self, window=None):
        super().step()
        if not self.pause:

            if self.begin:
                self.begin = False
                self.reset()
                self.eval_iter = 0

            done, _ = self.body(self.body_move_dir, self.supervised)

            if done:
                self.eval_iter += 1
                if self.eval_iter >= self.eval_iters:
                    self.begin = True
                    self.eval_iter = 0

                    self.handler.give_reward((self.env.robot_pos[0] - self.env.start_position[0]) * 15)
                    self.handler.env_done()
                    self.update_policy_params()

                self.reset()


class Handler:

    def __init__(self, optim):
        w = []
        for param in optim.params:
            for p in param[0]:
                w.append(p)
        self.es = optim
        self.solutions_n = 1
        self.episode = 0
        self.solutions = [w]
        self.solution_idx = 0
        self.losses = [0]
        self.best_loss = np.inf
        self.best_w = w
        self.prediction = 0
        self.average_scores = []
        self.weights_median = []
        self.weights_min = []
        self.weights_max = []
        self.weights_median.append(sum(w) / len(w))
        self.weights_min.append(min(self.best_w))
        self.weights_max.append(max(self.best_w))
        self.training_log = []

    def begin_episode(self):
        self.episode += 1
        self.solution_idx = 0
        self.average_scores.append(-sum(self.losses) / len(self.losses))
        if self.es is not None:
            self.solutions = self.es.ask()
            self.solutions_n = len(self.solutions)
        self.losses = [0] * self.solutions_n

    def env_done(self):

        if self.losses[self.solution_idx] < self.best_loss:
            self.best_loss = self.losses[self.solution_idx]
            self.best_w = [item for sublist in self.solutions[self.solution_idx].args for item in sublist]

        self.solution_idx += 1
        if self.solution_idx >= self.solutions_n:
            self.end_episode()
            self.begin_episode()

    def end_episode(self):
        if self.es is not None:
            self.es.tell(self.solutions, self.losses)
        self.weights_median.append(sum(self.best_w) / len(self.best_w))
        self.weights_min.append(min(self.best_w))
        self.weights_max.append(max(self.best_w))

        for i in range(len(self.solutions)):
            self.training_log.append([-self.losses[i], self.solutions[i].args[0]])

    def should_train(self):
        return self.es is not None

    def get_current_solution(self):
        return self.solutions[self.solution_idx].args

    def give_reward(self, reward):
        self.losses[self.solution_idx] -= reward


class StructuredTester(StructuredSimulation):

    def __init__(self, terrain, start_position, params, max_steps, animate, noise_level, gait_handlers, bilateral_sharing, use_gait_pol, body_handlers, use_body_pol, leg_handlers, use_leg_pol, auto_reset):
        super(StructuredTester, self).__init__(terrain, start_position, params, max_steps, animate, noise_level, gait_handlers, bilateral_sharing, use_gait_pol, body_handlers, use_body_pol, leg_handlers, use_leg_pol)
        self.auto_reset = auto_reset
        self.reset()
        self.reward = 0

    def step(self, window=None):
        super().step()
        if not self.pause:

            done, r = self.body(self.body_move_dir, False, window)
            self.reward += r

            if self.auto_reset:
                if done:
                    self.reset()

    def reset(self):
        super().reset()
        self.reward = 0

    def get_gait_score(self):
        return self.avr_transition_rew + self.avr_swing_in_rew + self.avr_stance_out_dist_rew + self.avr_stance_in_rew + self.avr_tripod_rew


class StructuredEvaluator(StructuredSimulation):

    def __init__(self, terrain, start_position, params, max_steps, animate, noise_level, gait_policy, torso_policy, leg_policy, eval_iters, lod):
        super(StructuredEvaluator, self).__init__(terrain, start_position, params, max_steps, animate, noise_level, gait_policy, torso_policy, leg_policy, lod)
        self.eval_iters = eval_iters
        self.eval_iter = 0
        self.distance_traveled = 0
        self.smoothness = 0
        self.work = 0
        self.reset()
        self.trajectories = []

    def step(self, window=None):
        super().step()
        if not self.pause:

            done, r = self.body(self.body_move_dir, False, None)

            if done:
                self.eval_iter += 1
                self.distance_traveled += (self.env.robot_pos[0] - self.env.start_position[0])
                self.work += self.env.total_work
                self.smoothness += calculate_smoothness(self.env.trajectory)
                self.trajectories.append(self.env.trajectory)
                if self.eval_iter < self.eval_iters:
                    self.reset()
                else:
                    self.pause = True

    def get_distance(self):
        return float(5000 * self.distance_traveled / (self.eval_iters * self.env.max_steps))

    def get_smoothness(self):
        return float(self.smoothness / (self.eval_iters * self.env.max_steps))
        #return float(50 * (2-self.smoothness / (self.eval_iters * self.env.max_steps)))

    def get_work(self):
        return float(0.5 * self.work / (self.eval_iters * self.env.max_steps))


class UnstructuredSimulation(Simulation):

    def __init__(self, terrain, start_pos, params, policy, max_steps, animate, noise_level):
        super(UnstructuredSimulation, self).__init__(terrain, params)
        self.env = UnstructuredHexapodEnv(terrain, params, animate, max_steps, noise_level, start_pos)
        self.policy = policy
        self.begin = True

    def body(self):
        self.act = self.policy.forward(self.obs)
        # Step environment
        self.obs, r, done, _ = self.env.step(self.act)

        return done, r

    def step(self):
        super().step()

    def reset(self):
        super().reset()


class UnstructuredTrainer(UnstructuredSimulation):

    def __init__(self, terrain, start_pos, params, handler, policy, animate, max_steps, eval_iters, noise_level):
        super(UnstructuredTrainer, self).__init__(terrain, start_pos, params, policy, max_steps, animate, noise_level)
        self.eval_iters = eval_iters
        self.eval_iter = 0
        self.handler = handler
        self.first_eval()
        self.handler.begin_episode()
        self.update_policy_params()

    def first_eval(self):
        for ev in range(self.eval_iters):
            self.reset()
            done = False
            while not done:
                done, r = self.body()
                #self.handler.give_reward(r)

            self.handler.give_reward(self.calc_reward())
            self.handler.best_loss = self.handler.losses[0]
        self.reset()

    def update_policy_params(self):
        self.policy.set_params(self.handler.get_current_solution()[0])

    def step(self):
        super().step()
        if not self.pause:
            done, r = self.body()

            if done:
                self.eval_iter += 1
                self.handler.give_reward(self.calc_reward())
                if self.eval_iter >= self.eval_iters:
                    self.eval_iter = 0
                    self.handler.env_done()
                    self.update_policy_params()
                self.reset()

    def calc_reward(self):
        sm = calculate_smoothness(self.env.trajectory) / 50000
        distx = (self.env.robot_pos[0] - self.env.start_position[0]) * 10
        disty = abs(self.env.robot_pos[1] - self.env.start_position[1]) * 10
        power = self.env.get_work() / 10
        j_vel = self.env.joint_velocities / 10000
        return sm + distx - disty - power


class UnstructuredTester(UnstructuredSimulation):

    def __init__(self, terrain, start_position, params, policy_handler, max_steps, animate, noise_level, auto_reset):
        super(UnstructuredTester, self).__init__(terrain, start_position, params, policy_handler, max_steps, animate, noise_level)
        self.obs = self.env.reset()
        self.auto_reset = auto_reset
        self.reward = 0

    def step(self):
        super().step()
        if not self.pause:

            done, r = self.body()

            self.reward += r
            if self.auto_reset:
                if done:
                    self.reset()

    def reset(self):
        super().reset()
        self.reward = 0


class UnstructuredEvaluator(UnstructuredSimulation):

    def __init__(self, terrain, start_position, params, policy, max_steps, animate, noise_level, eval_iters):
        super(UnstructuredEvaluator, self).__init__(terrain, start_position, params, policy, max_steps, animate, noise_level)
        self.eval_iters = eval_iters
        self.eval_iter = 0
        self.distance_traveled = 0
        self.smoothness = 0
        self.work = 0
        self.trajectories = []
        self.reset()

    def step(self, window=None):
        super().step()
        if not self.pause:

            done, r = self.body()

            if done:
                self.eval_iter += 1
                self.distance_traveled += (self.env.robot_pos[0] - self.env.start_position[0])
                self.work += self.env.total_work
                self.trajectories.append(self.env.trajectory)
                self.smoothness += calculate_smoothness(self.env.trajectory)
                if self.eval_iter < self.eval_iters:
                    self.reset()
                else:
                    self.pause = True

    def get_distance(self):
        return float(5000 * self.distance_traveled / (self.eval_iters * self.env.max_steps))

    def get_smoothness(self):
        return float(self.smoothness / (self.eval_iters * self.env.max_steps))
        #return float(50 * (2-self.smoothness / (self.eval_iters * self.env.max_steps)))

    def get_work(self):
        return float(0.5 * self.work / (self.eval_iters * self.env.max_steps))


def calculate_smoothness(trajectory):
    acc = [0] * (len(trajectory) - 1)
    for i in range(len(acc)):
        delta_t = (trajectory[i][-1]) * 100
        if delta_t == 0:
            continue
        translation_acc = np.sum(np.square((trajectory[i+1][2] - trajectory[i][2]) / delta_t))
        angular_acc = np.sum(np.square((trajectory[i+1][3] - trajectory[i][3]) / delta_t))
        acc[i] = translation_acc + angular_acc

    return -sum(acc)
