import pickle

from OpenGL.GL import *
import numpy as np
import opensimplex
import my_math


class Vao:

    def __init__(self):
        self.id = glGenVertexArrays(1)
        self.vbos = []

    def bind(self):
        glBindVertexArray(self.id)

    def unbind(self):
        glBindVertexArray(0)

    def store_float_data_in_attribute_list(self, data, mode, attrib_list, instance_size):
        self.bind()
        vbo = glGenBuffers(1)
        self.vbos.append(vbo)
        glBindBuffer(GL_ARRAY_BUFFER, vbo)
        glBufferData(GL_ARRAY_BUFFER, data.nbytes, data, mode)
        glVertexAttribPointer(attrib_list, instance_size, GL_FLOAT, GL_FALSE, 0, None)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        self.unbind()

    def change_float_subdata_in_attribute_list(self, data, attrib_list, instance_size):
        self.bind()
        glBindBuffer(GL_ARRAY_BUFFER, self.vbos[attrib_list])
        glBufferSubData(GL_ARRAY_BUFFER, 0, data.nbytes, data)
        glVertexAttribPointer(attrib_list, instance_size, GL_FLOAT, GL_FALSE, 0, None)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        self.unbind()

    def change_float_data_in_attribute_list(self, data, mode, attrib_list, instance_size):
        self.bind()
        glBindBuffer(GL_ARRAY_BUFFER, self.vbos[attrib_list])
        glBufferData(GL_ARRAY_BUFFER, data.nbytes, data, mode)
        glVertexAttribPointer(attrib_list, instance_size, GL_FLOAT, GL_FALSE, 0, None)
        glBindBuffer(GL_ARRAY_BUFFER, 0)
        self.unbind()


# def clean_up():
#     for vao in vaos:
#         for vbo in vao.vbos:
#             glDeleteBuffers(1, vbo)
#         glDeleteVertexArrays(1, vao.id)



class RawModel:
    def __init__(self, vao, vertex_count):
        self.vao = vao
        self.vertex_count = int(vertex_count)

    def get_vao(self):
        return self.vao

    def bind(self):
        self.get_vao().bind()

    def unbind(self):
        self.get_vao().unbind()


def create_circle(r, resolution):
    vertices = []
    angle_step = 2*np.pi / resolution
    angle = 0

    while angle < 2*np.pi:
        vertices += [r * np.cos(angle),
                     r * np.sin(angle),
                     0]
        angle += angle_step

    vertices = np.array(vertices, dtype=np.float32)

    return load_model(vertices, None, GL_STATIC_DRAW)


def create_trajectory(trajectories):
    vertices = [0] * (2 * len(trajectories[0]) - 2) * 3 * len(trajectories)
    k = 0
    for traj in trajectories:
        n = 0
        for i in range(len(traj)):
            for j in range(3):
                vertices[k+n] = traj[i][0][j]
                n += 1
            if 0 < i < len(traj)-1:
                for j in range(3):
                    vertices[k+n] = traj[i][0][j]
                    n += 1
        k += (2 * len(trajectories[0]) - 2) * 3
    vertices = np.array(vertices, dtype=np.float32)
    return load_model(vertices, None, GL_STATIC_DRAW)


def create_quad_2d(sx, sy):
    sx /= 2
    sy /= 2
    vertices = np.array([
        -sx, sy,
         sx, sy,
        -sx,-sy,

         sx, sy,
         sx,-sy,
        -sx,-sy
        ], dtype=np.float32)

    return load_model_2D(vertices, GL_STATIC_DRAW)


def create_triangle_2d():
    vertices = np.array([-0.5, -0.5,
                          0.5, -0.5,
                          0.0, 0.5], dtype=np.float32)

    return load_model_2D(vertices, GL_STATIC_DRAW)


def create_cube(sx, sy, sz):
    sx /= 2.0
    sy /= 2.0
    sz /= 2.0
    vertices = np.array([
        sx, sy, -sz,
        sx, -sy, -sz,
        -sx, -sy, -sz,
        -sx, -sy, -sz,
        -sx, sy, -sz,
        sx, sy, -sz,

        -sx, -sy, sz,
        sx, -sy, sz,
        sx, sy, sz,
        sx, sy, sz,
        -sx, sy, sz,
        -sx, -sy, sz,

        -sx, sy, sz,
        -sx, sy, -sz,
        -sx, -sy, -sz,
        -sx, -sy, -sz,
        -sx, -sy, sz,
        -sx, sy, sz,

        sx, -sy, -sz,
        sx, sy, -sz,
        sx, sy, sz,
        sx, sy, sz,
        sx, -sy, sz,
        sx, -sy, -sz,

        -sx, -sy, -sz,
        sx, -sy, -sz,
        sx, -sy, sz,
        sx, -sy, sz,
        -sx, -sy, sz,
        -sx, -sy, -sz,

        sx, sy, sz,
        sx, sy, -sz,
        -sx, sy, -sz,
        -sx, sy, -sz,
        -sx, sy, sz,
        sx, sy, sz,
    ], dtype=np.float32)

    normals = np.array([
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,

        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,

        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,

        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,

        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,

        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.
    ], dtype=np.float32)
    return load_model(vertices, normals, GL_STATIC_DRAW)


def create_offsetted_cube(sx_l, sx_u, sy_l, sy_u, sz_l, sz_u):
    vertices = np.array([
        sx_u, sy_u, sz_l,
        sx_u, sy_l, sz_l,
        sx_l, sy_l, sz_l,
        sx_l, sy_l, sz_l,
        sx_l, sy_u, sz_l,
        sx_u, sy_u, sz_l,

        sx_l, sy_l, sz_u,
        sx_u, sy_l, sz_u,
        sx_u, sy_u, sz_u,
        sx_u, sy_u, sz_u,
        sx_l, sy_u, sz_u,
        sx_l, sy_l, sz_u,

        sx_l, sy_u, sz_u,
        sx_l, sy_u, sz_l,
        sx_l, sy_l, sz_l,
        sx_l, sy_l, sz_l,
        sx_l, sy_l, sz_u,
        sx_l, sy_u, sz_u,

        sx_u, sy_l, sz_l,
        sx_u, sy_u, sz_l,
        sx_u, sy_u, sz_u,
        sx_u, sy_u, sz_u,
        sx_u, sy_l, sz_u,
        sx_u, sy_l, sz_l,

        sx_l, sy_l, sz_l,
        sx_u, sy_l, sz_l,
        sx_u, sy_l, sz_u,
        sx_u, sy_l, sz_u,
        sx_l, sy_l, sz_u,
        sx_l, sy_l, sz_l,

        sx_u, sy_u, sz_u,
        sx_u, sy_u, sz_l,
        sx_l, sy_u, sz_l,
        sx_l, sy_u, sz_l,
        sx_l, sy_u, sz_u,
        sx_u, sy_u, sz_u,
    ], dtype=np.float32)

    normals = np.array([
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,
        0., 0., -1.,

        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,
        0., 0., 1.,

        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,
        -1., 0., 0.,

        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,
        1., 0., 0.,

        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,
        0., -1., 0.,

        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.,
        0., 1., 0.
    ], dtype=np.float32)
    return load_model(vertices, normals, GL_STATIC_DRAW)


class Terrain(RawModel):
    def __init__(self, name, position, n_x, n_y, slope_x, slope_y, step, scale):
        self.name = name
        self.size_x = n_x * step
        self.size_y = n_y * step
        self.position = position
        self.step = step
        self.n_x = n_x
        self.n_y = n_y
        self.slope_x = slope_x
        self. slope_y = slope_y
        self.scale = scale
        self.origin = np.array([0., 0., 0.])
        self.draw_mode = GL_STATIC_DRAW
        self.points_updated = True
        self.vao_not_updated = True
        self.vao = None
        self.points = None

    def regenerate(self):
        if self.points_updated:
            self.get_points()
        if self.vao_not_updated:
            self.get_vao().change_float_data_in_attribute_list(self.points.flatten(), self.draw_mode, 0, 3)
            self.vertex_count = len(self.points)

    def get_vao(self):
        if self.vao is None:
            self.vao = Vao()
            self.vao.store_float_data_in_attribute_list(self.get_points().flatten(), self.draw_mode, 0, 3)
            self.vertex_count = len(self.points)
        return self.vao

    def get_size_n(self):
        return [self.n_x, self.n_y]

    def set_position(self, position):
        self.position = position
        self.points_updated = True

    def set_size(self, n_x, n_y):
        if n_x != self.n_x or n_y != self.n_y:
            self.points_updated = True
            self.n_x = n_x
            self.n_y = n_y
            self.size_x = self.n_x * self.step
            self.size_y = self.n_y * self.step

    def set_slope(self, slope_x, slope_y):
        if slope_x != self.slope_x or slope_y != self.slope_y:
            self.points_updated = True
            self.slope_x = slope_x
            self.slope_y = slope_y

    def set_step(self, step):
        if step != self.step:
            self.points_updated = True
            self.step = step
            self.size_x = self.n_x * step
            self.size_y = self.n_y * step

    def set_scale(self, scale):
        if scale[0] != self.scale[0] or scale[0] != self.scale[0] or scale[1] != self.scale[2]:
            self.points_updated = True
            self.scale = scale


class SimplexTerrain(Terrain):

    def __init__(self, name, position, n_x, n_y, slope_x, slope_y, step, scale):
        super(SimplexTerrain, self).__init__(name, position, n_x, n_y, slope_x, slope_y, step, scale)
        self.seed = 0

    def get_points(self):
        if self.points_updated:
            coords_x = (np.array([i for i in range(0, self.n_x)],
                                dtype=np.float32) - float(self.n_x-1) / 2.0) * self.step / self.scale[0]
            coords_y = (np.array([i for i in range(0, self.n_y)],
                                dtype=np.float32) - float(self.n_y-1) / 2.0) * self.step / self.scale[1]
            noise = opensimplex.noise2array(np.array(coords_x + self.seed / 3), np.array(coords_y + self.seed / 4)) * self.scale[2]
            self.points = np.array([np.array([0, 0, 0], np.float32) for _ in range(self.n_x * self.n_y)])

            idx = 0
            self.origin = np.array([0., 0., 0.])
            for y in range(0, self.n_y):
                for x in range(0, self.n_x):
                    self.points[idx][0] = coords_x[x] * self.scale[0] + self.position[0]
                    self.points[idx][1] = coords_y[y] * self.scale[1] + self.position[1]
                    self.points[idx][2] = noise[y][x] + self.position[2] + (x - self.n_x / 2) * self.slope_x + (y - self.n_y / 2) * self.slope_y
                    self.origin += self.points[idx]
                    idx += 1
            self.origin /= len(self.points)
            self.points_updated = False
            self.vao_not_updated = True

        return self.points

    def set_seed(self, seed):
        if self.seed != seed:
            self.points_updated = True
            self.seed = seed


class StepTerrain(Terrain):

    def __init__(self, name, position, n_x, n_y, slope_x, slope_y, step, scale, step_height, step_dist, step_len, arc):
        super(StepTerrain, self).__init__(name, position, n_x, n_y, slope_x, slope_y, step, scale)
        self.step_height = step_height
        self.step_dist = step_dist
        self.step_len = step_len
        self.arc = arc

    def get_points(self):
        if self.points_updated:

            coords_x = [0] * self.n_x
            step_count = 0
            step = True
            for i in range(self.n_x):
                coords_x[i] = (i - step_count) * self.step - self.size_x / 2.0
                if (i - step_count) >= self.step_dist[0]:
                    if (i - step_count) % self.step_len[0] == 0:
                        if step:
                            step_count += 1
                            step = False
                    else:
                        step = True

            coords_y = [0] * self.n_y
            for i in range(self.n_y):
                coords_y[i] = i * self.step - self.size_y / 2.0

            self.points = np.array([np.array([0, 0, 0], np.float32) for _ in range(self.n_x * self.n_y)])

            idx = 0
            self.origin = np.array([0., 0., 0.])
            for y in range(0, self.n_y):
                height = 0
                for x in range(0, self.n_x):
                    self.points[idx][0] = coords_x[x] + self.position[0]
                    self.points[idx][1] = coords_y[y] + self.position[1]
                    self.points[idx][2] = self.position[2] + (x - self.n_x / 2) * self.slope_x + (y - self.n_y / 2) * self.slope_y\
                                          + (y - self.n_y/2)**2 * self.arc[1] + (x - self.n_x/2)**2 * self.arc[0]
                    if x > self.step_dist[0]:
                        self.points[idx][2] += height
                        if x < self.n_x-1:
                            if coords_x[x] == coords_x[x+1]:
                                height += self.step_height
                    self.origin += self.points[idx]
                    idx += 1
            self.origin /= len(self.points)
            self.points_updated = False
            self.vao_not_updated = True

        return self.points

    def set_step_dist(self, dist_x, dist_y):
        if dist_x != self.step_dist[0] or dist_y != self.step_dist[1]:
            self.points_updated = True
            self.step_dist[0] = dist_x
            self.step_dist[1] = dist_y

    def set_step_len(self, len_x, len_y):
        if len_x != self.step_len[0] or len_y != self.step_len[1]:
            self.points_updated = True
            self.step_len[0] = len_x
            self.step_len[1] = len_y

    def set_arc(self, arc_x, arc_y):
        if arc_x != self.arc[0] or arc_y != self.arc[1]:
            self.points_updated = True
            self.arc[0] = arc_x
            self.arc[1] = arc_y

    def set_step_height(self, height):
        if self.step_height != height:
            self.points_updated = True
            self.step_height = height


class CubeTerrain(SimplexTerrain):

    def __init__(self, name, position, n_x, n_y, slope_x, slope_y, step, scale, cube_height):
        super(CubeTerrain, self).__init__(name, position, n_x, n_y, slope_x, slope_y, step, scale)
        self.cube_height = cube_height

    def get_points(self):
        if self.points_updated:
            coords_x = (np.array([i for i in range(0, self.n_x)],
                                dtype=np.float32) - float(self.n_x-1) / 2.0) * self.step / self.scale[0]
            coords_y = (np.array([i for i in range(0, self.n_y)],
                                dtype=np.float32) - float(self.n_y-1) / 2.0) * self.step / self.scale[1]
            noise = opensimplex.noise2array(np.array(coords_x + self.seed / 3), np.array(coords_y + self.seed / 4)) * self.scale[2]
            self.points = np.array([np.array([0, 0, 0], np.float32) for _ in range(self.n_x * self.n_y)])

            idx = 0
            self.origin = np.array([0., 0., 0.])
            for y in range(0, self.n_y):
                for x in range(0, self.n_x):
                    self.points[idx][0] = coords_x[x] * self.scale[0] + self.position[0]
                    self.points[idx][1] = coords_y[y] * self.scale[1] + self.position[1]
                    self.points[idx][2] = self.position[2] + (x - self.n_x / 2) * self.slope_x + (y - self.n_y / 2) * self.slope_y
                    if self.cube_height > 0:
                        self.points[idx][2] += int(noise[y][x] / self.cube_height) * self.cube_height

                    self.origin += self.points[idx]
                    idx += 1
            self.origin /= len(self.points)
            self.points_updated = False
            self.vao_not_updated = True

        return self.points

    def set_cube_height(self, height):
        if self.cube_height != height:
            self.points_updated = True
            self.cube_height = height


def load_model(vertices, normals, mode):
    vao = Vao()
    vao.store_float_data_in_attribute_list(vertices, mode, 0, 3)
    if normals is not None:
        vao.store_float_data_in_attribute_list(normals, mode, 1, 3)
    return RawModel(vao, len(vertices) / 3)


def load_model_2D(vertices, mode):
    vao = Vao()
    vao.store_float_data_in_attribute_list(data=vertices, mode=mode, attrib_list=0, instance_size=2)
    return RawModel(vao, len(vertices) / 2)


def save_terrain(path, terrain):
    if isinstance(terrain, SimplexTerrain):
        terrain_copy = SimplexTerrain(terrain.name, terrain.position, terrain.n_x, terrain.n_y, terrain.slope_x, terrain.slope_y, terrain.step, terrain.scale)
    if isinstance(terrain, StepTerrain):
        terrain_copy = StepTerrain(terrain.name, terrain.position, terrain.n_x, terrain.n_y, terrain.slope_x, terrain.slope_y, terrain.step, terrain.scale, terrain.step_height, terrain.step_dist, terrain.step_len, terrain.arc)
    if isinstance(terrain, CubeTerrain):
        terrain_copy = CubeTerrain(terrain.name, terrain.position, terrain.n_x, terrain.n_y, terrain.slope_x, terrain.slope_y, terrain.step, terrain.scale, terrain.cube_height)

    with open(path+"/"+terrain.name+".ter", "wb") as f:
        pickle.dump(terrain_copy, f)


def load_terrain(path):
    with open(path, "rb") as f:
        t = pickle.load(f)
        t.name = path.split("/")[-1].split(".")[0]
        t.regenerate()
    return t


if __name__ == "__main__":
    ter = load_terrain("terrains/Waves.ter")
    ter.n_y = 80
    print(ter)
    save_terrain("terrains", ter)
