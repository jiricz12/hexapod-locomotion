import numpy as np
import os
import time
from src.environment.environments import StructuredHexapodEnv

# This script will run correctly assuming you have pythonpath pointing to the src directory. If you open a pycharm project inside the hw_1 file from sources then pythonpath should be set automatically
# Otherwise you can export PYTHONPATH to add the src directory in the terminal as follows: export PYTHONPATH=${PYTHONPATH}:/home/.../hw_1

SAVE_ITERS = 150
TRAINING_ITERS = 1
EVALUATION_ITERS = 10
EXPLORATION_RATE = 0.01
EXPLORATION_ATTEMPTS = 100

class PolicyNet():
    def __init__(self, obs_dim, act_dim):
        # Initialize your weight parameters of your policy.
        # The simplest policy to implement would be a simple affine mapping y = xw+b, where x is the input,
        # w is the weight matrix and b is the bias
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        #self.U = np.random.rand(act_dim, obs_dim+1)
        self.U = np.zeros((act_dim, obs_dim + 1))

    def set_params(self, w):
        self.U = w.reshape((self.act_dim, self.obs_dim+1))

    def get_params(self):
        return self.U.flatten()

    def forward(self, x):
        x = np.append(x, 1)
        return self.U.dot(x)


def f_wrapper(env, policy):
    def f(w):
        reward = 0
        done = False
        obs = env.reset()

        # Map the weight vector to your policy
        policy.set_params(w)

        while not done:

            # Get action from policy
            act = policy.forward(obs)

            # Step environment
            obs, rew, done, _ = env.step(act)

            reward += rew
        return reward

    return f


def perturbation_w(size):
    return (np.random.rand(size) - 0.5) * EXPLORATION_RATE


def evaluate_w(f, w, iterations):
    score = 0
    for _ in range(iterations):
        score += f(w)
    return score/iterations


def my_opt(f, w_init, iters):
    # Your optimization algorithm. Takes in an evaluation function f, and initial solution guess w_init and returns
    # parameters w_best which 'solve' the problem to some degree.
    global_best_w = w_init
    global_best_score = evaluate_w(f, global_best_w, 10)
    clc = time.time()
    for iter in range(0, iters):
        print("opt_iteration: ", iter)
        local_best_w = global_best_w
        local_best_score = global_best_score
        print("exp_iteration: ", end="")
        for alter in range(EXPLORATION_ATTEMPTS):
            print(alter, end=" ")
            w = global_best_w + perturbation_w(global_best_w.size)
            score = evaluate_w(f, w, EVALUATION_ITERS)
            if score > local_best_score:
                local_best_score = score
                local_best_w = w
        global_best_w = local_best_w
        global_best_score = local_best_score
        print("\nScore: ", local_best_score)
    print("time:", time.time()-clc)

    return global_best_w, global_best_score


def test(w_best, max_steps=70, animate=False):
    # Make the environment and your policy
    env = QuadrupedBulletEnv(animate=animate, max_steps=max_steps)
    policy = PolicyNet(env.obs_dim, env.act_dim)

    # Make evaluation function
    f = f_wrapper(env, policy)

    # Evaluate
    r_avg = 0
    eval_iters = 10
    for i in range(eval_iters):
        r = f(w_best)
        r_avg += r
        print(r)
    return r_avg / eval_iters


if __name__ == "__main__":
    train = False
    policy_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'policies/quadruped_bbx_swing_phases.npy')
    max_steps = 70
    N_training_iters = TRAINING_ITERS
    w_best = None
    
    if train:
        # Make the environment and your policy
        env = StructuredHexapodEnv(animate=True, max_steps=max_steps)
        policy = PolicyNet(env.obs_dim, env.act_dim)
        #policy.set_params(np.load(policy_path))
        # Make evaluation function
        f = f_wrapper(env, policy)

        # Initial guess of solution
        w_best = policy.get_params()
        # Perform optimization
        iter = 0
        while True:
            print(iter+1, end=".\n")
            w_best, r_best = my_opt(f, w_best, N_training_iters)
            if r_best > 70:
                np.save(policy_path, w_best)
                print("SAVING...")
            iter += 1
        print(f"r_best: {r_best}")

        # Save policy
        #np.save(policy_path, w_best)
        env.close()

    if not train:
        w_best = np.load(policy_path)
    print(f"Avg test rew: {test(w_best, max_steps=max_steps, animate=not train)}")
