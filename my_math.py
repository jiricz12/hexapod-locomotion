import copy

import numpy as np


def map(x, src_min, src_max, des_min, des_max):
    return (x - src_min) / (src_max - src_min) * (des_max - des_min) + des_min


def clamp(x, min, max):
    if x <= min:
        return min
    elif x >= max:
        return max
    else:
        return x

def create_trans_mat(pos, rot, s):
    mat = np.array([[1, 0, 0, pos[0]],
                    [0, 1, 0, pos[1]],
                    [0, 0, 1, pos[2]],
                    [0, 0, 0,      1]])

    mat = mat @ get_rotation_mat4_z(rot[2])
    mat = mat @ get_rotation_mat4_y(rot[1])
    mat = mat @ get_rotation_mat4_x(rot[0])

    if isinstance(s, list):
        scale = np.array([[s[0], 0, 0, 0],
                          [0, s[1], 0, 0],
                          [0, 0, s[2], 0],
                          [0, 0, 0, 1]])
    else:
        scale = np.array([[s, 0, 0, 0],
                          [0, s, 0, 0],
                          [0, 0, s, 0],
                          [0, 0, 0, 1]])
    mat = mat @ scale

    return mat


def get_rotation_mat3_x(a):
    s = np.sin(a)
    c = np.cos(a)
    mat = np.array([[1, 0, 0],
                    [0, c, -s],
                    [0, s, c]])
    return mat


def get_rotation_mat3_y(a):
    s = np.sin(a)
    c = np.cos(a)
    mat = np.array([[c,  0,  s],
                    [0,  1,  0],
                    [-s, 0,  c]])
    return mat


def get_rotation_mat3_z(a):
    s = np.sin(a)
    c = np.cos(a)
    mat = np.array([[c, -s, 0],
                    [s,  c, 0],
                    [0,  0, 1]])
    return mat


def get_rotation_mat4_x(a):
    s = np.sin(a)
    c = np.cos(a)
    mat = np.array([[1, 0, 0, 0],
                    [0, c, -s, 0],
                    [0, s, c, 0],
                    [0, 0, 0, 1]])
    return mat


def get_rotation_mat4_y(a):
    s = np.sin(a)
    c = np.cos(a)
    mat = np.array([[c,  0,  s, 0],
                    [0,  1,  0, 0],
                    [-s, 0,  c, 0],
                    [0,  0,  0, 1]])
    return mat


def get_rotation_mat4_z(a):
    s = np.sin(a)
    c = np.cos(a)
    mat = np.array([[c, -s, 0, 0],
                    [s,  c, 0, 0],
                    [0,  0, 1, 0],
                    [0,  0, 0, 1]])
    return mat


def inv_trans_mat(mat):
    r = np.transpose(mat[0:3, 0:3])
    t = -r @ mat[0:3, 3]

    new_mat = np.block([
                        [np.array(r), np.array(t).reshape(3, 1)],
                        [np.array([0, 0, 0]), np.array([1])]
                        ])

    return new_mat


def get_translation(mat):
    return mat[0:3, 3]


def normalize(vec):
    vec_len = np.linalg.norm(vec)
    vec = copy.deepcopy(vec) / vec_len
    return vec


def get_view_matrix(eye, center, up):
    f = normalize(center - eye)
    s = normalize(np.cross(f, up))
    u = np.cross(s, f)
    view = np.array(
        [[s[0], s[1], s[2], -np.dot(s, eye)],
         [u[0], u[1], u[2], -np.dot(u, eye)],
         [-f[0], -f[1], -f[2],  np.dot(f, eye)],
         [0, 0, 0, 1]])
    return view


def get_perspektive_matrix(fovy, aspect, z_near, z_far):
    tan_half_fovy = np.tan(fovy / 2)
    perspective = np.array(
        [[1 / (aspect * tan_half_fovy), 0, 0, 0],
         [0, 1 / tan_half_fovy, 0, 0],
         [0, 0, - (z_far + z_near) / (z_far - z_near), - (2 * z_far * z_near) / (z_far - z_near)],
         [0, 0, -1, 0]])
    return perspective


def spherical_to_cartesian(coords):
    r = coords[0] * np.sin(coords[2])
    x = r * np.cos(coords[1])
    y = r * np.sin(coords[1])
    z = coords[0] * np.cos(coords[2])
    return np.array([x, y, z])


def DH(theta, d, a, alpha):
    sa = np.sin(alpha)
    st = np.sin(theta)
    ca = np.cos(alpha)
    ct = np.cos(theta)
    mat = np.array([[ct, -st*ca,  st*sa, a*ct],
                    [st,  ct*ca, -ct*sa, a*st],
                    [0,      sa,     ca,    d],
                    [0,       0,      0,    1]])
    return mat


def transform(mat, vec):
    return (mat @ np.array([*vec, 1.0]))[0:3]


def projection(base, vec):
    return base * (np.dot(base, vec) / np.linalg.norm(base)**2)


def rejection(base, vec):
    return vec - projection(base, vec)


def get_intersections(x0, y0, r0, x1, y1, r1):
    # circle 1: (x0, y0), radius r0
    # circle 2: (x1, y1), radius r1

    d = np.sqrt((x1 - x0) ** 2 + (y1 - y0) ** 2)


    # non intersecting
    if d > r0 + r1:
        return None, None

    if np.round(d, 5) == r0 + r1:
        return np.array([x0 + x1, y0 + y1])/2.0, np.array([x0 + x1, y0 + y1])/2.0
    # One circle within other
    if d < abs(r0 - r1):
        return None, None
    # coincident circles

    if d == 0 and r0 == r1:
        return None, None
    else:
        a = (r0 ** 2 - r1 ** 2 + d ** 2) / (2 * d)

        h = np.sqrt(r0 ** 2 - a ** 2)
        x2 = x0 + a * (x1 - x0) / d
        y2 = y0 + a * (y1 - y0) / d
        x3 = x2 + h * (y1 - y0) / d
        y3 = y2 - h * (x1 - x0) / d

        x4 = x2 - h * (y1 - y0) / d
        y4 = y2 + h * (x1 - x0) / d

        return np.array([x3, y3]), np.array([x4, y4])


def snap(val, base, distance):
    arr_val = np.array(val)
    arr_base = np.array(base)
    dist = np.linalg.norm(arr_base-arr_val)
    if dist < distance:
        return base
    else:
        return val


def shift_right(arr, n):
    for i in range(n):
        tmp = arr[-1]
        arr[1:] = arr[0:-1]
        arr[0] = tmp


def shift_left(arr, n):
    for i in range(n):
        tmp = arr[0]
        arr[0:-1] = arr[1:]
        arr[-1] = tmp


# if __name__ == "__main__":
#     arr = [0, 1, 2, 3, 4, 5]
#     shift_left(arr, 2)
#     print(arr)